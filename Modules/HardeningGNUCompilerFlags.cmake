### Preamble {{{
##  ==========================================================================
##        @file HardeningGNUCompilerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 11:07:05 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-13 Monday 21:32:04 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_HARDENING_GNU_COMPILER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_HARDENING_GNU_COMPILER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Hardening)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(HARDENING_PIE)
  if    (CMAKE_VERSION VERSION_EQUAL   2.8.9)
    def_bool(CMAKE_POSITION_INDEPENDENT_FLAGS Y)
  elseif(CMAKE_VERSION VERSION_GREATER 2.8.9)
    def_bool(CMAKE_POSITION_INDEPENDENT_CODE  Y)
  else()
    def(HARDENING_PIE_GNU_COMPILER_FLAGS
      -fPIE)
  endif()
endif()
##
if(HARDENING_SSP)
  def(HARDENING_SSP_GNU_COMPILER_FLAGS
    -fstack-protector-strong)
endif()
##
if(HARDENING_FFS)
  def(HARDENING_FFS_GNU_COMPILER_FLAGS
#   -O1
    -D_FORTIFY_SOURCE=2)
endif()
##
if(HARDENING_SFSFS)
  def(HARDENING_SFSFS_GNU_COMPILER_FLAGS
    -Werror=format
    -Wformat=2)
endif()
##
if(HARDENING_NES)
  def(HARDENING_NES_GNU_COMPILER_FLAGS
    -Werror=trampolines
    -Wtrampolines)
endif()
##
def(HARDENING_GNU_COMPILER_FLAGS
  ${HARDENING_PIE_GNU_COMPILER_FLAGS}
  ${HARDENING_SSP_GNU_COMPILER_FLAGS}
  ${HARDENING_FFS_GNU_COMPILER_FLAGS}
  ${HARDENING_SFSFS_GNU_COMPILER_FLAGS}
  ${HARDENING_NES_GNU_COMPILER_FLAGS})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
