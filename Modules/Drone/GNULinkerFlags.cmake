### Preamble {{{
##  ==========================================================================
##        @file GNULinkerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-28 Tuesday 01:03:43 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-12 Sunday 23:47:44 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_GNU_LINKER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_GNU_LINKER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(HardeningGNULinkerFlags)
include(LinkerGNULinkerFlags)
include(SanitizerGNULinkerFlags)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(DRONE_GNU_LINKER_FLAGS
  -Wl,--build-id=sha1
  -Wl,--discard-all
  -Wl,--no-undefined
  ${HARDENING_GNU_LINKER_FLAGS}
  ${LINKER_GNU_LINKER_FLAGS}
  ${SANITIZER_GNU_LINKER_FLAGS})
##
def(DRONE_GNU_EXE_LINKER_FLAGS
  ${HARDENING_GNU_EXE_LINKER_FLAGS})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
