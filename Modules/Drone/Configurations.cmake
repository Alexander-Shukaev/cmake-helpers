### Preamble {{{
##  ==========================================================================
##        @file Configurations.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-17 Sunday 21:55:37 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-12 Sunday 14:24:35 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_CONFIGURATIONS_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_CONFIGURATIONS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Configurations)
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(DRONE_CONFIGURATIONS
  None
  ${CONFIGURATIONS})
def(DRONE_CONFIGURATIONS_LOWER
  none
  ${CONFIGURATIONS_LOWER})
def(DRONE_CONFIGURATIONS_UPPER
  NONE
  ${CONFIGURATIONS_UPPER})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
