### Preamble {{{
##  ==========================================================================
##        @file GNUCompilerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-26 Sunday 21:49:26 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 04:09:56 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_GNU_COMPILER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_GNU_COMPILER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(HardeningGNUCompilerFlags)
include(MaxErrorsGNUCompilerFlags)
include(OutputGNUCompilerFlags)
include(SanitizerGNUCompilerFlags)
include(VisibilityGNUCompilerFlags)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(DRONE_GNU_COMPILER_FLAGS
  -W
  -Wall
  -Wbad-function-cast
  -Wcast-align
  -Wcast-qual
  -Wconversion
  -Wdisabled-optimization
  -Werror
  -Wextra
  -Wfloat-equal
  -Winline
  -Wlogical-op
  -Wmissing-include-dirs
  -Wmissing-prototypes
  -Wnested-externs
  -Wno-error=disabled-optimization
  -Wno-error=float-equal
  -Wno-error=inline
  -Wnon-virtual-dtor
  -Wnoexcept
  -Wold-style-cast
  -Wold-style-declaration
  -Wold-style-definition
  -Woverloaded-virtual
  -Wpedantic
  -Wredundant-decls
  -Wreorder
# -Wshadow
  -Wsign-promo
  -Wstrict-null-sentinel
  -Wstrict-overflow=5
  -Wstrict-prototypes
  -Wswitch-bool
  -Wswitch-default
  -Wswitch-enum
  -Wswitch-unreachable
  -Wundef
  -Wwrite-strings
  -ansi
  -fdiagnostics-color=auto
  -ffloat-store
  -finline-limit=10000
  "--param inline-unit-growth=100"
  "--param large-function-growth=1000"
  -fmessage-length=0
  -fno-common
  -ftrapv
  -fverbose-asm
  -pass-exit-codes
  -pedantic
  -pedantic-errors
  ${HARDENING_GNU_COMPILER_FLAGS}
  ${MAX_ERRORS_GNU_COMPILER_FLAGS}
  ${OUTPUT_GNU_COMPILER_FLAGS}
  ${SANITIZER_GNU_COMPILER_FLAGS}
  ${VISIBILITY_GNU_COMPILER_FLAGS})
##
def(DRONE_GNU_COMPILER_FLAGS_DEBUG
  -O1
  -D_DEBUG
  -g3
  -ggdb)
##
def(DRONE_GNU_COMPILER_FLAGS_RELEASE
  -funroll-loops
# -march=native
  -mtune=generic)
##
def(DRONE_GNU_COMPILER_FLAGS_RELWITHDEBINFO
  ${DRONE_GNU_COMPILER_FLAGS_RELEASE})
##
def(DRONE_GNU_COMPILER_FLAGS_MINSIZEREL
  ${DRONE_GNU_COMPILER_FLAGS_RELEASE})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
