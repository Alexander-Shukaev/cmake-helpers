### Preamble {{{
##  ==========================================================================
##        @file Message.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-09 Tuesday 23:37:12 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 03:10:38 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_MESSAGE_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_MESSAGE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Drone/Prompt)
##
include(CMakeParseArguments)
include(Contains)
include(Glue)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(drone_message)
  if(ARGN)
    set(_INDEX   1)
    set(_MESSAGE "${DRONE_PROMPT}: ")
    set(_MODE    ${ARGV0})
    set(_MODES   STATUS
                 WARNING
                 AUTHOR_WARNING
                 SEND_ERROR
                 FATAL_ERROR
                 DEPRECATION)
    contains(_MODE_IS_MODE _MODES ${_MODE})
    if(_MODE_IS_MODE)
      math(EXPR _INDEX "${_INDEX} + 1")
      set(_MESSAGE ${_MODE} ${_MESSAGE})
    endif()
    foreach(_I RANGE ${_INDEX} ${ARGC})
      math(EXPR _I "${_I} - 1")
      set(_MESSAGE ${_MESSAGE}${ARGV${_I}})
    endforeach()
    message(${_MESSAGE})
  else()
    message()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
