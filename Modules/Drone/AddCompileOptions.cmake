### Preamble {{{
##  ==========================================================================
##        @file AddCompileOptions.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:15:44 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 01:26:36 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_ADD_COMPILE_OPTIONS_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_ADD_COMPILE_OPTIONS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Drone/Configurations)
include(Drone/Format)
include(Drone/GNUCompilerFlags)
include(Drone/Message)
include(Drone/RelativeSourceDir)
include(Drone/Verbose)
##
include(CMakeParseArguments)
include(FilterCompilerFlags)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(drone_add_compile_options LANGUAGE)
  set(_OPTIONS  DEFAULT
                REQUIRED
                QUIET)
  set(_KEYWORDS CONFIGURATIONS)
  cmake_parse_arguments("" "${_OPTIONS}" "" "${_KEYWORDS}" ${ARGN})
  if(NOT _CONFIGURATIONS)
    # TODO:
    set(_CONFIGURATIONS None)
  endif()
  set(_MESSAGES)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    set(_FLAGS ${_UNPARSED_ARGUMENTS})
    if(NOT _FLAGS OR _DEFAULT)
      # TODO:
      foreach(_C ${_CONFIGURATIONS})
        string(TOUPPER "${_C}" _C)
        if(_C MATCHES "^NONE$")
          set(_SUFFIX)
        else()
          set(_SUFFIX _${_C})
        endif()
        set(_DEFAULT_FLAGS
          ${DRONE_COMPILER_FLAGS${_SUFFIX}}
          ${DRONE_${LANGUAGE}_COMPILER_FLAGS${_SUFFIX}})
        if(NOT _DEFAULT_FLAGS AND CMAKE_${LANGUAGE}_COMPILER_ID MATCHES "GNU")
          set(_DEFAULT_FLAGS
            ${DRONE_GNU_COMPILER_FLAGS${_SUFFIX}}
            ${DRONE_GNU_${LANGUAGE}_COMPILER_FLAGS${_SUFFIX}})
        endif()
        list(APPEND _FLAGS ${_DEFAULT_FLAGS})
      endforeach()
    endif()
    # TODO:
    if   (N)
    if(_FLAGS)
      list(REMOVE_DUPLICATES _FLAGS)
    endif()
    endif(N)
    set(_FILTERED_FLAGS)
    if(_FLAGS)
      filter_compiler_flags(${LANGUAGE} _FILTERED_FLAGS ${_FLAGS})
      if(NOT _FILTERED_FLAGS STREQUAL _FLAGS)
        foreach(_F ${_FILTERED_FLAGS})
          list(REMOVE_ITEM _FLAGS ${_F})
        endforeach()
        set(_MESSAGES "Compiler flags not supported: "
          "${CMAKE_${LANGUAGE}_COMPILER}\n")
        foreach(_F ${_FLAGS})
          list(APPEND _MESSAGES "  ${_F}\n")
        endforeach()
      endif()
    endif()
  else()
    set(_MESSAGES "${LANGUAGE} compiler not loaded")
  endif()
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
  endif()
  add_compile_options(${_FILTERED_FLAGS})
  get_property(_OPTIONS DIRECTORY PROPERTY COMPILE_OPTIONS)
  # TODO:
  if   (N)
  if(_OPTIONS)
    list(REMOVE_DUPLICATES _OPTIONS)
  endif()
  endif(N)
  set_property(DIRECTORY PROPERTY COMPILE_OPTIONS ${_OPTIONS})
  if(DRONE_VERBOSE AND _OPTIONS)
    drone_format(_VAR BLUE COMPILE_OPTIONS)
    drone_relative_source_dir(_DIR)
    drone_message(STATUS "${_VAR}: ${_DIR}")
    foreach(_O ${_OPTIONS})
      message(STATUS "  ${_O}")
    endforeach()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
