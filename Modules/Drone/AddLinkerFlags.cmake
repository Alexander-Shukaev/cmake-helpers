### Preamble {{{
##  ==========================================================================
##        @file AddLinkerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:17:06 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-12 Sunday 23:45:59 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_ADD_LINKER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_ADD_LINKER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Drone/Configurations)
include(Drone/Format)
include(Drone/GNULinkerFlags)
include(Drone/Message)
include(Drone/RelativeSourceDir)
include(Drone/Verbose)
##
include(Args)
include(CMakeParseArguments)
include(Contains)
include(FilterLinkerFlags)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(drone_add_linker_flags LANGUAGE)
  set(_MESSAGES)
  if(ARGN)
    set(_TYPE  ${ARGV1})
    set(_TYPES EXE
               MODULE
               SHARED
               STATIC)
    contains(_TYPE_IS_TYPE _TYPES ${_TYPE})
    if(NOT _TYPE_IS_TYPE)
      set(_MESSAGES "Unknown target type: ${_TYPE}")
    endif()
  else()
    set(_MESSAGES "Invalid number of arguments: 1")
  endif()
  if(_MESSAGES)
    message(FATAL_ERROR ${_MESSAGES})
  endif()
  list(REMOVE_AT ARGN 0)
  set(_OPTIONS  DEFAULT
                REQUIRED
                QUIET)
  set(_KEYWORDS CONFIGURATIONS)
  cmake_parse_arguments("" "${_OPTIONS}" "" "${_KEYWORDS}" ${ARGN})
  if(NOT _CONFIGURATIONS)
    set(_CONFIGURATIONS ${DRONE_CONFIGURATIONS})
  endif()
  foreach(_C ${_CONFIGURATIONS})
    string(TOUPPER "${_C}" _C)
    if(_C MATCHES "^NONE$")
      set(_SUFFIX)
    else()
      set(_SUFFIX _${_C})
    endif()
    set(_MESSAGES)
    if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
      set(_FLAGS ${_UNPARSED_ARGUMENTS})
      if(NOT _FLAGS OR _DEFAULT)
        set(_DEFAULT_FLAGS
          ${DRONE_LINKER_FLAGS${_SUFFIX}}
          ${DRONE_${LANGUAGE}_LINKER_FLAGS${_SUFFIX}}
          ${DRONE_${_TYPE}_LINKER_FLAGS${_SUFFIX}}
          ${DRONE_${LANGUAGE}_${_TYPE}_LINKER_FLAGS${_SUFFIX}})
        if(NOT _DEFAULT_FLAGS AND CMAKE_${LANGUAGE}_COMPILER_ID MATCHES "GNU")
          set(_DEFAULT_FLAGS
            ${DRONE_GNU_LINKER_FLAGS${_SUFFIX}}
            ${DRONE_GNU_${LANGUAGE}_LINKER_FLAGS${_SUFFIX}}
            ${DRONE_GNU_${_TYPE}_LINKER_FLAGS${_SUFFIX}}
            ${DRONE_GNU_${LANGUAGE}_${_TYPE}_LINKER_FLAGS${_SUFFIX}})
        endif()
        list(APPEND _FLAGS ${_DEFAULT_FLAGS})
      endif()
      # TODO:
      if   (N)
      if(_FLAGS)
        list(REMOVE_DUPLICATES _FLAGS)
      endif()
      endif(N)
      set(_FILTERED_FLAGS)
      if(_FLAGS)
        filter_linker_flags(${LANGUAGE} _FILTERED_FLAGS ${_FLAGS})
        if(NOT _FILTERED_FLAGS STREQUAL _FLAGS)
          foreach(_F ${_FILTERED_FLAGS})
            list(REMOVE_ITEM _FLAGS ${_F})
          endforeach()
          set(_MESSAGES "Linker flags not supported: "
            "${CMAKE_${LANGUAGE}_COMPILER}\n")
          foreach(_F ${_FLAGS})
            list(APPEND _MESSAGES "  ${_F}\n")
          endforeach()
        endif()
      endif()
    else()
      set(_MESSAGES "${LANGUAGE} linker not loaded")
    endif()
    if(_MESSAGES)
      if(_REQUIRED)
        message(FATAL_ERROR ${_MESSAGES})
      endif()
      if(NOT _QUIET)
        message(WARNING     ${_MESSAGES})
      endif()
    endif()
    set(_FLAGS ${CMAKE_${_TYPE}_LINKER_FLAGS${_SUFFIX}})
    separate_arguments(_FLAGS)
    list(APPEND _FLAGS ${_FILTERED_FLAGS})
    # TODO:
    if   (N)
    if(_FLAGS)
      list(REMOVE_DUPLICATES _FLAGS)
    endif()
    endif(N)
    # TODO:
    string(TOUPPER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_UPPER)
    if(DRONE_VERBOSE AND _FLAGS AND
       (_C STREQUAL CMAKE_BUILD_TYPE_UPPER OR _C MATCHES "^NONE$"))
      drone_format(_VAR BLUE CMAKE_${_TYPE}_LINKER_FLAGS${_SUFFIX})
      drone_relative_source_dir(_DIR)
      drone_message(STATUS "${_VAR}: ${_DIR}")
      foreach(_F ${_FLAGS})
        message(STATUS "  ${_F}")
      endforeach()
    endif()
    args(_FLAGS ${_FLAGS})
    set(CMAKE_${_TYPE}_LINKER_FLAGS${_SUFFIX} ${_FLAGS} PARENT_SCOPE)
  endforeach()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
