### Preamble {{{
##  ==========================================================================
##        @file Project.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-07 Tuesday 00:53:42 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-08-08 Monday 00:46:06 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_PROJECT_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_PROJECT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Drone/Verbose)
include(Drone/Message)
include(Drone/Format)
##
include(CMakeDisableInSourceBuild)
include(CMakeParseArguments)
include(DefProjectDefaultInstallDirs)
include(EmptyDefaultInstallDirs)
include(ExportProject)
include(HRule)
include(UnsetDefaultInstallDirs)
include(Version)
##  ==========================================================================
##  }}} Modules
##
### Policies {{{
##  ==========================================================================
if(POLICY CMP0011)
  cmake_policy(SET CMP0011 NEW)
endif()
if(POLICY CMP0048)
  cmake_policy(SET CMP0048 NEW)
endif()
##  ==========================================================================
##  }}} Policies
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(drone_project NAME)
  message("${HRULE80}")
  set(_DRONE_PROJECT_NAME ${NAME})
  if(    PROJECT_NAME                                       AND
     NOT PROJECT_NAME             STREQUAL "Project"        AND
     NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    set(_DRONE_PROJECT_NAME ${PROJECT_NAME}_${_DRONE_PROJECT_NAME})
  endif()
  # --------------------------------------------------------------------------
  set(_DRONE_PROJECT_OPTIONS ENABLE_IN_SOURCE_BUILD
                             EXPORT)
  cmake_parse_arguments(_DRONE_PROJECT
                        "${_DRONE_PROJECT_OPTIONS}"
                        ""
                        ""
                        ${ARGN})
  # --------------------------------------------------------------------------
  if(NOT _DRONE_PROJECT_ENABLE_IN_SOURCE_BUILD)
    cmake_disable_in_source_build()
  endif()
  project(${_DRONE_PROJECT_NAME} ${_DRONE_PROJECT_UNPARSED_ARGUMENTS})
  set(   SUBPROJECT              ${NAME})
  set(      PROJECT      ${PROJECT_NAME})
  set(CMAKE_PROJECT_NAME ${PROJECT_NAME})
  string(TOLOWER    "${SUBPROJECT}"         SUBPROJECT_LOWER)
  string(TOUPPER    "${SUBPROJECT}"         SUBPROJECT_UPPER)
  string(TOLOWER       "${PROJECT}"            PROJECT_LOWER)
  string(TOUPPER       "${PROJECT}"            PROJECT_UPPER)
  string(TOLOWER       "${PROJECT_NAME}"       PROJECT_NAME_LOWER)
  string(TOUPPER       "${PROJECT_NAME}"       PROJECT_NAME_UPPER)
  string(TOLOWER "${CMAKE_PROJECT_NAME}" CMAKE_PROJECT_NAME_LOWER)
  string(TOUPPER "${CMAKE_PROJECT_NAME}" CMAKE_PROJECT_NAME_UPPER)
  if(NOT PROJECT_VERSION)
    set(PROJECT_VERSION_MAJOR 0)
    set(PROJECT_VERSION_MINOR 0)
    set(PROJECT_VERSION_PATCH 0)
    version(PROJECT_VERSION ${PROJECT_VERSION_MAJOR}
                            ${PROJECT_VERSION_MINOR}
                            ${PROJECT_VERSION_PATCH})
  endif()
  if(NOT ${PROJECT}_VERSION)
    set(${PROJECT}_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
    set(${PROJECT}_VERSION_MINOR ${PROJECT_VERSION_MINOR})
    set(${PROJECT}_VERSION_PATCH ${PROJECT_VERSION_PATCH})
    set(${PROJECT}_VERSION       ${PROJECT_VERSION})
  endif()
  set(${PROJECT}_DIR ${PROJECT_BINARY_DIR}
    CACHE PATH
    "The project directory for a CMake configuration file of \"${PROJECT}\".")
  # --------------------------------------------------------------------------
  set(PROJECT_EXPORT N)
  if(PROJECT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    if(_DRONE_PROJECT_EXPORT)
      export_project()
    endif()
    set(PROJECT_STANDALONE Y)
    unset_default_install_dirs()
  else()
    set(PROJECT_STANDALONE N)
    empty_default_install_dirs()
  endif()
  def_project_default_install_dirs()
  # --------------------------------------------------------------------------
  unset(PROJECT_DEFINITIONS)
  unset(PROJECT_OPTIONS)
  unset(PROJECT_INCLUDE_DIR)
  unset(PROJECT_INCLUDE_DIRS)
  unset(PROJECT_LIBRARY_DIR)
  unset(PROJECT_LIBRARY_DIRS)
  unset(PROJECT_BINARY_INCLUDE_DIR)
  unset(PROJECT_SOURCE_INCLUDE_DIR)
  unset(PROJECT_BINARY_LIBRARY_DIR)
  unset(PROJECT_SOURCE_LIBRARY_DIR)
  # --------------------------------------------------------------------------
  drone_format(_PROJECT         BOLD_CYAN ${PROJECT})
  drone_format(_PROJECT_VERSION      CYAN ${PROJECT_VERSION})
  drone_message(STATUS "Project: ${_PROJECT} "
                       "(version \"${_PROJECT_VERSION}\")")
  if(DRONE_VERBOSE)
    foreach(_VAR SUBPROJECT
                 SUBPROJECT_LOWER
                 SUBPROJECT_UPPER
                    PROJECT
                    PROJECT_LOWER
                    PROJECT_UPPER
                    PROJECT_NAME
                    PROJECT_NAME_LOWER
                    PROJECT_NAME_UPPER
                    PROJECT_VERSION
                    PROJECT_VERSION_MAJOR
                    PROJECT_VERSION_MINOR
                    PROJECT_VERSION_PATCH
                    PROJECT_VERSION_TWEAK
                    PROJECT_EXPORT
                    PROJECT_STANDALONE
                    PROJECT_BINARY_DIR
                    PROJECT_SOURCE_DIR
                    INSTALL_CMAKE_DIR
                    INSTALL_INCLUDE_DIR
                    INSTALL_ARCHIVE_DIR
                    INSTALL_LIBRARY_DIR
                    INSTALL_RUNTIME_DIR
                    ${PROJECT}_VERSION
                    ${PROJECT}_VERSION_MAJOR
                    ${PROJECT}_VERSION_MINOR
                    ${PROJECT}_VERSION_PATCH
                    ${PROJECT}_VERSION_TWEAK
                    ${PROJECT}_DIR
                    ${PROJECT}_BINARY_DIR
                    ${PROJECT}_SOURCE_DIR
                    ${PROJECT}_INSTALL_CMAKE_DIR
                    ${PROJECT}_INSTALL_INCLUDE_DIR
                    ${PROJECT}_INSTALL_ARCHIVE_DIR
                    ${PROJECT}_INSTALL_LIBRARY_DIR
                    ${PROJECT}_INSTALL_RUNTIME_DIR
                    CMAKE_INSTALL_PREFIX
                    CMAKE_INSTALL_RPATH)
      if(DEFINED ${_VAR})
        set(_VAL ${${_VAR}})
        drone_format(_VAR BLUE ${_VAR})
        message(STATUS "  ${_VAR}: ${_VAL}")
      endif()
    endforeach()
  endif()
  unset(_PROJECT)
  unset(_PROJECT_VERSION)
  unset(_VAR)
  unset(_VAL)
  # --------------------------------------------------------------------------
  unset(_DRONE_PROJECT_NAME)
  unset(_DRONE_PROJECT_OPTIONS)
  unset(_DRONE_PROJECT_UNPARSED_ARGUMENTS)
  unset(_DRONE_PROJECT_ENABLE_IN_SOURCE_BUILD)
  unset(_DRONE_PROJECT_EXPORT)
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
