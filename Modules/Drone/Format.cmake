### Preamble {{{
##  ==========================================================================
##        @file Format.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-08 Monday 16:20:05 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 03:32:11 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_DRONE_FORMAT_CMAKE_INCLUDED)
  return()
endif()
set(_DRONE_FORMAT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(NOT WIN32)
  string(ASCII 27 DRONE_FORMAT_ESCAPE)
  ##
  def(DRONE_FORMAT_NORMAL       "${DRONE_FORMAT_ESCAPE}[0m")
  ##
  def(DRONE_FORMAT_BLUE         "${DRONE_FORMAT_ESCAPE}[34m")
  def(DRONE_FORMAT_CYAN         "${DRONE_FORMAT_ESCAPE}[36m")
  def(DRONE_FORMAT_GREEN        "${DRONE_FORMAT_ESCAPE}[32m")
  def(DRONE_FORMAT_MAGENTA      "${DRONE_FORMAT_ESCAPE}[35m")
  def(DRONE_FORMAT_RED          "${DRONE_FORMAT_ESCAPE}[31m")
  def(DRONE_FORMAT_WHITE        "${DRONE_FORMAT_ESCAPE}[37m")
  def(DRONE_FORMAT_YELLOW       "${DRONE_FORMAT_ESCAPE}[33m")
  ##
  def(DRONE_FORMAT_BOLD         "${DRONE_FORMAT_ESCAPE}[1m")
  def(DRONE_FORMAT_BOLD_BLUE    "${DRONE_FORMAT_ESCAPE}[1;34m")
  def(DRONE_FORMAT_BOLD_CYAN    "${DRONE_FORMAT_ESCAPE}[1;36m")
  def(DRONE_FORMAT_BOLD_GREEN   "${DRONE_FORMAT_ESCAPE}[1;32m")
  def(DRONE_FORMAT_BOLD_MAGENTA "${DRONE_FORMAT_ESCAPE}[1;35m")
  def(DRONE_FORMAT_BOLD_RED     "${DRONE_FORMAT_ESCAPE}[1;31m")
  def(DRONE_FORMAT_BOLD_WHITE   "${DRONE_FORMAT_ESCAPE}[1;37m")
  def(DRONE_FORMAT_BOLD_YELLOW  "${DRONE_FORMAT_ESCAPE}[1;33m")
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(drone_format_def VAR FORMAT STRING)
  def(${VAR} ${DRONE_FORMAT_${FORMAT}}${STRING}${DRONE_FORMAT_NORMAL})
endmacro()
##
macro(drone_format_set VAR FORMAT STRING)
  set(${VAR} ${DRONE_FORMAT_${FORMAT}}${STRING}${DRONE_FORMAT_NORMAL})
endmacro()
##
macro(drone_format VAR FORMAT STRING)
  drone_format_set(${VAR} ${FORMAT} ${STRING})
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
