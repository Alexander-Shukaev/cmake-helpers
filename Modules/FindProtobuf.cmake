### Preamble {{{
##  ==========================================================================
##        @file FindProtobuf.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:46:58 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-27 Sunday 13:58:13 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(DepPkgCheckModules)
include(FindDepPackage)
include(FindPackageHandleStandardArgs)
include(SetTargetProperty)
##  ==========================================================================
##  }}} Modules
##
dep_pkg_check_modules(Protobuf_PKG protobuf)
find_path(Protobuf_INCLUDE_DIR
  NAMES        protobuf/message.h
        google/protobuf/message.h
  HINTS ${Protobuf_PKG_INCLUDE_DIRS}
  PATHS ${Protobuf_INCLUDE_DIR}
        $ENV{Protobuf_INCLUDE_DIR}
        ${CMAKE_INSTALL_PREFIX}/include
  PATH_SUFFIXES        protobuf
                google/protobuf)
mark_as_advanced(Protobuf_INCLUDE_DIR)
find_library(Protobuf_LIBRARY
  NAMES protobuf
  HINTS ${Protobuf_PKG_LIBRARY_DIRS}
  PATHS ${Protobuf_LIBRARY_DIR}
        $ENV{Protobuf_LIBRARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/lib)
mark_as_advanced(Protobuf_LIBRARY)
set(Protobuf_DEFINITIONS)
set(Protobuf_INCLUDE_DIRS ${Protobuf_INCLUDE_DIR})
set(Protobuf_LIBRARIES    ${Protobuf_LIBRARY})
set(Protobuf_OPTIONS      ${Protobuf_PKG_CFLAGS_OTHER})
##
if(DEFINED Protobuf_Threads_PREFER_PTHREAD)
  set(CMAKE_THREAD_PREFER_PTHREAD ${Protobuf_Threads_PREFER_PTHREAD})
endif()
if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  set(THREADS_PREFER_PTHREAD_FLAG Y)
endif()
dep_find_package(Protobuf Threads QUIET)
if(THREADS_FOUND)
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
    list(APPEND Protobuf_LIBRARIES Threads::Threads)
  else()
    if(THREADS_HAVE_PTHREAD_ARG)
      list(APPEND Protobuf_OPTIONS   -pthread)
    endif()
    if(CMAKE_THREAD_LIBS_INIT)
      list(APPEND Protobuf_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
    endif()
  endif()
endif()
##
dep_find_package(Protobuf ZLIB QUIET)
if(ZLIB_FOUND)
  list(APPEND Protobuf_INCLUDE_DIRS ${ZLIB_INCLUDE_DIRS})
  list(APPEND Protobuf_LIBRARIES    ${ZLIB_LIBRARIES})
endif()
##
if(WIN32)
  list(APPEND Protobuf_LIBRARIES ws2_32)
endif()
##
find_package_handle_standard_args(Protobuf
  FOUND_VAR     Protobuf_FOUND
  REQUIRED_VARS Protobuf_LIBRARY
                Protobuf_LIBRARIES
                Protobuf_INCLUDE_DIR
                Protobuf_INCLUDE_DIRS)
##
if(Protobuf_FOUND AND NOT TARGET Protobuf)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(Protobuf INTERFACE IMPORTED)
    set_target_property(Protobuf
      INTERFACE_COMPILE_DEFINITIONS       ${Protobuf_DEFINITIONS})
    set_target_property(Protobuf
      INTERFACE_COMPILE_OPTIONS           ${Protobuf_OPTIONS})
    set_target_property(Protobuf
      INTERFACE_INCLUDE_DIRECTORIES       ${Protobuf_INCLUDE_DIRS})
    set_target_property(Protobuf
      INTERFACE_LINK_LIBRARIES            ${Protobuf_LIBRARIES})
    set_target_property(Protobuf
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  endif()
endif()
