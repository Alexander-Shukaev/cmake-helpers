### Preamble {{{
##  ==========================================================================
##        @file Def.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 17:58:38 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-04 Saturday 00:30:44 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DEF_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DEF_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(def VAR)
  if(NOT DEFINED ${VAR})
    set(${VAR} ${ARGN})
  endif()
endmacro()
##
macro(def_bool VAR)
  def(${VAR} ${ARGN})
  if(${VAR})
    set(${VAR} Y)
  else()
    set(${VAR} N)
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
