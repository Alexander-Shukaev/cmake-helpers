### Preamble {{{
##  ==========================================================================
##        @file Linker.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2017-02-26 Sunday 23:01:39 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_LINKER_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_LINKER_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Languages)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
foreach(_L ${LANGUAGES_UPPER})
  if(CMAKE_${_L}_COMPILER_LOADED)
    option_def_bool(LINKER
      "Whether to use linker."
      Y)
    break()
  endif()
endforeach()
##
if(DEFINED LINKER)
  option_def_bool(LINKER_BFD
    "Whether to use the BFD linker instead of the default linker."
    N)
  ##
  option_def_bool(LINKER_GOLD
    "Whether to use the GOLD linker instead of the default linker."
    ${LINKER})
endif()
##
if(LINKER_BFD AND LINKER_GOLD)
  # TODO:
  message(FATAL_ERROR "Cannot use the BFD and GOLD linkers simultaneously.")
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(LINKER_FEATURES BFD GOLD)
set(LINKER_FEATURES_LOWER)
set(LINKER_FEATURES_UPPER)
foreach(_F ${LINKER_FEATURES})
  string(TOLOWER "${_F}" _F_LOWER)
  string(TOUPPER "${_F}" _F_UPPER)
  list(APPEND LINKER_FEATURES_LOWER ${_F_LOWER})
  list(APPEND LINKER_FEATURES_UPPER ${_F_UPPER})
  if(LINKER_${_F_UPPER})
    find_program(LINKER_${_F_UPPER}_COMMAND ld.${_F_LOWER})
    set(_MESSAGES)
    if(NOT LINKER_${_F_UPPER}_COMMAND)
      set(_MESSAGES "Command not found: ${LINKER_${_F_UPPER}_COMMAND}")
    endif()
    if(_MESSAGES AND VERBOSE)
      message(WARNING ${_MESSAGES})
    endif()
    unset(_MESSAGES)
  endif()
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
