### Preamble {{{
##  ==========================================================================
##        @file TargetMakeTest.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-01-19 Thursday 01:43:06 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-01-19 Thursday 01:26:28 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_MAKE_TEST_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_MAKE_TEST_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddBTest)
include(AddBoostTest)
include(Contains)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_make_test TARGET PACKAGE)
  contains(_PACKAGE_IS_TEST_PACKAGE TEST_PACKAGES ${PACKAGE})
  if(_PACKAGE_IS_TEST_PACKAGE)
    if(PACKAGE MATCHES "^BTEST$"     )
      add_btest(${TARGET} ${ARGN})
    endif()
    if(PACKAGE MATCHES "^BOOST_TEST$")
      add_boost_test(${TARGET} ${ARGN})
    endif()
  else()
    message(FATAL_ERROR "Unknown test package: ${PACKAGE}")
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
