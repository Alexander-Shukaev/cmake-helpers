### Preamble {{{
##  ==========================================================================
##        @file GetHiddenAttribute.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:23:58 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-06 Monday 01:26:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GET_HIDDEN_ATTRIBUTE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GET_HIDDEN_ATTRIBUTE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCompilerHiddenVisibilityFlag)
include(DetectHiddenAttributes)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(get_hidden_attribute LANGUAGE VAR)
  cmake_parse_arguments("" "REQUIRED;QUIET" "" "" ${ARGN})
  warn_about_unknown_arguments(0 ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  set(_ATTRIBUTES)
  set(_VAR)
  set(_MESSAGES)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    check_compiler_hidden_visibility_flag(${LANGUAGE} _SUPPORTED)
    if(_SUPPORTED)
      detect_hidden_attributes(${LANGUAGE} _ATTRIBUTES)
    endif()
    if(_ATTRIBUTES)
      list(GET _ATTRIBUTES 0 _VAR)
    else()
      set(_MESSAGES "Hidden attribute not supported: "
        "${CMAKE_${LANGUAGE}_COMPILER}")
    endif()
  else()
    set(_MESSAGES "${LANGUAGE} compiler not loaded")
  endif()
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
