### Preamble {{{
##  ==========================================================================
##        @file AddNoRTTIFlagMaybe.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-20 Saturday 23:10:04 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-20 Saturday 23:09:52 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_NO_RTTI_FLAG_MAYBE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_NO_RTTI_FLAG_MAYBE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(GetNoRTTIOption)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(add_no_rtti_flag_maybe)
  get_no_rtti_option(_OPTION ${ARGN})
  if(_OPTION)
    add_compile_options(${_OPTION})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
