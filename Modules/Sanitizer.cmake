### Preamble {{{
##  ==========================================================================
##        @file Sanitizer.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:37:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2017-02-26 Sunday 19:51:55 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_SANITIZER_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_SANITIZER_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Languages)
include(OptionDef)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
foreach(_L ${LANGUAGES_UPPER})
  if(CMAKE_${_L}_COMPILER_LOADED)
    if(CMAKE_${_L}_COMPILER_ID MATCHES "Clang" OR
       CMAKE_${_L}_COMPILER_ID MATCHES "GNU")
      option_def_bool(SANITIZER
        "Sanitize whatever is possible."
        N)
      break()
    endif()
  endif()
endforeach()
##
if(DEFINED SANITIZER)
  option_def_bool(SANITIZER_ADDRESS
    "Sanitize memory access to detect out-of-bounds and use-after-free bugs."
    N)
  ##
  option_def_bool(SANITIZER_THREAD
    "Sanitize memory access to detect data race bugs."
    ${SANITIZER})
  ##
  option_def_bool(SANITIZER_UNDEFINED
    "Sanitize various computations to detect undefined behavior bugs."
    ${SANITIZER})
  ##
  option_def_bool(SANITIZER_RESET
    "Sanitize reset."
    ${SANITIZER})
  ##
  option_def_bool(SANITIZER_NONE
    "Sanitize none."
    N)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(SANITIZER_FEATURES ADDRESS THREAD UNDEFINED RESET NONE)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
