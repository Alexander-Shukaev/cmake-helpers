### Preamble {{{
##  ==========================================================================
##        @file FindGRPCXX.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:44:49 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-19 Saturday 13:06:15 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(DepGetCXX11StandardOption)
include(DepPkgCheckModules)
include(FindDepPackage)
include(FindPackageHandleStandardArgs)
include(SetTargetProperty)
##  ==========================================================================
##  }}} Modules
##
dep_pkg_check_modules(GRPCXX_PKG grpc++)
find_path(GRPCXX_INCLUDE_DIR
  NAMES grpc++/grpc++.h
  HINTS ${GRPCXX_PKG_INCLUDE_DIRS}
  PATHS ${GRPCXX_INCLUDE_DIR}
        $ENV{GRPCXX_INCLUDE_DIR}
        ${CMAKE_INSTALL_PREFIX}/include
  PATH_SUFFIXES grpc++)
mark_as_advanced(GRPCXX_INCLUDE_DIR)
find_library(GRPCXX_LIBRARY
  NAMES grpc++
  HINTS ${GRPCXX_PKG_LIBRARY_DIRS}
  PATHS ${GRPCXX_LIBRARY_DIR}
        $ENV{GRPCXX_LIBRARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/lib)
mark_as_advanced(GRPCXX_LIBRARY)
set(GRPCXX_DEFINITIONS)
set(GRPCXX_INCLUDE_DIRS ${GRPCXX_INCLUDE_DIR})
set(GRPCXX_LIBRARIES    ${GRPCXX_LIBRARY})
set(GRPCXX_OPTIONS      ${GRPCXX_PKG_CFLAGS_OTHER})
dep_get_cxx11_standard_option(GRPCXX CXX11_STANDARD_OPTION)
if(CXX11_STANDARD_OPTION)
  list(APPEND GRPCXX_OPTIONS ${CXX11_STANDARD_OPTION})
endif()
##
dep_find_package(GRPCXX GRPC QUIET)
if(GRPC_FOUND)
  list(APPEND GRPCXX_DEFINITIONS  ${GRPC_DEFINITIONS})
  list(APPEND GRPCXX_INCLUDE_DIRS ${GRPC_INCLUDE_DIRS})
  list(APPEND GRPCXX_LIBRARIES    ${GRPC_LIBRARIES})
  list(APPEND GRPCXX_OPTIONS      ${GRPC_OPTIONS})
endif()
##
dep_find_package(GRPCXX Protobuf QUIET)
if(Protobuf_FOUND)
  list(APPEND GRPCXX_DEFINITIONS  ${Protobuf_DEFINITIONS})
  list(APPEND GRPCXX_INCLUDE_DIRS ${Protobuf_INCLUDE_DIRS})
  list(APPEND GRPCXX_LIBRARIES    ${Protobuf_LIBRARIES})
  list(APPEND GRPCXX_OPTIONS      ${Protobuf_OPTIONS})
endif()
##
find_package_handle_standard_args(GRPCXX
  FOUND_VAR     GRPCXX_FOUND
  REQUIRED_VARS GRPCXX_LIBRARY
                GRPCXX_LIBRARIES
                GRPCXX_INCLUDE_DIR
                GRPCXX_INCLUDE_DIRS)
##
if(GRPCXX_FOUND AND NOT TARGET GRPCXX)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(GRPCXX INTERFACE IMPORTED)
    set_target_property(GRPCXX
      INTERFACE_COMPILE_DEFINITIONS       ${GRPCXX_DEFINITIONS})
    set_target_property(GRPCXX
      INTERFACE_COMPILE_OPTIONS           ${GRPCXX_OPTIONS})
    set_target_property(GRPCXX
      INTERFACE_INCLUDE_DIRECTORIES       ${GRPCXX_INCLUDE_DIRS})
    set_target_property(GRPCXX
      INTERFACE_LINK_LIBRARIES            ${GRPCXX_LIBRARIES})
    set_target_property(GRPCXX
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
#   set_target_property(GRPCXX
#     INTERFACE_CXX_STANDARD              11)
  endif()
endif()
