### Preamble {{{
##  ==========================================================================
##        @file ExtractPackageDefinition.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:24:56 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-04 Friday 17:52:00 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_EXTRACT_PACKAGE_DEFINITION_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_EXTRACT_PACKAGE_DEFINITION_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(ExtractSourceDefinition)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(extract_package_definition VAR PACKAGE HEADER NAME)
  set(_DEFINITION)
  set(_REQUIRED)
  set(_QUIET)
  if(${PACKAGE}_FIND_REQUIRED)
    set(_REQUIRED REQUIRED)
  endif()
  if(${PACKAGE}_FIND_QUIETLY)
    set(_QUIET QUIET)
  endif()
  foreach(_ARG ${ARGN})
    if    (_ARG MATCHES "^REQUIRED$")
      set(_REQUIRED REQUIRED)
    elseif(_ARG MATCHES "^QUIET$")
      set(_QUIET    QUIET)
    else()
      # TODO:
      message(AUTHOR_WARNING "Unknown argument: ${_ARG}")
    endif()
  endforeach()
  set(_DIR ${${PACKAGE}_INCLUDE_DIR})
  if(EXISTS       "${_DIR}" AND
     IS_DIRECTORY "${_DIR}")
    extract_source_definition(_DEFINITION
      ${_DIR}/${HEADER}
      ${NAME}
      ${_QUIET})
  else()
    set(_MESSAGES "Directory not found: ${_DIR}")
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
  endif()
  if(_DEFINITION MATCHES "^$")
    set(${VAR}                PARENT_SCOPE)
  else()
    set(${VAR} ${_DEFINITION} PARENT_SCOPE)
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
