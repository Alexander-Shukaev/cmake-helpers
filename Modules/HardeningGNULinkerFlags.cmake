### Preamble {{{
##  ==========================================================================
##        @file HardeningGNULinkerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 11:07:31 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-13 Monday 21:56:59 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_HARDENING_GNU_LINKER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_HARDENING_GNU_LINKER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Hardening)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(HARDENING_PIE)
  if    (CMAKE_VERSION VERSION_EQUAL   2.8.9)
    def_bool(CMAKE_POSITION_INDEPENDENT_FLAGS Y)
  elseif(CMAKE_VERSION VERSION_GREATER 2.8.9)
    def_bool(CMAKE_POSITION_INDEPENDENT_CODE  Y)
  else()
    # ...
  endif()
  # As per [1]:
  def(HARDENING_PIE_GNU_LINKER_FLAGS
    -pie)
endif()
##
if(HARDENING_NES)
  def(HARDENING_NES_GNU_LINKER_FLAGS
    # As per [2]:
#   -Wl,-z,noexecstack
    -Wa,--noexecstack)
endif()
##
if(HARDENING_NEH)
  def(HARDENING_NEH_GNU_LINKER_FLAGS
    -Wl,-z,noexecheap)
endif()
##
if(HARDENING_RELRO)
  def(HARDENING_RELRO_GNU_LINKER_FLAGS
    -Wl,-z,relro)
endif()
##
if(HARDENING_NOW)
  def(HARDENING_NOW_GNU_LINKER_FLAGS
    -Wl,-z,now)
endif()
##
def(HARDENING_GNU_EXE_LINKER_FLAGS
  ${HARDENING_PIE_GNU_LINKER_FLAGS})
##
def(HARDENING_GNU_LINKER_FLAGS
  ${HARDENING_NES_GNU_LINKER_FLAGS}
  ${HARDENING_NEH_GNU_LINKER_FLAGS}
  ${HARDENING_RELRO_GNU_LINKER_FLAGS}
  ${HARDENING_NOW_GNU_LINKER_FLAGS})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### References {{{
##  ==========================================================================
##  [1] http://cmake.org/pipermail/cmake-developers/2014-June/010692.html
##  [2] http://gcc.gnu.org/bugzilla/show_bug.cgi?id=52747
##  ==========================================================================
##  }}} References
