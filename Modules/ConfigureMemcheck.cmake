### Preamble {{{
##  ==========================================================================
##        @file ConfigureMemcheck.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-11-12 Saturday 17:07:46 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-05-09 Monday 15:46:31 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CONFIGURE_MEMCHECK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CONFIGURE_MEMCHECK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(ConfigureValgrind)
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(UNIX)
  def(MEMCHECK_ARGS
    --error-exitcode=1
    --free-fill=0xfe
    --gen-suppressions=all
    --leak-check=full
    --malloc-fill=0xff
    --num-callers=64
    --show-reachable=yes
  # --trace-children=yes
    --track-fds=yes
    --track-origins=yes)
else()
  # TODO:
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(configure_memcheck)
  if(UNIX)
    set(_VALGRIND_MEMCHECK_ARGS ${MEMCHECK_ARGS})
    _configure_valgrind_memcheck(${ARGN})
  else()
    # TODO:
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
function(_configure_valgrind_memcheck)
  cmake_parse_arguments("" "SET" "SUPPRESSIONS_FILE" "ARGS" ${ARGN})
  if(NOT _SET)
    set(_ARGS ${_VALGRIND_MEMCHECK_ARGS} ${_ARGS})
  endif()
  configure_valgrind(${_UNPARSED_ARGUMENTS}
    SUPPRESSIONS_FILE ${_SUPPRESSIONS_FILE}
    ARGS --tool=memcheck ${_ARGS})
  set(VALGRIND_COMMAND           ${VALGRIND_COMMAND}
    CACHE FILEPATH
    "Path to the Valgrind command."
    FORCE)
  set(VALGRIND_COMMAND_OPTIONS   ${VALGRIND_COMMAND_OPTIONS}
    CACHE STRING
    "The options for the Valgrind command."
    FORCE)
  set(VALGRIND_SUPPRESSIONS_FILE ${VALGRIND_SUPPRESSIONS_FILE}
    CACHE FILEPATH
    "The file containing a list of suppressions for Valgrind."
    FORCE)
  mark_as_advanced(VALGRIND_COMMAND)
  mark_as_advanced(VALGRIND_COMMAND_OPTIONS)
  mark_as_advanced(VALGRIND_SUPPRESSIONS_FILE)
  set(MEMORYCHECK_COMMAND           ${VALGRIND_COMMAND}
    CACHE FILEPATH
    "Path to the memory check command."
    FORCE)
  set(MEMORYCHECK_COMMAND_OPTIONS   ${VALGRIND_COMMAND_OPTIONS}
    CACHE STRING
    "The options for the memory check command."
    FORCE)
  set(MEMORYCHECK_SUPPRESSIONS_FILE ${VALGRIND_SUPPRESSIONS_FILE}
    CACHE FILEPATH
    "The file containing a list of suppressions for memory check."
    FORCE)
  mark_as_advanced(MEMORYCHECK_COMMAND)
  mark_as_advanced(MEMORYCHECK_COMMAND_OPTIONS)
  mark_as_advanced(MEMORYCHECK_SUPPRESSIONS_FILE)
endfunction()
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Functions
