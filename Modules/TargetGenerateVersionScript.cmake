### Preamble {{{
##  ==========================================================================
##        @file TargetGenerateVersionScript.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:07:27 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-08 Wednesday 00:10:40 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_GENERATE_VERSION_SCRIPT_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_GENERATE_VERSION_SCRIPT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(GenerateVersionScript)
include(GetOptions)
include(Path)
include(TargetLinkVersionScripts)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_generate_version_script TARGET)
  set(_MESSAGES)
  if(TARGET ${TARGET})
    get_target_property(_TYPE ${TARGET} TYPE)
    set(_TYPES EXECUTABLE
               MODULE_LIBRARY
               OBJECT_LIBRARY
               SHARED_LIBRARY
               STATIC_LIBRARY)
    contains(_TYPE_IS_LIBRARY _TYPES ${_TYPE})
    if(NOT _TYPE_IS_LIBRARY)
      set(_MESSAGES "Invalid target type: ${_TYPE}")
    endif()
  else()
    set(_MESSAGES "Unknown target: ${TARGET}")
  endif()
  # --------------------------------------------------------------------------
  set(_OPTIONS  LINK)
  set(_KEYWORDS NAME)
  cmake_parse_arguments("" "${_OPTIONS}" "${_KEYWORDS}" "" ${ARGN})
  # --------------------------------------------------------------------------
  get_options("REQUIRED;QUIET" ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
    return()
  endif()
  # --------------------------------------------------------------------------
  get_target_property(_LANGUAGE ${TARGET} LINKER_LANGUAGE)
  if(NOT _LANGUAGE)
    set(_LANGUAGE CXX)
  endif()
  # --------------------------------------------------------------------------
  if(NOT _NAME)
    path(_NAME ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
               ${TARGET}.dir)
  endif()
  # --------------------------------------------------------------------------
  generate_version_script(_SCRIPT ${_LANGUAGE}
                                  ${_NAME}
                                  ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_LINK)
    target_link_version_scripts(${TARGET} ${_SCRIPT})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
