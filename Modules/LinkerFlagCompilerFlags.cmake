### Preamble {{{
##  ==========================================================================
##        @file LinkerFlagCompilerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-03-03 Friday 00:36:15 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-03-03 Friday 00:11:45 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_LINKER_FLAG_COMPILER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_LINKER_FLAG_COMPILER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Map)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(linker_flag_compiler_flags_def PREFIX FLAG)
  map_def(${PREFIX}_LINKER_FLAG_COMPILER_FLAGS ${FLAG} ${ARGN}
    CACHE INTERNAL
    "Compiler flags for the \"${FLAG}\" linker flag.")
endmacro()
##
macro(linker_flag_compiler_flags_set PREFIX FLAG)
  map_set(${PREFIX}_LINKER_FLAG_COMPILER_FLAGS ${FLAG} ${ARGN}
    CACHE INTERNAL
    "Compiler flags for the \"${FLAG}\" linker flag.")
endmacro()
##
macro(linker_flag_compiler_flags_get PREFIX FLAG VAR)
  map_get(${PREFIX}_LINKER_FLAG_COMPILER_FLAGS ${FLAG} ${VAR} ${ARGN})
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
