### Preamble {{{
##  ==========================================================================
##        @file Debugger.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-10-24 Saturday 19:27:19 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-10 Wednesday 21:57:33 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DEBUGGER_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DEBUGGER_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Languages)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
foreach(_L ${LANGUAGES_UPPER})
  if(CMAKE_${_L}_COMPILER_LOADED)
    if(CMAKE_${_L}_COMPILER_ID MATCHES "Clang" OR
       CMAKE_${_L}_COMPILER_ID MATCHES "GNU"   OR
       CMAKE_${_L}_COMPILER_ID MATCHES "Intel")
      option_def_bool(DEBUGGER
        "Whether to use debugger."
        Y)
      break()
    endif()
  endif()
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(DEBUGGER)
  foreach(_L ${LANGUAGES_UPPER})
    if(CMAKE_${_L}_COMPILER_LOADED)
      if(CMAKE_${_L}_COMPILER_ID MATCHES "Clang")
        find_program(DEBUGGER_COMMAND lldb)
        def(DEBUGGER_ARGS)
        break()
      endif()
      if(CMAKE_${_L}_COMPILER_ID MATCHES "GNU"   OR
         CMAKE_${_L}_COMPILER_ID MATCHES "Intel")
        find_program(DEBUGGER_COMMAND gdb)
        def(DEBUGGER_ARGS)
        break()
      endif()
    endif()
  endforeach()
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Properties {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(DEBUGGER)
  set(_MESSAGES)
  if(DEBUGGER_COMMAND)
    add_custom_target(debugger
      COMMAND ${DEBUGGER_COMMAND} ${DEBUGGER_ARGS}
      VERBATIM)
  else()
    # TODO: `DEBUGGER_COMMAND' is empty:
    set(_MESSAGES "Command not found: ${DEBUGGER_COMMAND}")
  endif()
  if(_MESSAGES AND VERBOSE)
    message(WARNING ${_MESSAGES})
  endif()
  unset(_MESSAGES)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Properties
