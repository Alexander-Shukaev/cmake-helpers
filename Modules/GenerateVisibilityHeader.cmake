### Preamble {{{
##  ==========================================================================
##        @file GenerateVisibilityHeader.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:20:56 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-01 Wednesday 22:50:31 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GENERATE_VISIBILITY_HEADER_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GENERATE_VISIBILITY_HEADER_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Def)
include(GetExportAttribute)
include(GetHiddenAttribute)
include(GetImportAttribute)
include(GetOptions)
include(Languages)
include(Path)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(C_VISIBILITY_HEADER_EXT   .h)
def(CXX_VISIBILITY_HEADER_EXT .hpp)
##
def(VISIBILITY_HEADER_LANGUAGES ${LANGUAGES_UPPER})
##
foreach(_LANGUAGE ${VISIBILITY_HEADER_LANGUAGES})
  find_path(${_LANGUAGE}_VISIBILITY_HEADER_TEMPLATE_DIR
    NAMES visibility${${_LANGUAGE}_VISIBILITY_HEADER_EXT}.in
    PATHS ${Helpers_CMAKE_DIRS}
          ${Helpers_CMAKE_TEMPLATES_DIRS}
    PATH_SUFFIXES Templates)
  find_path(${_LANGUAGE}_VISIBILITY_DEPRECATED_HEADER_TEMPLATE_DIR
    NAMES visibility_deprecated${${_LANGUAGE}_VISIBILITY_HEADER_EXT}.in
    PATHS ${Helpers_CMAKE_DIRS}
          ${Helpers_CMAKE_TEMPLATES_DIRS}
    PATH_SUFFIXES Templates)
  path_def(${_LANGUAGE}_VISIBILITY_HEADER_TEMPLATE
    ${${_LANGUAGE}_VISIBILITY_HEADER_TEMPLATE_DIR}
    visibility${${_LANGUAGE}_VISIBILITY_HEADER_EXT}.in)
  path_def(${_LANGUAGE}_VISIBILITY_DEPRECATED_HEADER_TEMPLATE
    ${${_LANGUAGE}_VISIBILITY_DEPRECATED_HEADER_TEMPLATE_DIR}
    visibility_deprecated${${_LANGUAGE}_VISIBILITY_HEADER_EXT}.in)
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(generate_visibility_header VAR LANGUAGE NAME)
  set(_EXT      ${${LANGUAGE}_VISIBILITY_HEADER_EXT})
  set(_TEMPLATE ${${LANGUAGE}_VISIBILITY_HEADER_TEMPLATE})
  # --------------------------------------------------------------------------
  set(_MESSAGES)
  if(NOT NAME)
    set(_MESSAGES "Invalid argument: ${NAME}")
  endif()
  # --------------------------------------------------------------------------
  set(_OPTIONS  FORCE
                INCLUDE_DEPRECATED_HEADER)
  set(_KEYWORDS PREFIX
                DIR
                SUBDIR
                FILE
                VERSION
                INCLUDE_GUARD_IDENTIFIER
                EXPORT_ATTRIBUTE_IDENTIFIER
                IMPORT_ATTRIBUTE_IDENTIFIER
                HIDDEN_ATTRIBUTE_IDENTIFIER
                STATIC_CONDITION_IDENTIFIER
                BUILD_CONDITION_IDENTIFIER
                GLOBAL_VISIBILITY_IDENTIFIER
                LOCAL_VISIBILITY_IDENTIFIER)
  cmake_parse_arguments("_WITH" "${_KEYWORDS}" ""             "" ${ARGN})
  cmake_parse_arguments(""      "${_OPTIONS}"  "${_KEYWORDS}" "" ${ARGN})
  # --------------------------------------------------------------------------
  get_options("REQUIRED;QUIET" ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
    return()
  endif()
  # --------------------------------------------------------------------------
  if(_INCLUDE_DEPRECATED_HEADER)
    file(READ ${${LANGUAGE}_VISIBILITY_DEPRECATED_HEADER_TEMPLATE}
      _INCLUDE_DEPRECATED_HEADER)
  else()
    set(_INCLUDE_DEPRECATED_HEADER)
  endif()
  # --------------------------------------------------------------------------
  string(TOLOWER "${NAME}" _NAME_LOWER)
  string(TOUPPER "${NAME}" _NAME_UPPER)
  # --------------------------------------------------------------------------
  if(NOT _DIR)
    path(_DIR ${CMAKE_CURRENT_BINARY_DIR} include)
  endif()
  get_filename_component(_DIR ${_DIR} ABSOLUTE)
  # --------------------------------------------------------------------------
  if(NOT _WITH_SUBDIR)
    path(_SUBDIR ${_NAME_LOWER})
  endif()
  if(_SUBDIR AND IS_ABSOLUTE ${_SUBDIR})
    file(RELATIVE_PATH _SUBDIR ${_DIR} ${_SUBDIR})
  endif()
  # --------------------------------------------------------------------------
  if(NOT _FILE)
    path(_FILE visibility${_EXT})
  endif()
  if(NOT IS_ABSOLUTE ${_FILE})
    path(_FILE ${_DIR} ${_SUBDIR} ${_FILE})
  endif()
  set(${VAR} ${_FILE} PARENT_SCOPE)
  if(EXISTS ${_FILE} AND NOT _FORCE)
    return()
  endif()
  # --------------------------------------------------------------------------
  if(NOT _VERSION)
    set(_VERSION ${PROJECT_VERSION})
  endif()
  if(NOT _VERSION)
    set(_VERSION 0.0.0)
  endif()
  # --------------------------------------------------------------------------
  if(NOT _INCLUDE_GUARD_IDENTIFIER)
    string(TOUPPER "${_EXT}" _EXT_UPPER)
    set(_INCLUDE_GUARD_IDENTIFIER
      ${_NAME_UPPER}_VISIBILITY${_EXT_UPPER}_INCLUDED)
  endif()
  set(_INCLUDE_GUARD_IDENTIFIER
    ${_PREFIX}${_INCLUDE_GUARD_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_INCLUDE_GUARD_IDENTIFIER}
    _INCLUDE_GUARD_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _EXPORT_ATTRIBUTE_IDENTIFIER)
    set(_EXPORT_ATTRIBUTE_IDENTIFIER
      ${_NAME_UPPER}_EXPORT)
  endif()
  set(_EXPORT_ATTRIBUTE_IDENTIFIER
    ${_PREFIX}${_EXPORT_ATTRIBUTE_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_EXPORT_ATTRIBUTE_IDENTIFIER}
    _EXPORT_ATTRIBUTE_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _IMPORT_ATTRIBUTE_IDENTIFIER)
    set(_IMPORT_ATTRIBUTE_IDENTIFIER
      ${_NAME_UPPER}_IMPORT)
  endif()
  set(_IMPORT_ATTRIBUTE_IDENTIFIER
    ${_PREFIX}${_IMPORT_ATTRIBUTE_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_IMPORT_ATTRIBUTE_IDENTIFIER}
    _IMPORT_ATTRIBUTE_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _HIDDEN_ATTRIBUTE_IDENTIFIER)
    set(_HIDDEN_ATTRIBUTE_IDENTIFIER
      ${_NAME_UPPER}_HIDDEN)
  endif()
  set(_HIDDEN_ATTRIBUTE_IDENTIFIER
    ${_PREFIX}${_HIDDEN_ATTRIBUTE_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_HIDDEN_ATTRIBUTE_IDENTIFIER}
    _HIDDEN_ATTRIBUTE_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _STATIC_CONDITION_IDENTIFIER)
    set(_STATIC_CONDITION_IDENTIFIER
      ${_PREFIX}${_NAME_UPPER}_STATIC)
  endif()
  string(MAKE_C_IDENTIFIER ${_STATIC_CONDITION_IDENTIFIER}
    _STATIC_CONDITION_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _BUILD_CONDITION_IDENTIFIER)
    set(_BUILD_CONDITION_IDENTIFIER
      ${_PREFIX}${_NAME_UPPER}_BUILD)
  endif()
  string(MAKE_C_IDENTIFIER ${_BUILD_CONDITION_IDENTIFIER}
    _BUILD_CONDITION_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _GLOBAL_VISIBILITY_IDENTIFIER)
    set(_GLOBAL_VISIBILITY_IDENTIFIER
      ${_NAME_UPPER}_GLOBAL)
  endif()
  set(_GLOBAL_VISIBILITY_IDENTIFIER
    ${_PREFIX}${_GLOBAL_VISIBILITY_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_GLOBAL_VISIBILITY_IDENTIFIER}
    _GLOBAL_VISIBILITY_IDENTIFIER)
  # --------------------------------------------------------------------------
  if(NOT _LOCAL_VISIBILITY_IDENTIFIER)
    set(_LOCAL_VISIBILITY_IDENTIFIER
      ${_NAME_UPPER}_LOCAL)
  endif()
  set(_LOCAL_VISIBILITY_IDENTIFIER
    ${_PREFIX}${_LOCAL_VISIBILITY_IDENTIFIER})
  string(MAKE_C_IDENTIFIER ${_LOCAL_VISIBILITY_IDENTIFIER}
    _LOCAL_VISIBILITY_IDENTIFIER)
  # --------------------------------------------------------------------------
  set(INCLUDE_DEPRECATED_HEADER    ${_INCLUDE_DEPRECATED_HEADER})
  get_filename_component(FILE      ${_FILE} NAME)
  set(VERSION                      ${_VERSION})
  set(INCLUDE_GUARD_IDENTIFIER     ${_INCLUDE_GUARD_IDENTIFIER})
  set(EXPORT_ATTRIBUTE_IDENTIFIER  ${_EXPORT_ATTRIBUTE_IDENTIFIER})
  set(IMPORT_ATTRIBUTE_IDENTIFIER  ${_IMPORT_ATTRIBUTE_IDENTIFIER})
  set(HIDDEN_ATTRIBUTE_IDENTIFIER  ${_HIDDEN_ATTRIBUTE_IDENTIFIER})
  set(STATIC_CONDITION_IDENTIFIER  ${_STATIC_CONDITION_IDENTIFIER})
  set(BUILD_CONDITION_IDENTIFIER   ${_BUILD_CONDITION_IDENTIFIER})
  set(GLOBAL_VISIBILITY_IDENTIFIER ${_GLOBAL_VISIBILITY_IDENTIFIER})
  set(LOCAL_VISIBILITY_IDENTIFIER  ${_LOCAL_VISIBILITY_IDENTIFIER})
  # --------------------------------------------------------------------------
  get_export_attribute(${LANGUAGE} EXPORT_ATTRIBUTE ${_UNPARSED_ARGUMENTS})
  get_import_attribute(${LANGUAGE} IMPORT_ATTRIBUTE ${_UNPARSED_ARGUMENTS})
  get_hidden_attribute(${LANGUAGE} HIDDEN_ATTRIBUTE ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  set(CMAKE_DISABLE_IN_SOURCE_BUILD N)
  set(CMAKE_DISABLE_SOURCE_CHANGES  N)
  configure_file(${_TEMPLATE} ${_FILE} @ONLY)
  if(_INCLUDE_DEPRECATED_HEADER)
    # TODO:
    configure_file(${_FILE} ${_FILE} @ONLY)
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
