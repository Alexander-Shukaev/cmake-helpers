### Preamble {{{
##  ==========================================================================
##        @file DepMessage.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 12:25:34 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-06 Sunday 18:07:47 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DEP_MESSAGE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DEP_MESSAGE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(dep_message PACKAGE)
  if(${PACKAGE}_FIND_REQUIRED)
    message(FATAL_ERROR ${ARGN})
  endif()
  if(NOT ${PACKAGE}_FIND_QUIETLY)
    message(WARNING     ${ARGN})
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
