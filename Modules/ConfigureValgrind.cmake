### Preamble {{{
##  ==========================================================================
##        @file ConfigureValgrind.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-10-22 Thursday 15:33:07 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 21:23:06 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CONFIGURE_VALGRIND_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CONFIGURE_VALGRIND_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(Join)
include(Path)
include(Valgrind)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
path_def(VALGRIND_SUPPRESSIONS_FILE_NAME default.supp)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(configure_valgrind)
  if(NOT UNIX)
    # TODO:
    return()
  endif()
  cmake_parse_arguments("" "" "SUPPRESSIONS_FILE" "ARGS" ${ARGN})
  if(NOT _UNPARSED_ARGUMENTS)
    set(_UNPARSED_ARGUMENTS valgrind)
  endif()
  if(_SUPPRESSIONS_FILE)
    get_filename_component(_SUPPRESSIONS_FILE ${_SUPPRESSIONS_FILE}
      ABSOLUTE)
  else()
    file(TO_CMAKE_PATH ${VALGRIND_SUPPRESSIONS_FILE_NAME}
      _SUPPRESSIONS_FILE_NAME)
    if(NOT _SUPPRESSIONS_FILE_NAME)
      set(_SUPPRESSIONS_FILE_NAME default.supp)
    endif()
    if(IS_ABSOLUTE ${_SUPPRESSIONS_FILE_NAME})
      set(_SUPPRESSIONS_FILE ${_SUPPRESSIONS_FILE_NAME})
    else()
      foreach(_DIR ${CMAKE_CURRENT_SOURCE_DIR}
                   ${PROJECT_SOURCE_DIR}
                   ${CMAKE_SOURCE_DIR})
        foreach(_FILE ${_SUPPRESSIONS_FILE_NAME}
                      valgrind/${_SUPPRESSIONS_FILE_NAME}
                      lib/valgrind/${_SUPPRESSIONS_FILE_NAME}
                      share/valgrind/${_SUPPRESSIONS_FILE_NAME})
          file(TO_CMAKE_PATH ${_DIR}/${_FILE} _SUPPRESSIONS_FILE)
          if(EXISTS           "${_SUPPRESSIONS_FILE}" AND
             NOT IS_DIRECTORY "${_SUPPRESSIONS_FILE}")
            break()
          else()
            set(_SUPPRESSIONS_FILE)
          endif()
        endforeach()
        if(EXISTS           "${_SUPPRESSIONS_FILE}" AND
           NOT IS_DIRECTORY "${_SUPPRESSIONS_FILE}")
          break()
        else()
          set(_SUPPRESSIONS_FILE)
        endif()
      endforeach()
    endif()
  endif()
  contains(_Q       _ARGS -q)
  contains(_QUIET   _ARGS --quiet)
  contains(_V       _ARGS -v)
  contains(_VERBOSE _ARGS --verbose)
  if(NOT _Q AND NOT _QUIET   AND
     NOT _V AND NOT _VERBOSE)
    if(VALGRIND_QUIET)
      list(APPEND _ARGS --quiet)
    endif()
    if(VALGRIND_VERBOSE)
      list(APPEND _ARGS --verbose)
    endif()
  endif()
  find_program(_CONFIGURE_VALGRIND_PROGRAM NAMES ${_UNPARSED_ARGUMENTS})
  set(_PROGRAM ${_CONFIGURE_VALGRIND_PROGRAM})
  unset(_CONFIGURE_VALGRIND_PROGRAM CACHE)
  if(_PROGRAM)
    set(VALGRIND_COMMAND ${_PROGRAM} PARENT_SCOPE)
  else()
    set(VALGRIND_COMMAND             PARENT_SCOPE)
  endif()
  if(_SUPPRESSIONS_FILE)
    set(VALGRIND_SUPPRESSIONS_FILE ${_SUPPRESSIONS_FILE} PARENT_SCOPE)
    if(CMAKE_VERSION VERSION_LESS 3.1)
      string(REPLACE " " "\\ " _SUPPRESSIONS_FILE "${_SUPPRESSIONS_FILE}")
      list(APPEND _ARGS --suppressions=${_SUPPRESSIONS_FILE})
    endif()
  else()
    set(VALGRIND_SUPPRESSIONS_FILE                       PARENT_SCOPE)
  endif()
  join(_OPTIONS DELIMITER " " ${_ARGS})
  if(_OPTIONS)
    set(VALGRIND_COMMAND_OPTIONS ${_OPTIONS} PARENT_SCOPE)
  else()
    set(VALGRIND_COMMAND_OPTIONS             PARENT_SCOPE)
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
