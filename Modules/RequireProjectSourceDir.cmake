### Preamble {{{
##  ==========================================================================
##        @file RequireProjectSourceDir.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 01:36:43 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-02-02 Tuesday 19:03:20 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_REQUIRE_PROJECT_SOURCE_DIR_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_REQUIRE_PROJECT_SOURCE_DIR_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(RequireProject)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(require_project_source_dir)
  require_project()
  # --------------------------------------------------------------------------
  if(NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR)
    message(FATAL_ERROR
      "Invalid directory: ${CMAKE_CURRENT_SOURCE_DIR}"
      "\n"
      "The corresponding code must be executed from the top-level source"
      " "
      "directory"
      "\n"
      "  ${PROJECT_SOURCE_DIR}"
      "\n"
      "for the current project"
      "\n"
      "  ${PROJECT}.")
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
