### Preamble {{{
##  ==========================================================================
##        @file DetectCXX11StandardFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:08:55 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-30 Wednesday 21:21:33 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DETECT_CXX11_STANDARD_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DETECT_CXX11_STANDARD_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(FilterCXXCompilerFlags)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(detect_cxx11_standard_flags VAR)
  filter_cxx_compiler_flags(${VAR}
    -std=c++11           # |Clang|    |   |GNU|  |IBM|Intel|Oracle|   |
    -std=c++0x           # |Clang|    |   |GNU|  |IBM|Intel|Oracle|   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -std=gnu++11         # |Clang|    |   |GNU|  |IBM|Intel|      |   |
    -std=gnu++0x         # |Clang|    |   |GNU|  |IBM|Intel|      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    /Qstd=c++11          # |     |    |   |   |  |   |Intel|      |   |
    /Qstd=c++0x          # |     |    |   |   |  |   |Intel|      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    /Qstd=gnu++11        # |     |    |   |   |  |   |Intel|      |   |
    /Qstd=gnu++0x        # |     |    |   |   |  |   |Intel|      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    --c++11              # |     |    |EDG|   |  |   |     |      |PGI|
    --c++0x              # |     |    |EDG|   |  |   |     |      |PGI|
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -c++11               # |     |    |   |   |  |   |     |      |   |
    -c++0x               # |     |    |   |   |  |   |     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -qlanglvl=extended0x # |     |    |   |   |  |IBM|     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -h std=c++11         # |     |Cray|   |   |  |   |     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -Ax                  # |     |    |   |   |HP|   |     |      |   |
    +std=c++11           # |     |    |   |   |HP|   |     |      |   |
    )
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
