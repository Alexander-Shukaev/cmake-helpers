### Preamble {{{
##  ==========================================================================
##        @file UseQt5.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-03 Sunday 19:53:56 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-11-05 Thursday 02:40:57 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Prerequisites {{{
##  ==========================================================================
if(CMAKE_VERSION VERSION_LESS "2.8.12")
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_use_qt5 TARGET SCOPE)
  if(NOT TARGET "${TARGET}")
    message(FATAL_ERROR "Unknown target: ${TARGET}")
  endif()
  if(SCOPE MATCHES "^PUBLIC$"       OR
     SCOPE MATCHES "^PRIVATE$"      OR
     SCOPE MATCHES "^INTERFACE$"    OR
     ##
     SCOPE MATCHES "^LINK_PUBLIC$"  OR
     SCOPE MATCHES "^LINK_PRIVATE$")
    set(COMPONENTS ${ARGN})
  else()
    set(COMPONENTS "${SCOPE}" ${ARGN})
    unset(SCOPE)
  endif()
  list(REMOVE_DUPLICATES COMPONENTS)
  list(LENGTH            COMPONENTS LENGTH)
  if(NOT LENGTH)
    message(FATAL_ERROR "Specify at least one Qt 5 component")
  endif()
  foreach(COMPONENT IN LISTS COMPONENTS)
    if(NOT Qt5${COMPONENT}_FOUND)
      find_package("Qt5${COMPONENT}" REQUIRED)
      get_target_property(LOCATION "Qt5::${COMPONENT}" LOCATION)
      message(STATUS "Found Qt 5 ${COMPONENT}: ${LOCATION}")
    endif()
    if(Qt5_POSITION_INDEPENDENT_CODE)
      set_target_properties("${TARGET}"
        PROPERTIES POSITION_INDEPENDENT_CODE
        "${Qt5_POSITION_INDEPENDENT_CODE}")
    endif()
    target_compile_definitions("${TARGET}" "${SCOPE}"
      ${Qt5${COMPONENT}_COMPILE_DEFINITIONS}
      $<$<NOT:$<CONFIG:Debug>>:QT_NO_DEBUG>)
    target_include_directories("${TARGET}" SYSTEM "${SCOPE}"
      ${Qt5${COMPONENT}_INCLUDE_DIRS})
    target_link_libraries("${TARGET}" "${SCOPE}"
      ${Qt5${COMPONENT}_LIBRARIES})
  endforeach()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
