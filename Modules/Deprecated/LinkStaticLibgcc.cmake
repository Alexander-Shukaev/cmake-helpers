### Preamble {{{
##  ==========================================================================
##        @file LinkStaticLibgcc.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-03 Sunday 19:54:43 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-11-05 Thursday 02:24:36 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Prerequisites {{{
##  ==========================================================================
if(CMAKE_VERSION VERSION_LESS "2.8.12")
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Modules {{{
##  ==========================================================================
include("CheckCCompilerFlag")
include("CheckCXXCompilerFlag")
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_link_static_libgcc TARGET SCOPE)
  if(NOT TARGET "${TARGET}")
    message(FATAL_ERROR "Unknown target: ${TARGET}")
  endif()
  if(SCOPE MATCHES "^PUBLIC$"       OR
     SCOPE MATCHES "^PRIVATE$"      OR
     SCOPE MATCHES "^INTERFACE$"    OR
     ##
     SCOPE MATCHES "^LINK_PUBLIC$"  OR
     SCOPE MATCHES "^LINK_PRIVATE$")
  else()
    unset(SCOPE)
  endif()
  if(CMAKE_C_COMPILER_LOADED)
    check_c_compiler_flag("-static-libgcc" C_COMPILER_FLAG_static_libgcc)
  endif()
  if(CMAKE_CXX_COMPILER_LOADED)
    check_cxx_compiler_flag("-static-libgcc" CXX_COMPILER_FLAG_static_libgcc)
  endif()
  get_target_property(LANGUAGE "${TARGET}" LINKER_LANGUAGE)
  if(LANGUAGE MATCHES "^C$")
    if(C_COMPILER_FLAG_static_libgcc)
      target_link_libraries("${TARGET}" "${SCOPE}" "-static-libgcc")
    endif()
  endif()
  if(LANGUAGE MATCHES "^CXX$")
    if(CXX_COMPILER_FLAG_static_libgcc)
      target_link_libraries("${TARGET}" "${SCOPE}" "-static-libgcc")
    endif()
  endif()
  if(NOT LANGUAGE)
    if(C_COMPILER_FLAG_static_libgcc OR CXX_COMPILER_FLAG_static_libgcc)
      target_link_libraries("${TARGET}" "${SCOPE}" "-static-libgcc")
    endif()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
