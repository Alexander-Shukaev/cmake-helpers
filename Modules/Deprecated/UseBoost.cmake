### Preamble {{{
##  ==========================================================================
##        @file UseBoost.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-03 Sunday 19:53:12 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-11-05 Thursday 01:27:25 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Prerequisites {{{
##  ==========================================================================
if(CMAKE_VERSION VERSION_LESS "3.1.0")
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_use_boost TARGET SCOPE)
  if(NOT TARGET "${TARGET}")
    message(FATAL_ERROR "Unknown target: ${TARGET}")
  endif()
  if(SCOPE MATCHES "^PUBLIC$"       OR
     SCOPE MATCHES "^PRIVATE$"      OR
     SCOPE MATCHES "^INTERFACE$"    OR
     ##
     SCOPE MATCHES "^LINK_PUBLIC$"  OR
     SCOPE MATCHES "^LINK_PRIVATE$")
    set(COMPONENTS ${ARGN})
  else()
    set(COMPONENTS "${SCOPE}" ${ARGN})
    unset(SCOPE)
  endif()
  list(REMOVE_DUPLICATES COMPONENTS)
  if(ENV{Boost_INCLUDE_DIR})
    set(BOOST_INCLUDEDIR "$ENV{Boost_INCLUDE_DIR}")
  endif()
  if(ENV{Boost_LIBRARY_DIR})
    set(BOOST_LIBRARYDIR "$ENV{Boost_LIBRARY_DIR}")
  endif()
  if(Boost_INCLUDE_DIR)
    set(BOOST_INCLUDEDIR "${Boost_INCLUDE_DIR}")
  endif()
  if(Boost_LIBRARY_DIR)
    set(BOOST_LIBRARYDIR "${Boost_LIBRARY_DIR}")
  endif()
  foreach(COMPONENT IN LISTS COMPONENTS)
    string(TOUPPER "${COMPONENT}" C)
    if(NOT Boost_${C}_FOUND)
      find_package("Boost" REQUIRED COMPONENTS "${COMPONENT}")
    endif()
    if(COMPONENT MATCHES "^filesystem$")
      target_compile_definitions("${TARGET}" "${SCOPE}"
        BOOST_FILESYSTEM_NO_DEPRECATED)
    endif()
    if(COMPONENT MATCHES "^thread$")
      target_compile_definitions("${TARGET}" "${SCOPE}"
        BOOST_THREAD_VERSION=4)
    endif()
    target_link_libraries("${TARGET}" "${SCOPE}" "${Boost_${C}_LIBRARY}")
  endforeach()
  if(NOT Boost_FOUND)
    find_package("Boost" REQUIRED)
  endif()
  target_include_directories("${TARGET}" SYSTEM "${SCOPE}"
    ${Boost_INCLUDE_DIRS})
  if(NOT Threads_FOUND)
    set(THREADS_PREFER_PTHREAD_FLAG Y)
    if(Boost_USE_MULTITHREADED)
      find_package("Threads" REQUIRED)
    else()
      find_package("Threads")
    endif()
  endif()
  if(Threads_FOUND)
    target_link_libraries("${TARGET}" "${SCOPE}" "Threads::Threads")
  endif()
  if(WIN32)
    target_link_libraries("${TARGET}" "${SCOPE}" "ws2_32")
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
