### Preamble {{{
##  ==========================================================================
##        @file TargetSymlink.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:18:52 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-28 Monday 20:00:35 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_SYMLINK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_SYMLINK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Prerequisites {{{
##  ==========================================================================
if(CMAKE_VERSION VERSION_LESS 2.8.4)
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Install)
include(Path)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_symlink TARGET)
  set(_MESSAGES)
  if(NOT TARGET ${TARGET})
    set(_MESSAGES "Unknown target: ${TARGET}")
  endif()
  # --------------------------------------------------------------------------
  set(_SV_KEYWORDS CASE
                   NAME)
  set(_MV_KEYWORDS INSTALL)
  cmake_parse_arguments("_WITH"
                        "${SV_KEYWORDS};${_MV_KEYWORDS}"
                        ""
                        ""
                        ${ARGN})
  cmake_parse_arguments(""
                        ""
                        "${_SV_KEYWORDS}"
                        "${_MV_KEYWORDS}"
                        ${ARGN})
  # --------------------------------------------------------------------------
  if(_WITH_CASE)
    set(_CASES LOWER
               UPPER)
    contains(_CASE_IS_CASE _CASES ${_CASE})
    if(NOT _CASE_IS_CASE)
      set(_MESSAGES "Unknown case: ${_CASE}")
    endif()
  endif()
  # --------------------------------------------------------------------------
  if(NOT _NAME)
    if(_WITH_NAME)
      set(_MESSAGES "Invalid name: ${_NAME}")
    else()
      if(_CASE)
        set(_NAME \$<${_CASE}_CASE:\$<TARGET_FILE_NAME:${TARGET}>>)
      else()
        set(_NAME \$<TARGET_FILE_NAME:${TARGET}>)
      endif()
    endif()
  endif()
  get_filename_component(_DIR ${_NAME} PATH)
  if(_DIR)
    set(_MESSAGES "Invalid name: ${_NAME}")
  endif()
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "REQUIRED;QUIET" "" "" ${_UNPARSED_ARGUMENTS})
  warn_about_unknown_arguments(0 ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
    return()
  endif()
  # --------------------------------------------------------------------------
  if(NOT UNIX)
    # TODO:
    return()
  endif()
  # --------------------------------------------------------------------------
  path(_LINK \$<TARGET_FILE_DIR:${TARGET}> ${_NAME})
  # --------------------------------------------------------------------------
  add_custom_command(TARGET ${TARGET} POST_BUILD
    COMMAND ${CMAKE_COMMAND}
    ARGS -E create_symlink
            $<TARGET_FILE_NAME:${TARGET}>
            ${_LINK}
  # BYPRODUCTS ${_LINK}
    VERBATIM)
  # --------------------------------------------------------------------------
  if(_WITH_INSTALL)
    get_target_property(_TYPE ${TARGET} TYPE)
    if(NOT _INSTALL)
      set(_KIND ${INSTALL_${_TYPE}_KIND})
      if(_KIND)
        set(_VAR INSTALL_${_KIND}_DIR)
        if    (NOT DEFINED ${_VAR})
          set(_MESSAGES "Variable not set: ${_VAR}")
        elseif(NOT         ${_VAR})
          set(_MESSAGES "Variable empty: ${_VAR}")
        endif()
      else()
        set(_MESSAGES "No install kind for target type: ${_TYPE}")
      endif()
      if(_MESSAGES)
        if(_REQUIRED AND PROJECT_STANDALONE)
          message(FATAL_ERROR ${_MESSAGES})
        endif()
        if(NOT _QUIET)
          message(WARNING     ${_MESSAGES})
        endif()
        return()
      endif()
      set(_INSTALL DESTINATION ${${_VAR}})
    endif()
    if(_TYPE MATCHES "^EXECUTABLE$")
      install(PROGRAMS ${_LINK} ${_INSTALL})
    else()
      install(FILES    ${_LINK} ${_INSTALL})
    endif()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
