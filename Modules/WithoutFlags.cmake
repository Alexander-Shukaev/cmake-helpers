### Preamble {{{
##  ==========================================================================
##        @file WithoutFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-01-19 Thursday 21:25:41 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-09-18 Sunday 21:07:10 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_WITHOUT_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_WITHOUT_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Configurations)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(without_flags_begin)
  set(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES ${ARGN})
  foreach(_L ${_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES})
    if(CMAKE_${_L}_COMPILER_LOADED)
      set(_WITHOUT_FLAGS_VAR CMAKE_${_L}_FLAGS)
      set(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}
        ${${_WITHOUT_FLAGS_VAR}})
      unset(${_WITHOUT_FLAGS_VAR})
      foreach(_C ${CONFIGURATIONS_UPPER})
        set(_WITHOUT_FLAGS_VAR CMAKE_${_L}_FLAGS_${_C})
        set(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}
          ${${_WITHOUT_FLAGS_VAR}})
        unset(${_WITHOUT_FLAGS_VAR})
        foreach(_T EXE
                   MODULE
                   SHARED
                   STATIC)
          set(_WITHOUT_FLAGS_VAR CMAKE_${_T}_LINKER_FLAGS_${_C})
          set(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}
            ${${_WITHOUT_FLAGS_VAR}})
          unset(${_WITHOUT_FLAGS_VAR})
        endforeach()
      endforeach()
    endif()
  endforeach()
  unset(_WITHOUT_FLAGS_VAR)
  math(EXPR _WITHOUT_FLAGS_INDEX "${_WITHOUT_FLAGS_INDEX} + 1")
endmacro()
##
macro(without_flags_end)
  math(EXPR _WITHOUT_FLAGS_INDEX "${_WITHOUT_FLAGS_INDEX} - 1")
  if(NOT DEFINED _WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES OR
     (NOT _WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES STREQUAL "${ARGN}"
      AND ${ARGC} GREATER 0))
    message(FATAL_ERROR "without_flags_end A WITHOUT_FLAGS_END command was "
                        "found outside of a proper WITHOUT_FLAGS_BEGIN "
                        "WITHOUT_FLAGS_END structure.  Or its arguments did "
                        "not match the opening WITHOUT_FLAGS_BEGIN command.")
  endif()
# if(${ARGC} GREATER 0)
#   set(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES ${ARGN})
# endif()
  foreach(_L ${_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_LANGUAGES})
    if(CMAKE_${_L}_COMPILER_LOADED)
      set(_WITHOUT_FLAGS_VAR CMAKE_${_L}_FLAGS)
      set(${_WITHOUT_FLAGS_VAR}
        ${_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}})
      unset(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR})
      foreach(_C ${CONFIGURATIONS_UPPER})
        set(_WITHOUT_FLAGS_VAR CMAKE_${_L}_FLAGS_${_C})
        set(${_WITHOUT_FLAGS_VAR}
          ${_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}})
        unset(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR})
        foreach(_T EXE
                   MODULE
                   SHARED
                   STATIC)
          set(_WITHOUT_FLAGS_VAR CMAKE_${_T}_LINKER_FLAGS_${_C})
          set(${_WITHOUT_FLAGS_VAR}
            ${_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR}})
          unset(_WITHOUT_FLAGS_${_WITHOUT_FLAGS_INDEX}_${_WITHOUT_FLAGS_VAR})
        endforeach()
      endforeach()
    endif()
  endforeach()
  unset(_WITHOUT_FLAGS_LANGUAGES)
  unset(_WITHOUT_FLAGS_VAR)
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
##
### Variables {{{
##  ==========================================================================
### Private {{{
##  ==========================================================================
set(_WITHOUT_FLAGS_INDEX 0)
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Variables
