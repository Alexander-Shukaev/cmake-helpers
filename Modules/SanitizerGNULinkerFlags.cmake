### Preamble {{{
##  ==========================================================================
##        @file SanitizerGNULinkerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-03-03 Friday 00:38:27 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-02-28 Tuesday 01:02:37 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_SANITIZER_GNU_LINKER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_SANITIZER_GNU_LINKER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(LinkerFlagCompilerFlags)
include(Sanitizer)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(SANITIZER_ADDRESS)
  def(SANITIZER_ADDRESS_GNU_LINKER_FLAGS
    -fsanitize=address)
  linker_flag_compiler_flags_def(CHECK
    -fsanitize=address
    -fsanitize=address)
endif()
##
if(SANITIZER_THREAD)
  def(SANITIZER_THREAD_GNU_LINKER_FLAGS
    -fsanitize=thread)
  linker_flag_compiler_flags_def(CHECK
    -fsanitize=thread
    -fsanitize=thread)
endif()
##
if(SANITIZER_UNDEFINED)
  def(SANITIZER_UNDEFINED_GNU_LINKER_FLAGS
    -fsanitize=undefined)
  linker_flag_compiler_flags_def(CHECK
    -fsanitize=undefined
    -fsanitize=undefined)
endif()
##
if(SANITIZER_RESET)
  def(SANITIZER_RESET_GNU_LINKER_FLAGS
    -fno-sanitize=all)
endif()
##
if(SANITIZER_NONE)
  def(SANITIZER_NONE_GNU_LINKER_FLAGS
    -fno-sanitize=all)
endif()
##
if(SANITIZER_NONE)
  def(SANITIZER_NONE_GNU_LINKER_FLAGS
    ${SANITIZER_NONE})
else()
  def(SANITIZER_GNU_LINKER_FLAGS
    ${SANITIZER_RESET_GNU_LINKER_FLAGS}
    ${SANITIZER_ADDRESS_GNU_LINKER_FLAGS}
    ${SANITIZER_THREAD_GNU_LINKER_FLAGS}
    ${SANITIZER_UNDEFINED_GNU_LINKER_FLAGS})
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
