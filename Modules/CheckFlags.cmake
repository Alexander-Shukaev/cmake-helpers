### Preamble {{{
##  ==========================================================================
##        @file CheckFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:18:25 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-02 Thursday 20:11:28 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CHECK_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CHECK_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeCheckCompilerFlagCommonPatterns)
include(CMakeParseArguments)
include(CheckSourceBuilds)
include(StripGenex)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(check_flags LANGUAGE VAR)
  set(_VAR)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    set(_SV_KEYWORDS CODE)
    set(_MV_KEYWORDS COMPILER
                     LINKER)
    cmake_parse_arguments("" "" "${_SV_KEYWORDS}" "${_MV_KEYWORDS}" ${ARGN})
    if(NOT _CODE)
      set(_CODE
        "
int
main() {
  return 0;
}
")
    endif()
    unset(CMAKE_REQUIRED_FLAGS)
    unset(CMAKE_REQUIRED_DEFINITIONS)
    unset(CMAKE_REQUIRED_INCLUDES)
    unset(CMAKE_REQUIRED_LIBRARIES)
    foreach(_FLAG ${_COMPILER})
      strip_genex(_FLAG ${_FLAG} RECURSE)
      list(APPEND CMAKE_REQUIRED_DEFINITIONS ${_FLAG})
    endforeach()
    foreach(_FLAG ${_LINKER})
      strip_genex(_FLAG ${_FLAG} RECURSE)
      if(NOT CMAKE_VERSION VERSION_LESS 2.8.11)
        list(APPEND CMAKE_REQUIRED_LIBRARIES ${_FLAG})
      else()
        list(APPEND CMAKE_REQUIRED_FLAGS     ${_FLAG})
      endif()
    endforeach()
    args(CMAKE_REQUIRED_FLAGS ${CMAKE_REQUIRED_FLAGS})
    check_compiler_flag_common_patterns(_PATTERNS)
    if(LANGUAGE MATCHES "^C$"  )
      list(APPEND _PATTERNS
        FAIL_REGEX
        "command line option .* is valid for .* but not for C")
    endif()
    if(LANGUAGE MATCHES "^CXX$")
      list(APPEND _PATTERNS
        FAIL_REGEX
        "command line option .* is valid for .* but not for C\\\\+\\\\+")
    endif()
    set(_ENV_VARS LANG
                  LC_ALL
                  LC_MESSAGES)
    foreach(_ENV_VAR ${_ENV_VARS})
      set(_ENV_${_VAR} $ENV{${_ENV_VAR}})
      set(ENV{${_ENV_VAR}} C)
    endforeach()
    check_source_builds(${LANGUAGE} "${_CODE}" _VAR
      ${_UNPARSED_ARGUMENTS} ${_PATTERNS})
    foreach(_ENV_VAR ${_ENV_VARS})
      set(ENV{${_ENV_VAR}} ${_ENV_${_ENV_VAR}})
    endforeach()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
