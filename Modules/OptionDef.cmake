### Preamble {{{
##  ==========================================================================
##        @file OptionDef.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-12-07 Monday 12:02:25 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-04 Saturday 00:30:44 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_OPTION_DEF_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_OPTION_DEF_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(option_def VAR DOC)
  if(DEFINED ${VAR})
    # ------------------------------------------------------------------------
    unset(${VAR})
    option(${VAR} ${DOC} ${${VAR}})
    #
    # NOTE:
    #
    # If the cache entry does not exist prior to the call or the `FORCE'
    # option is given then the cache entry will be set to the given value.
    # Furthermore, any normal variable binding in the current scope will be
    # removed to expose the newly cached value to any immediately following
    # evaluation.
    # ------------------------------------------------------------------------
  else()
    option(${VAR} ${DOC} ${ARGN})
  endif()
endmacro()
##
macro(option_def_bool VAR DOC)
  if(DEFINED ${VAR})
    # ------------------------------------------------------------------------
    if(${VAR})
      unset(${VAR})
      option(${VAR} ${DOC} Y)
    else()
      unset(${VAR})
      option(${VAR} ${DOC} N)
    endif()
    #
    # NOTE:
    #
    # If the cache entry does not exist prior to the call or the `FORCE'
    # option is given then the cache entry will be set to the given value.
    # Furthermore, any normal variable binding in the current scope will be
    # removed to expose the newly cached value to any immediately following
    # evaluation.
    # ------------------------------------------------------------------------
  else()
    if("${ARGN}")
      option(${VAR} ${DOC} Y)
    else()
      option(${VAR} ${DOC} N)
    endif()
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
