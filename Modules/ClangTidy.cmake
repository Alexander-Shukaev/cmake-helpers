### Preamble {{{
##  ==========================================================================
##        @file ClangTidy.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-10 Wednesday 21:57:33 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CLANG_TIDY_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CLANG_TIDY_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Languages)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
foreach(_L ${LANGUAGES_UPPER})
  if(CMAKE_${_L}_COMPILER_LOADED)
    option_def_bool(CLANG_TIDY
      "Whether to use Clang-Tidy."
      N)
    break()
  endif()
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CLANG_TIDY)
  find_program(CLANG_TIDY_COMMAND clang-tidy)
  def(CLANG_TIDY_ARGS
    -enable-check-profile)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Properties {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CLANG_TIDY)
  set(_MESSAGES)
  if(CLANG_TIDY_COMMAND)
    if(NOT CMAKE_VERSION VERSION_LESS 3.6)
      foreach(_L ${LANGUAGES_UPPER})
        if(CMAKE_${_L}_COMPILER_LOADED)
          set(CMAKE_${_L}_CLANG_TIDY
            ${CLANG_TIDY_COMMAND}
            ${CLANG_TIDY_ARGS})
        endif()
      endforeach()
    else()
      set(_MESSAGES "Low CMake version: ${CMAKE_VERSION}")
    endif()
  else()
    set(_MESSAGES "Command not found: ${CLANG_TIDY_COMMAND}")
  endif()
  if(_MESSAGES AND VERBOSE)
    message(WARNING ${_MESSAGES})
  endif()
  unset(_MESSAGES)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Properties
