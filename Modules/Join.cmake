### Preamble {{{
##  ==========================================================================
##        @file Join.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-04 Saturday 14:41:20 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-11-05 Thursday 02:03:23 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_JOIN_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_JOIN_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(JOIN_DELIMITER " ")
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(join VAR)
  cmake_parse_arguments("_WITH" "DELIMITER" ""          "" ${ARGN})
  cmake_parse_arguments(""      ""          "DELIMITER" "" ${ARGN})
  if(NOT _WITH_DELIMITER)
    set(_DELIMITER ${JOIN_DELIMITER})
  endif()
  string(REPLACE "\\" "\\\\" _DELIMITER "${_DELIMITER}")
  set(_VAR ${_UNPARSED_ARGUMENTS})
  string(REGEX REPLACE "([^\\]|^);" "\\1${_DELIMITER}" _VAR "${_VAR}")
  string(REGEX REPLACE "[\\](.)"    "\\1"              _VAR "${_VAR}")
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
