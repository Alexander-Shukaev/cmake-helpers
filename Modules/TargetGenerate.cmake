### Preamble {{{
##  ==========================================================================
##        @file TargetGenerate.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-04 Saturday 14:36:13 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-18 Saturday 10:16:49 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_GENERATE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_GENERATE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(TargetGenerateVersionScript)
include(TargetGenerateVisibilityHeader)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_generate TARGET)
  set(_KEYWORDS VERSION_SCRIPT
                VISIBILITY_HEADER)
  cmake_parse_arguments("" "" "" "${_KEYWORDS}" ${ARGN})
  # --------------------------------------------------------------------------
  if(_VERSION_SCRIPT)
    target_generate_version_script(${TARGET} ${_VERSION_SCRIPT})
  endif()
  if(_VISIBILITY_HEADER)
    target_generate_visibility_header(${TARGET} ${_VISIBILITY_HEADER})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
