### Preamble {{{
##  ==========================================================================
##        @file AddBoostTest.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 12:05:06 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-07 Saturday 20:28:46 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_BOOST_TEST_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_BOOST_TEST_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddTestTarget)
include(CMakeParseArguments)
include(Debugger)
include(Def)
include(FindPackageWithoutFlags)
include(Path)
include(QuoteRegex)
include(RequireExecutable)
include(RequireProject)
include(RequireTarget)
include(Target)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def_bool(BOOST_TEST_USE_MULTITHREADED Y)
def_bool(BOOST_TEST_USE_STATIC_LIBS   N)
def(BOOST_TEST_PARAMS
  AUTO_START_DBG
  BUILD_INFO
  CATCH_SYSTEM_ERRORS
  COLOR_OUTPUT
  DETECT_FP_EXCEPTIONS
  LOG_FORMAT
  LOG_LEVEL
  LOG_SINK
  RANDOM
  REPORT_FORMAT
  REPORT_LEVEL
  REPORT_SINK
  RESULT_CODE)
list(SORT BOOST_TEST_PARAMS)
foreach(_PARAM ${BOOST_TEST_PARAMS})
  # As per [1]:
  if(DEFINED ENV{BOOST_TEST_${_PARAM}})
    def(BOOST_TEST_${_PARAM} $ENV{BOOST_TEST_${_PARAM}})
  endif()
endforeach()
def(BOOST_TEST_AUTO_START_DBG       no)
def(BOOST_TEST_BUILD_INFO           yes)
def(BOOST_TEST_CATCH_SYSTEM_ERRORS  yes)
def(BOOST_TEST_COLOR_OUTPUT         yes)
def(BOOST_TEST_DETECT_FP_EXCEPTIONS yes)
def(BOOST_TEST_LOG_FORMAT           HRF)
def(BOOST_TEST_LOG_LEVEL            test_suite)
def(BOOST_TEST_LOG_SINK             stdout)
def(BOOST_TEST_RANDOM               1)
def(BOOST_TEST_REPORT_FORMAT        XML)
def(BOOST_TEST_REPORT_LEVEL         detailed)
def(BOOST_TEST_REPORT_SINK          \$<NAME>.report.xml)
def(BOOST_TEST_RESULT_CODE          yes)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(add_boost_test TARGET)
  require_executable(${TARGET})
  require_project()
  # --------------------------------------------------------------------------
  if(DEFINED BUILD_TESTING AND NOT BUILD_TESTING)
    return()
  endif()
  if(NOT PROJECT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    return()
  endif()
  if(CMAKE_CURRENT_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR)
    enable_testing()
  else()
    set(_DART_CONFIGURATION_FILE_NAME DartConfiguration.tcl)
    file(RELATIVE_PATH _DART_CONFIGURATION_FILE
      ${CMAKE_CURRENT_BINARY_DIR}
      ${PROJECT_BINARY_DIR}/${_DART_CONFIGURATION_FILE_NAME})
    if(UNIX)
      add_custom_command(TARGET ${TARGET} POST_BUILD
        COMMAND ${CMAKE_COMMAND}
        ARGS -E create_symlink
                ${_DART_CONFIGURATION_FILE}
                ${_DART_CONFIGURATION_FILE_NAME}
      # BYPRODUCTS ${_DART_CONFIGURATION_FILE_NAME}
        VERBATIM)
    else()
      # TODO:
    endif()
  endif()
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "NO_LINK" "" "ARGS;CONFIGURATIONS;ENV" ${ARGN})
  # --------------------------------------------------------------------------
  string(REPLACE "$<TARGET>" "${TARGET}" BOOST_TEST_LOG_SINK
    "${BOOST_TEST_LOG_SINK}")
  string(REPLACE "$<TARGET>" "${TARGET}" BOOST_TEST_REPORT_SINK
    "${BOOST_TEST_REPORT_SINK}")
  set(_LOG_SINK    ${BOOST_TEST_LOG_SINK})
  set(_REPORT_SINK ${BOOST_TEST_REPORT_SINK})
  # --------------------------------------------------------------------------
  if(NOT _UNPARSED_ARGUMENTS)
    foreach(_TEST_TARGET ${TEST_MAIN_TARGET} check ${TEST_TARGET})
      if(TARGET ${_TEST_TARGET})
        set(_UNPARSED_ARGUMENTS ${_TEST_TARGET})
        break()
      endif()
    endforeach()
  endif()
  macro(_add_test)
    add_test(NAME ${_NAME} COMMAND ${TARGET} ${_ARGS}
      CONFIGURATIONS ${_CONFIGURATIONS})
    if(NOT CMAKE_VERSION VERSION_LESS 2.8)
      string(REPLACE "$<NAME>" "${_NAME}" BOOST_TEST_LOG_SINK
        "${_LOG_SINK}")
      string(REPLACE "$<NAME>" "${_NAME}" BOOST_TEST_REPORT_SINK
        "${_REPORT_SINK}")
      set(_TEST_ENV)
      foreach(_PARAM ${BOOST_TEST_PARAMS})
        list(APPEND _TEST_ENV BOOST_TEST_${_PARAM}=${BOOST_TEST_${_PARAM}})
      endforeach()
      list(APPEND _TEST_ENV ${_ENV})
      set_tests_properties(${_NAME} PROPERTIES ENVIRONMENT "${_TEST_ENV}")
      # ----------------------------------------------------------------------
      foreach(_PARAM LOG_SINK REPORT_SINK)
        get_filename_component(_DIR ${BOOST_TEST_${_PARAM}} PATH)
        if(_DIR)
          if(NOT IS_ABSOLUTE ${_DIR})
            path(_DIR ${CMAKE_CURRENT_BINARY_DIR} ${_DIR})
          endif()
          file(TO_CMAKE_PATH ${_DIR} _DIR)
          # TODO:
          if   (N)
          string(SHA1 _TARGET ${_DIR})
          if(NOT TARGET ${_TARGET})
            add_custom_target(${_TARGET} ALL
              COMMAND ${CMAKE_COMMAND} -E make_directory ${_DIR}
            # BYPRODUCTS ${_DIR}
              VERBATIM)
            add_dependencies(${TARGET} ${_TARGET})
          endif()
          else (N)
          add_custom_command(TARGET ${TARGET} POST_BUILD
            COMMAND ${CMAKE_COMMAND}
            ARGS -E make_directory ${_DIR}
          # BYPRODUCTS ${_DIR}
            VERBATIM)
          endif(N)
        endif()
      endforeach()
      # ----------------------------------------------------------------------
      set(_TARGET ${_NAME})
      if(NOT TARGET ${_TARGET})
        set(_TEST_TARGET_ARGS)
        if(_TEST_TARGET)
          map_get(_ADD_TEST_TARGET_ARGS ${_TEST_TARGET} _TEST_TARGET_ARGS)
        endif()
        quote_regex(_REGEX ${_NAME})
        add_test_target(${_TARGET} ARGS ${_TEST_TARGET_ARGS} -R "^${_REGEX}$")
        add_dependencies(${_TARGET} ${TARGET})
      endif()
      # ----------------------------------------------------------------------
      if(DEBUGGER_COMMAND)
        get_filename_component(_DEBUGGER_NAME ${DEBUGGER_COMMAND} NAME_WE)
        set(_TARGET ${_DEBUGGER_NAME}.${TARGET})
        if(NOT TARGET ${_TARGET})
          add_custom_target(${_TARGET}
            COMMAND ${DEBUGGER_COMMAND} $<TARGET_FILE:${TARGET}>
            VERBATIM)
          add_dependencies(${_TARGET} ${TARGET})
        endif()
        set(_TARGET ${TARGET}.${_DEBUGGER_NAME})
        if(NOT TARGET ${_TARGET})
          add_custom_target(${_TARGET}
            COMMAND ${DEBUGGER_COMMAND} $<TARGET_FILE:${TARGET}>
            VERBATIM)
          add_dependencies(${_TARGET} ${TARGET})
        endif()
      endif()
      #
      # NOTE:
      #
      # Ninja unconditionally quits debugger (because of pipe).
      # ----------------------------------------------------------------------
    endif()
  endmacro()
  foreach(_TEST_TARGET ${_UNPARSED_ARGUMENTS})
    require_target(${_TEST_TARGET})
    set(_NAME ${_TEST_TARGET}.${TARGET})
    _add_test()
    add_dependencies(${_TEST_TARGET} ${TARGET})
    set(_NAME ${TARGET}.${_TEST_TARGET})
    _add_test()
  endforeach()
  if(NOT _UNPARSED_ARGUMENTS)
    set(_NAME test.${TARGET})
    _add_test()
    set(_NAME ${TARGET}.test)
    _add_test()
  endif()
  # --------------------------------------------------------------------------
  if(NOT _NO_LINK)
    def_bool(BOOST_TEST_USE_MULTITHREADED Y)
    def_bool(BOOST_TEST_USE_STATIC_LIBS   N)
    set(Boost_USE_MULTITHREADED ${BOOST_TEST_USE_MULTITHREADED})
    set(Boost_USE_STATIC_LIBS   ${BOOST_TEST_USE_STATIC_LIBS})
    find_package_without_flags(Boost QUIET REQUIRED
      COMPONENTS unit_test_framework)
    set(TARGET_CHECK N)
    if(NOT Boost_USE_STATIC_LIBS)
      target(${TARGET} COMPILE DEFS PRIVATE BOOST_TEST_DYN_LINK)
    endif()
    target(${TARGET}
      INCLUDE DIRS SYSTEM
                   PRIVATE ${Boost_INCLUDE_DIRS}
      LINK    LIBS PRIVATE ${Boost_LIBRARIES})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
##
### References {{{
##  ==========================================================================
##  [1] http://gitlab.kitware.com/cmake/cmake/issues/14737
##  ==========================================================================
##  }}} References
