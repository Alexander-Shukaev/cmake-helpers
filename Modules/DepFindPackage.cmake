### Preamble {{{
##  ==========================================================================
##        @file DepFindPackage.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-01-18 Wednesday 01:16:26 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-04 Friday 16:31:31 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DEP_FIND_PACKAGE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DEP_FIND_PACKAGE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(FindPackageWithoutFlags)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(dep_find_package PACKAGE)
  if    (${PACKAGE}_FIND_QUIETLY AND ${PACKAGE}_FIND_REQUIRED)
    find_package_without_flags(${ARGN} QUIET REQUIRED)
  elseif(${PACKAGE}_FIND_QUIETLY                             )
    find_package_without_flags(${ARGN} QUIET         )
  elseif(                            ${PACKAGE}_FIND_REQUIRED)
    find_package_without_flags(${ARGN}       REQUIRED)
  else  (                                                    )
    find_package_without_flags(${ARGN}               )
  endif (                                                    )
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
