### Preamble {{{
##  ==========================================================================
##        @file WarnAboutUnknownArguments.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 13:47:02 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 18:22:09 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_WARN_ABOUT_UNKNOWN_ARGUMENTS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_WARN_ABOUT_UNKNOWN_ARGUMENTS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(warn_about_unknown_arguments INDEX)
  list(LENGTH ARGN _LENGTH)
  if(INDEX LESS _LENGTH)
    if(INDEX GREATER 0)
      foreach(_I RANGE 1 INDEX)
        list(REMOVE_AT ARGN 0)
      endforeach()
    endif()
    foreach(_ARG IN LISTS ARGN)
      message(AUTHOR_WARNING "Unknown argument: ${_ARG}")
    endforeach()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
