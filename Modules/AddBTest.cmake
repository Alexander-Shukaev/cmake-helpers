### Preamble {{{
##  ==========================================================================
##        @file AddBTest.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 10:38:19 (+0200)
##  --------------------------------------------------------------------------
##     @created 2017-01-18 Wednesday 00:44:52 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_BTEST_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_BTEST_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddBoostTest)
include(Contains)
include(RequireProject)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def_bool(BTEST_USE_MULTITHREADED Y)
def_bool(BTEST_USE_STATIC_LIBS   N)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(add_btest TARGET)
  require_project()
  # --------------------------------------------------------------------------
  get_target_property(_TYPE ${TARGET} TYPE)
  # --------------------------------------------------------------------------
  set(_TYPES SHARED)
  contains(_TYPE_IS_SHARED     _TYPES ${_TYPE})
  if(_TYPE_IS_SHARED)
    message(FATAL_ERROR "Invalid target type: ${_TYPE}")
  endif()
  # --------------------------------------------------------------------------
  set(_TYPES EXECUTABLE)
  contains(_TYPE_IS_EXECUTABLE _TYPES ${_TYPE})
  if(_TYPE_IS_EXECUTABLE)
    add_boost_test(${TARGET} NO_LINK ${ARGN})
    # ------------------------------------------------------------------------
    cmake_parse_arguments("" "LOCAL;NO_LINK" "" "" ${ARGN})
    # ------------------------------------------------------------------------
    if(_NO_LINK)
      return()
    endif()
  else()
    warn_about_unknown_arguments(0 ${ARGN})
  endif()
  # --------------------------------------------------------------------------
  def_bool(BTEST_USE_MULTITHREADED Y)
  def_bool(BTEST_USE_STATIC_LIBS   N)
  set(BTest_USE_MULTITHREADED ${BTEST_USE_MULTITHREADED})
  set(BTest_USE_STATIC_LIBS   ${BTEST_USE_STATIC_LIBS})
  set(${PROJECT}_BTest_ARGS QUIET)
  set(           BTest_ARGS QUIET REQUIRED)
  if(_LOCAL)
    list(APPEND ${PROJECT}_BTest_ARGS REQUIRED)
  endif()
  foreach(_PACKAGE ${PROJECT}_BTest BTest)
    set(${_PACKAGE}_FOUND N)
    find_package_without_flags(${_PACKAGE} ${${_PACKAGE}_ARGS})
    if(${_PACKAGE}_FOUND)
      message(STATUS "Using ${_PACKAGE}: ${${_PACKAGE}_CMAKE_DIRS} "
                     "(using version \"${${_PACKAGE}_VERSION}\")")
      if(_TYPE_IS_EXECUTABLE)
        set(TARGET_CHECK N)
        target(${TARGET}
          LINK LIBS PRIVATE ${${_PACKAGE}_LIBRARIES})
      else()
        if(TARGET ${_PACKAGE})
          get_target_property(_IMPORTED
            ${_PACKAGE} IMPORTED)
          get_target_property(_INCLUDE_DIRS
            ${_PACKAGE} INTERFACE_INCLUDE_DIRECTORIES)
          if(_IMPORTED)
            set(${_PACKAGE}_INCLUDE_DIRS
              ${_INCLUDE_DIRS}
              ${${_PACKAGE}_INCLUDE_DIRS})
          else()
            set(TARGET_CHECK N)
            target(${TARGET}
              INCLUDE DIRS PRIVATE ${_INCLUDE_DIRS})
          endif()
        endif()
      endif()
      set(TARGET_CHECK N)
      target(${TARGET}
        COMPILE DEFS PRIVATE ${${_PACKAGE}_DEFINITIONS}
                OPTS PRIVATE ${${_PACKAGE}_OPTIONS}
        INCLUDE DIRS SYSTEM
                     PRIVATE ${${_PACKAGE}_INCLUDE_DIRS})
      break()
    endif()
  endforeach()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
