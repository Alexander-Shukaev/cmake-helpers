### Preamble {{{
##  ==========================================================================
##        @file FindCasablanca.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:38:47 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-02 Wednesday 21:24:48 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(DepGetCXX11StandardOption)
include(DepPkgCheckModules)
include(ExtractPackageDefinition)
include(FindDepPackage)
include(FindPackageHandleStandardArgs)
include(SetTargetProperty)
include(Version)
##  ==========================================================================
##  }}} Modules
##
dep_pkg_check_modules(Casablanca_PKG casablanca
                                     cpprest
                                     cpprestsdk)
find_path(Casablanca_INCLUDE_DIR
  NAMES cpprest/http_client.h
        cpprest/version.h
        pplx/pplx.h
  HINTS ${Casablanca_PKG_INCLUDE_DIRS}
  PATHS ${Casablanca_INCLUDE_DIR}
        $ENV{Casablanca_INCLUDE_DIR}
        ${CMAKE_INSTALL_PREFIX}/include
  PATH_SUFFIXES casablanca
                cpprest
                cpprestsdk
                pplx)
mark_as_advanced(Casablanca_INCLUDE_DIR)
find_library(Casablanca_LIBRARY
  NAMES casablanca
        cpprest
        cpprestsdk
  HINTS ${Casablanca_PKG_LIBRARY_DIRS}
  PATHS ${Casablanca_LIBRARY_DIR}
        $ENV{Casablanca_LIBRARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/lib)
mark_as_advanced(Casablanca_LIBRARY)
set(Casablanca_DEFINITIONS  -D_NO_ASYNCRTIMP -D_NO_PPLXIMP)
set(Casablanca_INCLUDE_DIRS ${Casablanca_INCLUDE_DIR})
set(Casablanca_LIBRARIES    ${Casablanca_LIBRARY})
set(Casablanca_OPTIONS      ${Casablanca_PKG_CFLAGS_OTHER})
dep_get_cxx11_standard_option(Casablanca CXX11_STANDARD_OPTION)
if(CXX11_STANDARD_OPTION)
  list(APPEND Casablanca_OPTIONS ${CXX11_STANDARD_OPTION})
endif()
extract_package_definition(Casablanca_VERSION_MAJOR
  Casablanca
  cpprest/version.h
  CPPREST_VERSION_MAJOR)
extract_package_definition(Casablanca_VERSION_MINOR
  Casablanca
  cpprest/version.h
  CPPREST_VERSION_MINOR)
extract_package_definition(Casablanca_VERSION_PATCH
  Casablanca
  cpprest/version.h
  CPPREST_VERSION_REVISION)
if(NOT Casablanca_VERSION_MAJOR MATCHES "^$" AND
   NOT Casablanca_VERSION_MINOR MATCHES "^$" AND
   NOT Casablanca_VERSION_PATCH MATCHES "^$")
  version(Casablanca_VERSION ${Casablanca_VERSION_MAJOR}
                             ${Casablanca_VERSION_MINOR}
                             ${Casablanca_VERSION_PATCH})
endif()
##
if(DEFINED Casablanca_Boost_USE_MULTITHREADED)
  set(Boost_USE_MULTITHREADED ${Casablanca_Boost_USE_MULTITHREADED})
endif()
if(DEFINED Casablanca_Boost_USE_STATIC_LIBS)
  set(Boost_USE_STATIC_LIBS   ${Casablanca_Boost_USE_STATIC_LIBS})
endif()
dep_find_package(Casablanca Boost 1.55 QUIET COMPONENTS atomic
                                                        chrono
                                                        filesystem
                                                        random
                                                        regex
                                                        system
                                                        thread)
if(Boost_FOUND)
  list(APPEND Casablanca_DEFINITIONS  -DBOOST_FILESYSTEM_NO_DEPRECATED
                                      -DBOOST_THREAD_VERSION=3)
  list(APPEND Casablanca_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
  list(APPEND Casablanca_LIBRARIES    ${Boost_LIBRARIES})
endif()
##
if(DEFINED Casablanca_Threads_PREFER_PTHREAD)
  set(CMAKE_THREAD_PREFER_PTHREAD ${Casablanca_Threads_PREFER_PTHREAD})
endif()
if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  set(THREADS_PREFER_PTHREAD_FLAG Y)
endif()
dep_find_package(Casablanca Threads QUIET)
if(THREADS_FOUND)
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
    list(APPEND Casablanca_LIBRARIES Threads::Threads)
  else()
    if(THREADS_HAVE_PTHREAD_ARG)
      list(APPEND Casablanca_OPTIONS   -pthread)
    endif()
    if(CMAKE_THREAD_LIBS_INIT)
      list(APPEND Casablanca_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
    endif()
  endif()
endif()
##
dep_find_package(Casablanca OpenSSL 1 QUIET)
if(OPENSSL_FOUND)
  list(APPEND Casablanca_INCLUDE_DIRS ${OPENSSL_INCLUDE_DIR})
  list(APPEND Casablanca_LIBRARIES    ${OPENSSL_LIBRARIES})
endif()
##
dep_find_package(Casablanca websocketpp QUIET)
if(WEBSOCKETPP_FOUND)
  list(APPEND Casablanca_INCLUDE_DIRS ${WEBSOCKETPP_INCLUDE_DIR})
endif()
##
if(WIN32)
  list(APPEND Casablanca_DEFINITIONS -D_UNICODE
                                     -D_WINSOCK_DEPRECATED_NO_WARNINGS
                                     -DUNICODE
                                     -DWIN32)
  list(APPEND Casablanca_LIBRARIES   ws2_32)
endif()
##
find_package_handle_standard_args(Casablanca
  FOUND_VAR     Casablanca_FOUND
  REQUIRED_VARS Casablanca_LIBRARY
                Casablanca_LIBRARIES
                Casablanca_INCLUDE_DIR
                Casablanca_INCLUDE_DIRS
  VERSION_VAR   Casablanca_VERSION)
##
if(Casablanca_FOUND AND NOT TARGET Casablanca)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(Casablanca INTERFACE IMPORTED)
    set_target_property(Casablanca
      INTERFACE_COMPILE_DEFINITIONS       ${Casablanca_DEFINITIONS})
    set_target_property(Casablanca
      INTERFACE_COMPILE_OPTIONS           ${Casablanca_OPTIONS})
    set_target_property(Casablanca
      INTERFACE_INCLUDE_DIRECTORIES       ${Casablanca_INCLUDE_DIRS})
    set_target_property(Casablanca
      INTERFACE_LINK_LIBRARIES            ${Casablanca_LIBRARIES})
    set_target_property(Casablanca
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
    set_target_property(Casablanca
      INTERFACE_Casablanca_VERSION_MAJOR  ${Casablanca_VERSION_MAJOR})
    set_target_property(Casablanca
      COMPATIBLE_INTERFACE_STRING           Casablanca_VERSION_MAJOR)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
#   set_target_property(Casablanca
#     INTERFACE_CXX_STANDARD              11)
  endif()
endif()
