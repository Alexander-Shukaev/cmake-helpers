### Preamble {{{
##  ==========================================================================
##        @file TargetCheck.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:37:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-22 Wednesday 23:56:27 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_CHECK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_CHECK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(OptionDef)
include(TargetCheckHardening)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(TARGET_CHECK
  "Whether to check for various features in the targets."
  Y)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_check TARGET)
  set(_KEYWORDS HARDENING)
  cmake_parse_arguments("_WITH" "${_KEYWORDS}" "" ""             ${ARGN})
  cmake_parse_arguments(""      ""             "" "${_KEYWORDS}" ${ARGN})
  # --------------------------------------------------------------------------
  if(_WITH_HARDENING OR TARGET_CHECK_HARDENING)
    if(NOT WIN32)
      get_target_property(_TYPE ${TARGET} TYPE)
      set(_TYPES OBJECT_LIBRARY)
      contains(_TYPE_IS_OBJECT_LIBRARY _TYPES ${_TYPE})
      if(_WITH_HARDENING OR NOT _TYPE_IS_OBJECT_LIBRARY)
        target_check_hardening(${TARGET} ${_HARDENING})
      endif()
    endif()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
