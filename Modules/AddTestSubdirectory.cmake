### Preamble {{{
##  ==========================================================================
##        @file AddTestSubdirectory.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:34:59 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 17:36:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_TEST_SUBDIRECTORY_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_TEST_SUBDIRECTORY_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(OptionDef)
include(RequireProject)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(add_test_subdirectory)
  require_project()
  # --------------------------------------------------------------------------
  option_def_bool(${PROJECT}_TEST_INCLUDE_IN_ALL
    "Whether to include targets in the test subdirectory in the ALL target."
    N)
  if(PROJECT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    if(${ARGC} EQUAL 0)
      if(${PROJECT}_TEST_INCLUDE_IN_ALL)
        add_subdirectory(test)
      else()
        add_subdirectory(test EXCLUDE_FROM_ALL)
      endif()
    endif()
    if(${ARGC} EQUAL 1)
      if(${PROJECT}_TEST_INCLUDE_IN_ALL)
        add_subdirectory(${ARGV0})
      else()
        add_subdirectory(${ARGV0} EXCLUDE_FROM_ALL)
      endif()
    endif()
    if(${ARGC} EQUAL 2)
      if(${PROJECT}_TEST_INCLUDE_IN_ALL)
        add_subdirectory(${ARGV0} ${ARGV1})
      else()
        add_subdirectory(${ARGV0} ${ARGV1} EXCLUDE_FROM_ALL)
      endif()
    endif()
    if(${ARGC} GREATER 2)
      warn_about_unknown_arguments(2 ${ARGN})
    endif()
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
