### Preamble {{{
##  ==========================================================================
##        @file DetectNoRTTIFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-20 Saturday 22:56:58 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-20 Saturday 22:56:37 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DETECT_NO_RTTI_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DETECT_NO_RTTI_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(FilterCXXCompilerFlags)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(detect_no_rtti_flags VAR)
  filter_cxx_compiler_flags(${VAR}
    -fno-rtti # |Clang|GNU|IBM|Intel|Oracle|  |
    # ------- # |-----|---|---|-----|------|--|
    /GR-      # |     |   |   |Intel|      |MS|
    )
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
