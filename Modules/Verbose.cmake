### Preamble {{{
##  ==========================================================================
##        @file Verbose.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-10-06 Tuesday 11:29:34 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-08 Monday 17:10:57 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_VERBOSE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_VERBOSE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(OptionDef)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
# As per [1]:
if(DEFINED ENV{VERBOSE})
  def_bool(VERBOSE $ENV{VERBOSE})
endif()
##
option_def_bool(VERBOSE
  "Whether to print verbose output."
  Y)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### References {{{
##  ==========================================================================
##  [1] http://gitlab.kitware.com/cmake/cmake/issues/14737
##  ==========================================================================
##  }}} References
