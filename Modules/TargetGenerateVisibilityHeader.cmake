### Preamble {{{
##  ==========================================================================
##        @file TargetGenerateVisibilityHeader.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:19:26 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-08 Wednesday 00:10:40 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_GENERATE_VISIBILITY_HEADER_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_GENERATE_VISIBILITY_HEADER_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(GenerateVisibilityHeader)
include(GetOptions)
include(SetTargetProperty)
include(Target)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_generate_visibility_header TARGET)
  set(_MESSAGES)
  if(TARGET ${TARGET})
    get_target_property(_TYPE ${TARGET} TYPE)
    set(_TYPES MODULE_LIBRARY
               OBJECT_LIBRARY
               SHARED_LIBRARY
               STATIC_LIBRARY)
    contains(_TYPE_IS_LIBRARY _TYPES ${_TYPE})
    if(NOT _TYPE_IS_LIBRARY)
      set(_MESSAGES "Invalid target type: ${_TYPE}")
    endif()
  else()
    set(_MESSAGES "Unknown target: ${TARGET}")
  endif()
  # --------------------------------------------------------------------------
  set(_OPTIONS     INCLUDE)
  set(_SV_KEYWORDS NAME
                   VERSION
                   STATIC_CONDITION_IDENTIFIER
                   BUILD_CONDITION_IDENTIFIER)
  set(_MV_KEYWORDS INSTALL)
  cmake_parse_arguments("_WITH"
                        "${_MV_KEYWORDS}"
                        ""
                        ""
                        ${ARGN})
  cmake_parse_arguments(""
                        "${_OPTIONS}"
                        "${_SV_KEYWORDS}"
                        "${_MV_KEYWORDS}"
                        ${ARGN})
  # --------------------------------------------------------------------------
  get_options("REQUIRED;QUIET" ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
    return()
  endif()
  # --------------------------------------------------------------------------
  get_target_property(_LANGUAGE ${TARGET} LINKER_LANGUAGE)
  if(NOT _LANGUAGE)
    set(_LANGUAGE CXX)
  endif()
  # --------------------------------------------------------------------------
  if(NOT _NAME)
    set(_NAME ${TARGET})
  endif()
  string(TOLOWER "${_NAME}" _NAME_LOWER)
  string(TOUPPER "${_NAME}" _NAME_UPPER)
  # --------------------------------------------------------------------------
  if(NOT _VERSION)
    get_target_property(_VERSION ${TARGET}   VERSION)
  endif()
  if(NOT _VERSION)
    get_target_property(_VERSION ${TARGET} SOVERSION)
  endif()
  # --------------------------------------------------------------------------
  if(NOT _STATIC_CONDITION_IDENTIFIER)
    set(_STATIC_CONDITION_IDENTIFIER ${_NAME_UPPER}_STATIC)
  endif()
  if(_TYPE MATCHES "^STATIC_LIBRARY$")
    set(TARGET_CHECK N)
    target(${TARGET} COMPILE DEFS PUBLIC ${_STATIC_CONDITION_IDENTIFIER})
  endif()
  # --------------------------------------------------------------------------
  if(NOT _BUILD_CONDITION_IDENTIFIER)
    get_target_property(_BUILD_CONDITION_IDENTIFIER ${TARGET} DEFINE_SYMBOL)
  endif()
  if(NOT _BUILD_CONDITION_IDENTIFIER)
    set(_BUILD_CONDITION_IDENTIFIER ${_NAME_UPPER}_BUILD)
  endif()
  set_target_property(${TARGET} DEFINE_SYMBOL ${_BUILD_CONDITION_IDENTIFIER})
  # --------------------------------------------------------------------------
  generate_visibility_header(_HEADER ${_LANGUAGE}
                                     ${_NAME}
                                     ${_UNPARSED_ARGUMENTS}
    VERSION                     ${_VERSION}
    STATIC_CONDITION_IDENTIFIER ${_STATIC_CONDITION_IDENTIFIER}
    BUILD_CONDITION_IDENTIFIER  ${_BUILD_CONDITION_IDENTIFIER})
  get_filename_component(_HEADER ${_HEADER} ABSOLUTE)
  get_filename_component(_DIR    ${_HEADER} PATH)
  if(_INCLUDE)
    if(CMAKE_VERSION VERSION_LESS 2.8.11)
      add_target_property(${TARGET} INCLUDE_DIRECTORIES ${_DIR})
    else()
      target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${_DIR}>)
    endif()
  endif()
  if(_WITH_INSTALL)
    if(NOT _INSTALL)
      set(_VAR INSTALL_INCLUDE_DIR)
      if    (NOT DEFINED ${_VAR})
        set(_MESSAGES "Variable not set: ${_VAR}")
      elseif(NOT         ${_VAR})
        set(_MESSAGES "Variable empty: ${_VAR}")
      endif()
      if(_MESSAGES)
        if(_REQUIRED AND PROJECT_STANDALONE)
          message(FATAL_ERROR ${_MESSAGES})
        endif()
        if(NOT _QUIET)
          message(WARNING     ${_MESSAGES})
        endif()
        return()
      endif()
      foreach(_PARENT_DIR ${CMAKE_CURRENT_BINARY_DIR}
                          ${CMAKE_CURRENT_SOURCE_DIR}
                          ${PROJECT_BINARY_DIR}
                          ${PROJECT_SOURCE_DIR}
                          ${CMAKE_BINARY_DIR}
                          ${CMAKE_SOURCE_DIR})
        foreach(_PARENT_SUBDIR include
                               source
                               src
                               test
                               "")
          path(_PARENT_SUBDIR ${_PARENT_DIR} ${_PARENT_SUBDIR})
          file(RELATIVE_PATH _RELATIVE_DIR ${_PARENT_SUBDIR} ${_DIR})
          path(_HEADER_DIR ${_PARENT_SUBDIR} ${_RELATIVE_DIR})
          if(_HEADER_DIR STREQUAL _DIR)
            break()
          else()
            set(_RELATIVE_DIR)
          endif()
        endforeach()
        if(_RELATIVE_DIR)
          break()
        endif()
      endforeach()
      path(${_VAR} ${${_VAR}} ${_RELATIVE_DIR})
      set(_INSTALL DESTINATION ${${_VAR}})
    endif()
    install(FILES ${_HEADER} ${_INSTALL})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
