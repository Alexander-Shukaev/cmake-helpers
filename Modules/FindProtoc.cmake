### Preamble {{{
##  ==========================================================================
##        @file FindProtoc.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:02:00 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-27 Sunday 14:52:56 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(DepPkgCheckModules)
include(FindDepPackage)
include(FindPackageHandleStandardArgs)
include(GetCXX11StandardFlag)
include(Path)
include(SetTargetProperty)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(Protoc_GENERATE LANGUAGE HEADER_FILES_VAR SOURCE_FILES_VAR)
  set(LANGUAGE_IS_CPP  N)
  set(LANGUAGE_IS_GPRC N)
  string(TOUPPER "${LANGUAGE}" LANGUAGE)
  if    (LANGUAGE MATCHES "^C[+][+]$" OR
         LANGUAGE MATCHES "^CPP$"     OR
         LANGUAGE MATCHES "^CXX$")
    set(LANGUAGE_IS_CPP Y)
    set(HEADER_EXT .pb.h)
    set(SOURCE_EXT .pb.cc)
  elseif(LANGUAGE MATCHES "^GRPC$"       OR
         LANGUAGE MATCHES "^GRPC[+][+]$" OR
         LANGUAGE MATCHES "^GRPCPP$"     OR
         LANGUAGE MATCHES "^GRPCXX$")
    set(LANGUAGE_IS_GRPC Y)
    set(HEADER_EXT .grpc.pb.h)
    set(SOURCE_EXT .grpc.pb.cc)
    if(NOT GRPC_CPP_PLUGIN)
      find_program(GRPC_CPP_PLUGIN
        NAMES grpc_cpp_plugin
        PATHS ${GRPC_RUNTIME_DIR}
              $ENV{GRPC_RUNTIME_DIR}
              ${GRPC_BINARY_DIR}
              $ENV{GRPC_BINARY_DIR}
              ${CMAKE_INSTALL_PREFIX}/bin)
    endif()
  else()
    message(FATAL_ERROR "Unknown language: ${LANGUAGE}")
  endif()
  if(NOT ARGN)
    message(FATAL_ERROR "Specify at least one Proto file")
  endif()
  set(${HEADER_FILES_VAR})
  set(${SOURCE_FILES_VAR})
  foreach(PROTO_FILE IN LISTS ARGN)
    get_filename_component(DIR     ${PROTO_FILE} PATH)
    get_filename_component(NAME_WE ${PROTO_FILE} NAME_WE)
    if(IS_ABSOLUTE ${PROTO_FILE})
      path(OUT_DIR ${CMAKE_CURRENT_BINARY_DIR})
    else()
      path(OUT_DIR ${CMAKE_CURRENT_BINARY_DIR} ${DIR})
    endif()
    path(HEADER_FILE ${OUT_DIR} ${NAME_WE}${HEADER_EXT})
    path(SOURCE_FILE ${OUT_DIR} ${NAME_WE}${SOURCE_EXT})
    list(APPEND ${HEADER_FILES_VAR} ${HEADER_FILE})
    list(APPEND ${SOURCE_FILES_VAR} ${SOURCE_FILE})
    if(LANGUAGE_IS_CPP)
      set(OPTIONS --cpp_out=${OUT_DIR})
    endif()
    if(LANGUAGE_IS_GRPC)
      set(OPTIONS --grpc_out=${OUT_DIR}
                  --plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN})
    endif()
    add_custom_command(
      OUTPUT ${HEADER_FILE}
             ${SOURCE_FILE}
      COMMAND ${Protoc_EXECUTABLE}
      ARGS ${OPTIONS} -I ${DIR} ${PROTO_FILE}
      DEPENDS ${Protoc_EXECUTABLE} ${PROTO_FILE}
      VERBATIM)
  endforeach()
  set_source_files_properties(${${HEADER_FILES_VAR}}
                              ${${SOURCE_FILES_VAR}}
    PROPERTIES GENERATED Y)
  set_source_files_properties(${${SOURCE_FILES_VAR}}
    PROPERTIES LANGUAGE CXX)
  if(LANGUAGE_IS_GRPC)
    get_cxx11_standard_flag(CXX11_STANDARD_FLAG)
    if(CXX11_STANDARD_FLAG)
      set_source_files_properties(${${SOURCE_FILES_VAR}}
        PROPERTIES COMPILE_FLAGS ${CXX11_STANDARD_FLAG})
    endif()
  endif()
  set(${HEADER_FILES_VAR} ${${HEADER_FILES_VAR}} PARENT_SCOPE)
  set(${SOURCE_FILES_VAR} ${${SOURCE_FILES_VAR}} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
##
find_program(Protoc_EXECUTABLE
  NAMES protoc
  PATHS ${Protoc_RUNTIME_DIR}
        $ENV{Protoc_RUNTIME_DIR}
        ${Protoc_BINARY_DIR}
        $ENV{Protoc_BINARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/bin)
mark_as_advanced(Protoc_EXECUTABLE)
dep_pkg_check_modules(Protoc_PKG protoc)
find_library(Protoc_LIBRARY
  NAMES protoc
  HINTS ${Protoc_PKG_LIBRARY_DIRS}
  PATHS ${Protoc_LIBRARY_DIR}
        $ENV{Protoc_LIBRARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/lib)
mark_as_advanced(Protoc_LIBRARY)
set(Protoc_LIBRARIES ${Protoc_LIBRARY})
##
find_package_handle_standard_args(Protoc
  FOUND_VAR     Protoc_FOUND
  REQUIRED_VARS Protoc_LIBRARY
                Protoc_LIBRARIES
                Protoc_EXECUTABLE)
##
if(Protoc_FOUND AND NOT TARGET Protoc)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(Protoc INTERFACE IMPORTED)
    set_target_property(Protoc
      INTERFACE_LINK_LIBRARIES            ${Protoc_LIBRARIES})
    set_target_property(Protoc
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  endif()
endif()
