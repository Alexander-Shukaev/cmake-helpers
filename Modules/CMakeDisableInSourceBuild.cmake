### Preamble {{{
##  ==========================================================================
##        @file CMakeDisableInSourceBuild.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 12:16:52 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-08 Tuesday 17:39:07 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CMAKE_DISABLE_IN_SOURCE_BUILD_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CMAKE_DISABLE_IN_SOURCE_BUILD_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(cmake_disable_in_source_build)
  set(CMAKE_DISABLE_IN_SOURCE_BUILD Y)
  set(CMAKE_DISABLE_SOURCE_CHANGES  Y)
  if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_CURRENT_BINARY_DIR)
    message(FATAL_ERROR "In-source builds are not allowed")
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
