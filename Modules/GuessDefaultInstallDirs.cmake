### Preamble {{{
##  ==========================================================================
##        @file GuessDefaultInstallDirs.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 23:57:04 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-02-06 Monday 01:25:25 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GUESS_DEFAULT_INSTALL_DIRS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GUESS_DEFAULT_INSTALL_DIRS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Install)
include(Path)
include(ProjectDef)
include(RequireProject)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(guess_default_install_dirs)
  require_project()
  # --------------------------------------------------------------------------
  foreach(_KIND ${INSTALL_DIR_KINDS})
    foreach(_VAR INSTALL_${_KIND}_DIR)
      project_def(${_VAR})
      if(DEFINED ${PROJECT}_${_VAR})
        set(${_VAR} ${${PROJECT}_${_VAR}})
      endif()
      unset(${SUBPROJECT}_${_VAR})
      unset(   ${PROJECT}_${_VAR})
      unset(              ${_VAR} CACHE)
    endforeach()
  endforeach()
  if(APPLE)
    path(CMAKE_INSTALL_NAME_DIR "@rpath")
    path(CMAKE_INSTALL_RPATH    "@loader_path" .. lib)
  else()
    path(CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_PREFIX} ${INSTALL_LIBRARY_DIR})
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
