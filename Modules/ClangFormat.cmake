### Preamble {{{
##  ==========================================================================
##        @file ClangFormat.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-09-10 Tuesday 14:48:38 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-10 Wednesday 21:57:33 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CLANG_FORMAT_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CLANG_FORMAT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(CLANG_FORMAT
  "Whether to use Clang-Format."
  Y)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CLANG_FORMAT)
  find_program(CLANG_FORMAT_COMMAND clang-format)
  def(CLANG_FORMAT_ARGS
    -style=file
    -fallback-style=none)
  def(CLANG_FORMAT_DIRS
    include
    source
    src
    test)
  def(CLANG_FORMAT_EXTS
    .CC .HH .II .TT
    .cc .hh .ii .tt .tcc
    .[CHIT]
    .[CHIT]PP
    .[chit]++
    .[chit]p
    .[chit]pp
    .[chit]xx
    .[chi])
  def(CLANG_FORMAT_SUFFIXES
    .in)
  set(_PATTERNS)
  foreach(_EXT ${CLANG_FORMAT_EXTS})
    if(CLANG_FORMAT_DIRS)
      foreach(_DIR ${CLANG_FORMAT_DIRS})
        list(APPEND _PATTERNS ${_DIR}/*${_EXT})
        foreach(_SUFFIX ${CLANG_FORMAT_SUFFIXES})
          list(APPEND _PATTERNS ${_DIR}/*${_EXT}${_SUFFIX})
        endforeach()
      endforeach()
    else()
      list(APPEND _PATTERNS *${_EXT})
      foreach(_SUFFIX ${CLANG_FORMAT_SUFFIXES})
        list(APPEND _PATTERNS *${_EXT}${_SUFFIX})
      endforeach()
    endif()
  endforeach()
  def(CLANG_FORMAT_PATTERNS ${_PATTERNS})
  unset(_PATTERNS)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Properties {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CLANG_FORMAT)
  set(_MESSAGES)
  if(CLANG_FORMAT_COMMAND)
    file(GLOB_RECURSE _FILES ${CLANG_FORMAT_PATTERNS})
    add_custom_target(clang-format
      COMMAND ${CLANG_FORMAT_COMMAND} -i ${CLANG_FORMAT_ARGS} ${_FILES}
      VERBATIM)
    unset(_FILES)
  else()
    set(_MESSAGES "Command not found: ${CLANG_FORMAT_COMMAND}")
  endif()
  if(_MESSAGES AND VERBOSE)
    message(WARNING ${_MESSAGES})
  endif()
  unset(_MESSAGES)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Properties
