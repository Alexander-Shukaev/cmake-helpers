### Preamble {{{
##  ==========================================================================
##        @file ExtractDefinition.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:06:51 (+0100)
##  --------------------------------------------------------------------------
##     @created 2015-12-04 Friday 17:05:26 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_EXTRACT_DEFINITION_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_EXTRACT_DEFINITION_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(extract_definition VAR STRING NAME)
  set(_REGEX "#[ \t]*define[ \t]+${NAME}[ \t]+([^\n]+)")
  string(REGEX MATCH "${_REGEX}" _MATCH "${STRING}")
  if(_MATCH MATCHES "^$")
    set(${VAR}                  PARENT_SCOPE)
  else()
    set(${VAR} ${CMAKE_MATCH_1} PARENT_SCOPE)
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
