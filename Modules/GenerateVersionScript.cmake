### Preamble {{{
##  ==========================================================================
##        @file GenerateVersionScript.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:23:35 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-19 Sunday 00:29:40 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GENERATE_VERSION_SCRIPT_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GENERATE_VERSION_SCRIPT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Def)
include(Join)
include(Languages)
include(Path)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(VERSION_SCRIPT_EXT     .ld)
def(C_VERSION_SCRIPT_EXT   .c${VERSION_SCRIPT_EXT})
def(CXX_VERSION_SCRIPT_EXT .cpp${VERSION_SCRIPT_EXT})
##
def(VERSION_SCRIPT_LANGUAGES ${LANGUAGES_UPPER})
##
foreach(_LANGUAGE ${VERSION_SCRIPT_LANGUAGES})
  find_path(${_LANGUAGE}_VERSION_SCRIPT_TEMPLATE_DIR
    NAMES version${${_LANGUAGE}_VERSION_SCRIPT_EXT}.in
    PATHS ${Helpers_CMAKE_DIRS}
          ${Helpers_CMAKE_TEMPLATES_DIRS}
    PATH_SUFFIXES Templates)
  path_def(${_LANGUAGE}_VERSION_SCRIPT_TEMPLATE
    ${${_LANGUAGE}_VERSION_SCRIPT_TEMPLATE_DIR}
    version${${_LANGUAGE}_VERSION_SCRIPT_EXT}.in)
endforeach()
##
def(VERSION_SCRIPT_GLOBAL_VISIBILITY_PATTERNS "\"0\"")
def(VERSION_SCRIPT_LOCAL_VISIBILITY_PATTERNS  *)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(generate_version_script VAR LANGUAGE NAME)
  set(_EXT      ${${LANGUAGE}_VERSION_SCRIPT_EXT})
  set(_TEMPLATE ${${LANGUAGE}_VERSION_SCRIPT_TEMPLATE})
  # --------------------------------------------------------------------------
  set(_MESSAGES)
  if(NOT NAME)
    set(_MESSAGES "Invalid argument: ${NAME}")
  endif()
  # --------------------------------------------------------------------------
  set(_SV_KEYWORDS DIR
                   SUBDIR
                   FILE)
  set(_MV_KEYWORDS GLOBAL_VISIBILITY_PATTERNS
                   GLOBAL_VISIBILITY_PATTERNS_MANGLED
                   LOCAL_VISIBILITY_PATTERNS
                   LOCAL_VISIBILITY_PATTERNS_MANGLED)
  cmake_parse_arguments("_WITH"
                        "${_SV_KEYWORDS};${_MV_KEYWORDS}"
                        ""
                        ""
                        ${ARGN})
  cmake_parse_arguments(""
                        ""
                        "${_SV_KEYWORDS}"
                        "${_MV_KEYWORDS}"
                        ${ARGN})
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "REQUIRED;QUIET" "" "" ${_UNPARSED_ARGUMENTS})
  warn_about_unknown_arguments(0 ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
    return()
  endif()
  # --------------------------------------------------------------------------
  string(TOLOWER "${NAME}" _NAME_LOWER)
  string(TOUPPER "${NAME}" _NAME_UPPER)
  # --------------------------------------------------------------------------
  if(NOT _DIR)
    path(_DIR ${CMAKE_CURRENT_BINARY_DIR})
  endif()
  get_filename_component(_DIR ${_DIR} ABSOLUTE)
  # --------------------------------------------------------------------------
  if(NOT _WITH_SUBDIR)
    path(_SUBDIR ${NAME})
  endif()
  if(_SUBDIR AND IS_ABSOLUTE ${_SUBDIR})
    file(RELATIVE_PATH _SUBDIR ${_DIR} ${_SUBDIR})
  endif()
  # --------------------------------------------------------------------------
  if(NOT _FILE)
    path(_FILE version${_EXT})
  endif()
  if(NOT IS_ABSOLUTE ${_FILE})
    path(_FILE ${_DIR} ${_SUBDIR} ${_FILE})
  endif()
  # --------------------------------------------------------------------------
  foreach(_VISIBILITY GLOBAL LOCAL)
    if(NOT _${_VISIBILITY}_VISIBILITY_PATTERNS)
      set(_${_VISIBILITY}_VISIBILITY_PATTERNS
        ${VERSION_SCRIPT_${_VISIBILITY}_VISIBILITY_PATTERNS})
    endif()
    if(NOT _${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED)
      set(_${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED
        ${VERSION_SCRIPT_${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED})
    endif()
    join(${_VISIBILITY}_VISIBILITY_PATTERNS         DELIMITER "\\\;\n    "
      ${_${_VISIBILITY}_VISIBILITY_PATTERNS})
    join(${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED DELIMITER "\\\;\n  "
      ${_${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED})
    if(${_VISIBILITY}_VISIBILITY_PATTERNS)
      set(${_VISIBILITY}_VISIBILITY_PATTERNS
        ${${_VISIBILITY}_VISIBILITY_PATTERNS}\;)
    endif()
    if(${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED)
      set(${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED
        ${${_VISIBILITY}_VISIBILITY_PATTERNS_MANGLED}\;)
    endif()
  endforeach()
  # --------------------------------------------------------------------------
  configure_file(${_TEMPLATE} ${_FILE} @ONLY)
  set(${VAR} ${_FILE} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
