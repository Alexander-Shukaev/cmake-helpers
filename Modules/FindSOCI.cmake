### Preamble {{{
##  ==========================================================================
##        @file FindSOCI.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-17 Sunday 22:03:59 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-07 Thursday 22:04:40 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(Contains)
include(DepGetCXX11StandardOption)
include(DepPkgCheckModules)
include(FindPackageHandleStandardArgs)
include(SetTargetProperty)
##  ==========================================================================
##  }}} Modules
##
dep_pkg_check_modules(SOCI_PKG soci)
find_path(SOCI_INCLUDE_DIR
  NAMES soci.h
  HINTS ${SOCI_PKG_INCLUDE_DIRS}
  PATHS ${SOCI_INCLUDE_DIR}
        $ENV{SOCI_INCLUDE_DIR}
        ${CMAKE_INSTALL_PREFIX}/include
  PATH_SUFFIXES soci)
mark_as_advanced(SOCI_INCLUDE_DIR)
set(SOCI_DEFINITIONS)
set(SOCI_INCLUDE_DIRS ${SOCI_INCLUDE_DIR})
set(SOCI_LIBRARIES)
set(SOCI_OPTIONS       ${SOCI_PKG_CFLAGS_OTHER})
dep_get_cxx11_standard_option(SOCI CXX11_STANDARD_OPTION)
if(CXX11_STANDARD_OPTION)
  list(APPEND SOCI_OPTIONS ${CXX11_STANDARD_OPTION})
endif()
##
if(SOCI_FIND_REQUIRED)
  set(REQUIRED_INCLUDE_DIR_VARS SOCI_INCLUDE_DIR)
  set(REQUIRED_LIBRARY_VARS     SOCI_CORE_LIBRARY)
endif()
list(APPEND            SOCI_FIND_COMPONENTS core)
list(REMOVE_DUPLICATES SOCI_FIND_COMPONENTS)
set(COMPONENTS
  core
  db2
  empty
  firebird
  mysql
  odbc
  oracle
  postgresql
  sqlite3)
foreach(COMPONENT IN LISTS COMPONENTS)
  string(TOUPPER "${COMPONENT}" C)
  if(NOT C MATCHES "^CORE$")
    contains(CONTAINS SOCI_FIND_COMPONENTS ${COMPONENT})
    if(SOCI_FIND_REQUIRED AND CONTAINS)
      list(APPEND REQUIRED_INCLUDE_DIR_VARS SOCI_${C}_INCLUDE_DIR)
      list(APPEND REQUIRED_LIBRARY_VARS     SOCI_${C}_LIBRARY)
    endif()
    find_path(SOCI_${C}_INCLUDE_DIR
      NAMES soci-${COMPONENT}.h
      HINTS ${SOCI_PKG_INCLUDE_DIRS}
      PATHS ${SOCI_INCLUDE_DIR}
            $ENV{SOCI_INCLUDE_DIR}
            ${CMAKE_INSTALL_PREFIX}/include
      PATH_SUFFIXES soci/${COMPONENT})
    mark_as_advanced(SOCI_${C}_INCLUDE_DIR)
    if(SOCI_${C}_INCLUDE_DIR)
      list(APPEND SOCI_INCLUDE_DIRS ${SOCI_${C}_INCLUDE_DIR})
    endif()
  endif()
  find_library(SOCI_${C}_LIBRARY
    NAMES soci_${COMPONENT}
    HINTS ${SOCI_PKG_LIBRARY_DIRS}
    PATHS ${SOCI_LIBRARY_DIR}
          $ENV{SOCI_LIBRARY_DIR}
          ${CMAKE_INSTALL_PREFIX}/lib)
  mark_as_advanced(SOCI_${C}_LIBRARY)
  if(SOCI_${C}_LIBRARY)
    list(APPEND SOCI_LIBRARIES ${SOCI_${C}_LIBRARY})
  endif()
  if(NOT C MATCHES "^CORE$")
    if(SOCI_${C}_INCLUDE_DIR AND SOCI_${C}_LIBRARY)
      set(SOCI_${C}_FOUND Y)
    else()
      set(SOCI_${C}_FOUND N)
    endif()
  endif()
endforeach()
if(REQUIRED_INCLUDE_DIR_VARS)
  list(REMOVE_DUPLICATES REQUIRED_INCLUDE_DIR_VARS)
endif()
if(REQUIRED_LIBRARY_VARS)
  list(REMOVE_DUPLICATES REQUIRED_LIBRARY_VARS)
endif()
##
if(WIN32)
  list(APPEND SOCI_LIBRARIES ws2_32)
endif()
##
find_package_handle_standard_args(SOCI
  FOUND_VAR     SOCI_FOUND
  REQUIRED_VARS ${REQUIRED_LIBRARY_VARS}
                SOCI_LIBRARIES
                ${REQUIRED_INCLUDE_DIR_VARS}
                SOCI_INCLUDE_DIRS)
##
if(SOCI_FOUND AND NOT TARGET SOCI)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(SOCI INTERFACE IMPORTED)
    set_target_property(SOCI
      INTERFACE_COMPILE_DEFINITIONS       ${SOCI_DEFINITIONS})
    set_target_property(SOCI
      INTERFACE_COMPILE_OPTIONS           ${SOCI_OPTIONS})
    set_target_property(SOCI
      INTERFACE_INCLUDE_DIRECTORIES       ${SOCI_INCLUDE_DIRS})
    set_target_property(SOCI
      INTERFACE_LINK_LIBRARIES            ${SOCI_LIBRARIES})
    set_target_property(SOCI
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  endif()
endif()
set(SOCI_CORE_FOUND ${SOCI_FOUND})
foreach(COMPONENT IN LISTS SOCI_FIND_COMPONENTS)
  string(TOUPPER "${COMPONENT}" C)
  if(SOCI_${C}_FOUND AND NOT TARGET SOCI::${C})
    if(NOT CMAKE_VERSION VERSION_LESS 3)
      add_library(SOCI::${C} INTERFACE IMPORTED)
      set_target_property(SOCI::${C}
        INTERFACE_COMPILE_DEFINITIONS       ${SOCI_DEFINITIONS})
      set_target_property(SOCI::${C}
        INTERFACE_COMPILE_OPTIONS           ${SOCI_OPTIONS})
      set_target_property(SOCI::${C}
        INTERFACE_INCLUDE_DIRECTORIES       ${SOCI_INCLUDE_DIRS})
      set_target_property(SOCI::${C}
        INTERFACE_LINK_LIBRARIES            ${SOCI_${C}_LIBRARY})
      set_target_property(SOCI::${C}
        INTERFACE_POSITION_INDEPENDENT_CODE Y)
    endif()
    if(NOT CMAKE_VERSION VERSION_LESS 3.1)
    endif()
  endif()
endforeach()
