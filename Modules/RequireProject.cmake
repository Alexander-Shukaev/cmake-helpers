### Preamble {{{
##  ==========================================================================
##        @file RequireProject.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-21 Thursday 00:12:51 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-02-02 Tuesday 18:56:54 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_REQUIRE_PROJECT_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_REQUIRE_PROJECT_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(require_project)
  if(NOT PROJECT            OR
     NOT PROJECT_NAME       OR
     NOT PROJECT_BINARY_DIR OR
     NOT PROJECT_SOURCE_DIR)
    message(FATAL_ERROR "Project not defined")
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
