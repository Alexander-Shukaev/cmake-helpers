### Preamble {{{
##  ==========================================================================
##        @file OutputGNUCompilerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 13:39:37 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-08 Friday 00:16:51 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_OUTPUT_GNU_COMPILER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_OUTPUT_GNU_COMPILER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Output)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(OUTPUT_PIPES)
  def(OUTPUT_PIPES_GNU_COMPILER_FLAGS -pipe)
endif()
##
if(OUTPUT_TEMPS)
  def(OUTPUT_TEMPS_GNU_COMPILER_FLAGS -save-temps=obj)
endif()
##
def(OUTPUT_GNU_COMPILER_FLAGS
  ${OUTPUT_PIPES_GNU_COMPILER_FLAGS}
  ${OUTPUT_TEMPS_GNU_COMPILER_FLAGS})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
