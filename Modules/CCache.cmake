### Preamble {{{
##  ==========================================================================
##        @file CCache.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:34:59 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-10 Wednesday 21:57:33 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CCACHE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CCACHE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Languages)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
foreach(_L ${LANGUAGES_UPPER})
  if(CMAKE_${_L}_COMPILER_LOADED)
    if(CMAKE_${_L}_COMPILER_ID MATCHES "Clang" OR
       CMAKE_${_L}_COMPILER_ID MATCHES "GNU"   OR
       CMAKE_${_L}_COMPILER_ID MATCHES "Intel")
      option_def_bool(CCACHE
        "Whether to use CCache."
        Y)
      break()
    endif()
  endif()
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CCACHE)
  find_program(CCACHE_COMMAND ccache)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### Properties {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CCACHE)
  set(_MESSAGES)
  if(CCACHE_COMMAND)
    if    (NOT CMAKE_VERSION VERSION_LESS 3.4)
      foreach(_L ${LANGUAGES_UPPER})
        if(CMAKE_${_L}_COMPILER_LOADED)
          set(CMAKE_${_L}_COMPILER_LAUNCHER ${CCACHE_COMMAND})
        endif()
      endforeach()
    elseif(NOT CMAKE_VERSION VERSION_LESS 2.8)
      set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ${CCACHE_COMMAND})
      set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK    ${CCACHE_COMMAND})
    else()
      set(_MESSAGES "Low CMake version: ${CMAKE_VERSION}")
    endif()
  else()
    set(_MESSAGES "Command not found: ${CCACHE_COMMAND}")
  endif()
  if(_MESSAGES AND VERBOSE)
    message(WARNING ${_MESSAGES})
  endif()
  unset(_MESSAGES)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Properties
