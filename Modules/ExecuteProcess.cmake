### Preamble {{{
##  ==========================================================================
##        @file ExecuteProcess.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-11-01 Sunday 01:47:55 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-10-31 Saturday 21:10:07 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_EXECUTE_PROCESS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_EXECUTE_PROCESS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
find_path(EXECUTE_PROCESS_CMAKE_SCRIPT_DIR
  NAMES ExecuteProcess.cmake
  PATHS ${Helpers_CMAKE_DIRS}
        ${Helpers_CMAKE_SCRIPTS_DIRS}
  PATH_SUFFIXES Scripts)
path_def(EXECUTE_PROCESS_CMAKE_SCRIPT
  ${EXECUTE_PROCESS_CMAKE_SCRIPT_DIR}
  ExecuteProcess.cmake)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
