### Preamble {{{
##  ==========================================================================
##        @file CMakeBuildType.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-08 Monday 02:50:12 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-17 Sunday 20:42:42 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CMAKE_BUILD_TYPE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CMAKE_BUILD_TYPE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(NOT CMAKE_CONFIGURATION_TYPES)
  def(CMAKE_BUILD_TYPE Debug)
  string(TOLOWER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_LOWER)
  string(TOUPPER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_UPPER)
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
