### Preamble {{{
##  ==========================================================================
##        @file VisibilityGNUCompilerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 11:08:49 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-17 Friday 00:08:31 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_VISIBILITY_GNU_COMPILER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_VISIBILITY_GNU_COMPILER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Languages)
include(Visibility)
##  ==========================================================================
##  }}} Modules
##
### Policies {{{
##  ==========================================================================
if(POLICY CMP0011)
  cmake_policy(SET CMP0011 NEW)
endif()
if(POLICY CMP0063)
  cmake_policy(SET CMP0063 NEW)
endif()
##  ==========================================================================
##  }}} Policies
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(VISIBILITY_HIDDEN)
  if(NOT CMAKE_VERSION VERSION_LESS 2.8.12 AND POLICY CMP0063)
    foreach(_L ${LANGUAGES_UPPER})
      def(CMAKE_${_L}_VISIBILITY_PRESET hidden)
    endforeach()
  else()
    def(VISIBILITY_HIDDEN_GNU_COMPILER_FLAGS
      -fvisibility=hidden)
  endif()
endif()
##
if(VISIBILITY_INLINES_HIDDEN)
  if(NOT CMAKE_VERSION VERSION_LESS 2.8.12 AND POLICY CMP0063)
    def_bool(CMAKE_VISIBILITY_INLINES_HIDDEN Y)
  else()
    def(VISIBILITY_INLINES_HIDDEN_GNU_COMPILER_FLAGS
      -fvisibility-inlines-hidden)
  endif()
endif()
##
def(VISIBILITY_GNU_COMPILER_FLAGS
  ${VISIBILITY_HIDDEN_GNU_COMPILER_FLAGS}
  ${VISIBILITY_INLINES_HIDDEN_GNU_COMPILER_FLAGS})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
