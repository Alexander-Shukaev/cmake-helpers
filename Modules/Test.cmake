### Preamble {{{
##  ==========================================================================
##        @file Test.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 13:52:11 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-15 Wednesday 21:40:45 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TEST_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TEST_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(TEST_QUIET
  "Whether to suppress output from tests."
  N)
##
option_def_bool(TEST_VERBOSE
  "Whether to print verbose output from tests."
  N)
##
option_def_bool(TEST_EXTRA_VERBOSE
  "Whether to print extra verbose output from tests."
  N)
##
if(NOT CMAKE_VERSION VERSION_LESS 2.8)
  # As per [1]:
  if(DEFINED ENV{CTEST_OUTPUT_ON_FAILURE})
    def_bool(TEST_OUTPUT_ON_FAILURE $ENV{CTEST_OUTPUT_ON_FAILURE})
  endif()
  ##
  option_def_bool(TEST_OUTPUT_ON_FAILURE
    "Whether to print any output from tests on failure only."
    ${VERBOSE})
endif()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(TEST_EXT      .t)
def(TEST_PACKAGES BTEST BOOST_TEST)
def(TEST_TARGET   ctest)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### References {{{
##  ==========================================================================
##  [1] http://gitlab.kitware.com/cmake/cmake/issues/14737
##  ==========================================================================
##  }}} References
