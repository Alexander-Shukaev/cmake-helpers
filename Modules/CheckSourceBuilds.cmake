### Preamble {{{
##  ==========================================================================
##        @file CheckSourceBuilds.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:24:23 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-02 Thursday 22:37:35 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CHECK_SOURCE_BUILDS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CHECK_SOURCE_BUILDS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCSourceCompiles)
include(CheckCXXSourceCompiles)
include(Configurations)
include(UnsetFlags)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(check_source_builds LANGUAGE CODE VAR)
  set(_VAR)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    set(CMAKE_REQUIRED_QUIET Y)
    unset_flags(${LANGUAGE})
    if(LANGUAGE MATCHES "^C$"  )
      check_c_source_compiles  ("${CODE}" _VAR ${ARGN})
    endif()
    if(LANGUAGE MATCHES "^CXX$")
      check_cxx_source_compiles("${CODE}" _VAR ${ARGN})
    endif()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
  unset(_VAR CACHE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
