### Preamble {{{
##  ==========================================================================
##        @file StripGenex.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-10 Sunday 00:05:52 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 01:36:07 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_STRIP_GENEX_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_STRIP_GENEX_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(strip_genex VAR STRING)
  cmake_parse_arguments("" "RECURSE" "" "" ${ARGN})
  set(_REGEX "\\$<.+:([^>]+)>+")
  string(REGEX REPLACE "${_REGEX}" "\\1" _STRING "${STRING}")
  if(_RECURSE)
    while(NOT _STRING STREQUAL STRING)
      set(STRING ${_STRING})
      string(REGEX REPLACE "${_REGEX}" "\\1" _STRING "${STRING}")
    endwhile()
  endif()
  set(${VAR} ${_STRING} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
