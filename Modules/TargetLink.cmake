### Preamble {{{
##  ==========================================================================
##        @file TargetLink.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-04 Saturday 14:37:01 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-22 Wednesday 23:51:14 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_LINK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_LINK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(TargetLinkFlags)
include(TargetLinkLibs)
include(TargetLinkVersionScripts)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_link TARGET)
  set(_KEYWORDS FLAGS
                LIBS
                VERSION_SCRIPTS)
  cmake_parse_arguments("" "" "" "${_KEYWORDS}" ${ARGN})
  # --------------------------------------------------------------------------
  if(_FLAGS)
    target_link_flags(${TARGET} ${_FLAGS})
  endif()
  if(_LIBS)
    target_link_libs(${TARGET} ${_LIBS})
  endif()
  if(_VERSION_SCRIPTS)
    target_link_version_scripts(${TARGET} ${_VERSION_SCRIPTS})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
