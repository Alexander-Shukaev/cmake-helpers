### Preamble {{{
##  ==========================================================================
##        @file FindGRPC.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:43:49 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-16 Wednesday 22:24:48 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Modules {{{
##  ==========================================================================
include(DepGetC89StandardOption)
include(DepPkgCheckModules)
include(FindDepPackage)
include(FindPackageHandleStandardArgs)
include(SetTargetProperty)
##  ==========================================================================
##  }}} Modules
##
dep_pkg_check_modules(GRPC_PKG grpc)
find_path(GRPC_INCLUDE_DIR
  NAMES grpc/grpc.h
  HINTS ${GRPC_PKG_INCLUDE_DIRS}
  PATHS ${GRPC_INCLUDE_DIR}
        $ENV{GRPC_INCLUDE_DIR}
        ${CMAKE_INSTALL_PREFIX}/include
  PATH_SUFFIXES grpc)
mark_as_advanced(GRPC_INCLUDE_DIR)
find_library(GRPC_LIBRARY
  NAMES grpc
  HINTS ${GRPC_PKG_LIBRARY_DIRS}
  PATHS ${GRPC_LIBRARY_DIR}
        $ENV{GRPC_LIBRARY_DIR}
        ${CMAKE_INSTALL_PREFIX}/lib)
mark_as_advanced(GRPC_LIBRARY)
set(GRPC_DEFINITIONS)
set(GRPC_INCLUDE_DIRS ${GRPC_INCLUDE_DIR})
set(GRPC_LIBRARIES    ${GRPC_LIBRARY})
set(GRPC_OPTIONS      ${GRPC_PKG_CFLAGS_OTHER})
dep_get_c89_standard_option(GRPC C89_STANDARD_OPTION)
if(C89_STANDARD_OPTION)
  list(APPEND GRPC_OPTIONS ${C89_STANDARD_OPTION})
endif()
##
list(APPEND GRPC_LIBRARIES gpr)
##
if(DEFINED GRPC_Threads_PREFER_PTHREAD)
  set(CMAKE_THREAD_PREFER_PTHREAD ${GRPC_Threads_PREFER_PTHREAD})
endif()
if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  set(THREADS_PREFER_PTHREAD_FLAG Y)
endif()
dep_find_package(GRPC Threads QUIET)
if(THREADS_FOUND)
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
    list(APPEND GRPC_LIBRARIES Threads::Threads)
  else()
    if(THREADS_HAVE_PTHREAD_ARG)
      list(APPEND GRPC_OPTIONS   -pthread)
    endif()
    if(CMAKE_THREAD_LIBS_INIT)
      list(APPEND GRPC_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
    endif()
  endif()
endif()
##
dep_find_package(GRPC ZLIB QUIET)
if(ZLIB_FOUND)
  list(APPEND GRPC_INCLUDE_DIRS ${ZLIB_INCLUDE_DIRS})
  list(APPEND GRPC_LIBRARIES    ${ZLIB_LIBRARIES})
endif()
##
dep_find_package(GRPC OpenSSL 1.0.1 QUIET)
if(OPENSSL_FOUND)
  list(APPEND GRPC_INCLUDE_DIRS ${OPENSSL_INCLUDE_DIR})
  list(APPEND GRPC_LIBRARIES    ${OPENSSL_LIBRARIES})
endif()
##
if(WIN32)
  list(APPEND GRPC_LIBRARIES ws2_32)
endif()
##
find_package_handle_standard_args(GRPC
  FOUND_VAR     GRPC_FOUND
  REQUIRED_VARS GRPC_LIBRARY
                GRPC_LIBRARIES
                GRPC_INCLUDE_DIR
                GRPC_INCLUDE_DIRS)
##
if(GRPC_FOUND AND NOT TARGET GRPC)
  if(NOT CMAKE_VERSION VERSION_LESS 3)
    add_library(GRPC INTERFACE IMPORTED)
    set_target_property(GRPC
      INTERFACE_COMPILE_DEFINITIONS       ${GRPC_DEFINITIONS})
    set_target_property(GRPC
      INTERFACE_COMPILE_OPTIONS           ${GRPC_OPTIONS})
    set_target_property(GRPC
      INTERFACE_INCLUDE_DIRECTORIES       ${GRPC_INCLUDE_DIRS})
    set_target_property(GRPC
      INTERFACE_LINK_LIBRARIES            ${GRPC_LIBRARIES})
    set_target_property(GRPC
      INTERFACE_POSITION_INDEPENDENT_CODE Y)
  endif()
  if(NOT CMAKE_VERSION VERSION_LESS 3.1)
  endif()
endif()
