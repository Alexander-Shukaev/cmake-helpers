### Preamble {{{
##  ==========================================================================
##        @file CheckCXXCompilerFlag.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-07 Thursday 22:40:12 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CHECK_CXX_COMPILER_FLAG_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CHECK_CXX_COMPILER_FLAG_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCompilerFlag)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(CHECK_CXX_COMPILER_FLAG_CACHE
  "Whether to cache C++ compiler flag checks."
  ${CHECK_COMPILER_FLAG_CACHE})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(check_cxx_compiler_flag FLAG VAR)
  check_compiler_flag(CXX ${FLAG} ${VAR} ${ARGN})
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
