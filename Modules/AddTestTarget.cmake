### Preamble {{{
##  ==========================================================================
##        @file AddTestTarget.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-08 Monday 14:03:41 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 22:56:12 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_TEST_TARGET_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_TEST_TARGET_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(Map)
include(Test)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(add_test_target)
  if(DEFINED BUILD_TESTING AND NOT BUILD_TESTING)
    return()
  endif()
  if(NOT PROJECT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    return()
  endif()
  if(CMAKE_CURRENT_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR)
    enable_testing()
  endif()
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "MAIN" "" "ARGS;ENV" ${ARGN})
  # --------------------------------------------------------------------------
  if(_UNPARSED_ARGUMENTS)
    list(GET _UNPARSED_ARGUMENTS 0 _TARGET)
  else()
    set(_TARGET ${TEST_TARGET})
  endif()
  warn_about_unknown_arguments(1 ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  contains(_D                  _ARGS -D)
  #
  # TODO:
  #
  # Consider excluding
  #
  #   -D <var>:<type>=<value>
  #
  # as well.
  # --------------------------------------------------------------------------
  contains(_DASHBOARD          _ARGS --dashboard)
  contains(_T                  _ARGS -T)
  contains(_TEST_ACTION        _ARGS --test-action)
  contains(_S                  _ARGS -S)
  contains(_SCRIPT             _ARGS --script)
  contains(_SP                 _ARGS -SP)
  contains(_SCRIPT_NEW_PROCESS _ARGS --script-new-process)
  if(NOT _D  AND NOT _DASHBOARD          AND
     NOT _T  AND NOT _TEST_ACTION        AND
     NOT _S  AND NOT _SCRIPT             AND
     NOT _SP AND NOT _SCRIPT_NEW_PROCESS)
    list(APPEND _ARGS -T test)
  endif()
  # --------------------------------------------------------------------------
  contains(_Q                 _ARGS -Q)
  contains(_QUIET             _ARGS --quiet)
  contains(_V                 _ARGS -V)
  contains(_VERBOSE           _ARGS --verbose)
  contains(_VV                _ARGS -VV)
  contains(_EXTRA_VERBOSE     _ARGS --extra-verbose)
  contains(_OUTPUT_ON_FAILURE _ARGS --output-on-failure)
  if(_OUTPUT_ON_FAILURE AND CMAKE_VERSION VERSION_LESS 2.8)
    message(FATAL_ERROR "Invalid option: --output-on-failure")
  endif()
  if(NOT _Q  AND NOT _QUIET             AND
     NOT _V  AND NOT _VERBOSE           AND
     NOT _VV AND NOT _EXTRA_VERBOSE     AND
                 NOT _OUTPUT_ON_FAILURE)
    if(TEST_QUIET)
      list(APPEND _ARGS -Q)
    endif()
    if(TEST_VERBOSE)
      list(APPEND _ARGS -V)
    endif()
    if(TEST_EXTRA_VERBOSE)
      list(APPEND _ARGS -VV)
    endif()
    if(TEST_OUTPUT_ON_FAILURE AND NOT CMAKE_VERSION VERSION_LESS 2.8)
      list(APPEND _ENV  CTEST_OUTPUT_ON_FAILURE=1)
      list(APPEND _ARGS --output-on-failure)
    endif()
  endif()
  # --------------------------------------------------------------------------
  if(CMAKE_CONFIGURATION_TYPES)
    contains(_C            _ARGS -C)
    contains(_BUILD_CONFIG _ARGS --build-config)
    if(NOT _C AND NOT _BUILD_CONFIG)
      set(_ARGS -C \$<CONFIGURATION> ${_ARGS})
    endif()
  endif()
  # --------------------------------------------------------------------------
  contains(_FORCE_NEW_CTEST_PROCESS _ARGS --force-new-ctest-process)
  if(NOT _FORCE_NEW_CTEST_PROCESS)
    set(_ARGS --force-new-ctest-process ${_ARGS})
  endif()
  # --------------------------------------------------------------------------
  if(_ENV AND NOT CMAKE_VERSION VERSION_LESS 3.1)
    add_custom_target(${_TARGET}
      COMMAND ${CMAKE_COMMAND}       -E env ${_ENV}
              ${CMAKE_CTEST_COMMAND}        ${_ARGS}
      VERBATIM)
  else()
    add_custom_target(${_TARGET}
      COMMAND ${CMAKE_CTEST_COMMAND}        ${_ARGS}
      VERBATIM)
  endif()
  # --------------------------------------------------------------------------
  string(REPLACE "\\" "\\\\" _ARGS "${_ARGS}")
  map_set(_ADD_TEST_TARGET_ARGS ${_TARGET} ${_ARGS}
    CACHE INTERNAL
    "The arguments for the CTest command of the \"${_TARGET}\" test target.")
  if(_MAIN)
    set(TEST_MAIN_TARGET ${_TARGET}
      CACHE INTERNAL
      "Test main target.")
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
