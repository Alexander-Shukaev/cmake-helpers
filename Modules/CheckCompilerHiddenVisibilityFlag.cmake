### Preamble {{{
##  ==========================================================================
##        @file CheckCompilerHiddenVisibilityFlag.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:14:24 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-02 Thursday 20:10:08 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CHECK_COMPILER_HIDDEN_VISIBILITY_FLAG_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CHECK_COMPILER_HIDDEN_VISIBILITY_FLAG_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCompilerFlag)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(check_compiler_hidden_visibility_flag LANGUAGE VAR)
  set(_VAR)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    set(  _GNU_IS_OLD N)
    set(_INTEL_IS_OLD N)
    if(CMAKE_${LANGUAGE}_COMPILER_ID      MATCHES      "GNU"   AND
       CMAKE_${LANGUAGE}_COMPILER_VERSION VERSION_LESS 4.2)
      set(  _GNU_IS_OLD Y)
    endif()
    if(CMAKE_${LANGUAGE}_COMPILER_ID      MATCHES      "Intel" AND
       CMAKE_${LANGUAGE}_COMPILER_VERSION VERSION_LESS 12)
      set(_INTEL_IS_OLD Y)
    endif()
    if(NOT WIN32                                          AND
       NOT CYGWIN                                         AND
       NOT   _GNU_IS_OLD                                  AND
       NOT _INTEL_IS_OLD                                  AND
       NOT CMAKE_${LANGUAGE}_COMPILER_ID MATCHES "PGI"    AND
       NOT CMAKE_${LANGUAGE}_COMPILER_ID MATCHES "Watcom" AND
       NOT CMAKE_${LANGUAGE}_COMPILER_ID MATCHES "XL")
      check_compiler_flag(${LANGUAGE} -fvisibility=hidden _VAR ${ARGN})
    endif()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
