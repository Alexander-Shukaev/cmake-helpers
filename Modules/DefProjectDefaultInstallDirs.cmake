### Preamble {{{
##  ==========================================================================
##        @file DefProjectDefaultInstallDirs.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 23:53:07 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-01-16 Saturday 03:00:14 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DEF_PROJECT_DEFAULT_INSTALL_DIRS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DEF_PROJECT_DEFAULT_INSTALL_DIRS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(GuessDefaultInstallDirs)
include(Path)
include(RequireProjectSourceDir)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(def_project_default_install_dirs)
  require_project_source_dir()
  # --------------------------------------------------------------------------
  if(WIN32 AND NOT CYGWIN AND NOT MSYS)
    path_def(INSTALL_CMAKE_DIR CMake)
  else()
    path_def(INSTALL_CMAKE_DIR share cmake ${PROJECT})
  endif()
  path_def(INSTALL_INCLUDE_DIR include)
  path_def(INSTALL_ARCHIVE_DIR lib)
  path_def(INSTALL_LIBRARY_DIR lib)
  path_def(INSTALL_RUNTIME_DIR bin)
  # --------------------------------------------------------------------------
  guess_default_install_dirs()
  # --------------------------------------------------------------------------
  set(${PROJECT}_INSTALL_CMAKE_DIR   ${INSTALL_CMAKE_DIR}
    CACHE PATH
    "The install directory for CMake files of \"${PROJECT}\".")
  set(${PROJECT}_INSTALL_INCLUDE_DIR ${INSTALL_INCLUDE_DIR}
    CACHE PATH
    "The install directory for header files of \"${PROJECT}\".")
  set(${PROJECT}_INSTALL_ARCHIVE_DIR ${INSTALL_ARCHIVE_DIR}
    CACHE PATH
    "The install directory for archive files of \"${PROJECT}\".")
  set(${PROJECT}_INSTALL_LIBRARY_DIR ${INSTALL_LIBRARY_DIR}
    CACHE PATH
    "The install directory for library files of \"${PROJECT}\".")
  set(${PROJECT}_INSTALL_RUNTIME_DIR ${INSTALL_RUNTIME_DIR}
    CACHE PATH
    "The install directory for runtime files of \"${PROJECT}\".")
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
