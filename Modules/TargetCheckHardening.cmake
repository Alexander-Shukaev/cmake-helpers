### Preamble {{{
##  ==========================================================================
##        @file TargetCheckHardening.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:37:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-03 Sunday 20:45:00 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_HARDENING_CHECK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_HARDENING_CHECK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Prerequisites {{{
##  ==========================================================================
if(WIN32)
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(CheckLinkerNoexecheapFlag)
include(HardeningCheck)
include(OptionDef)
include(Verbose)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(TARGET_CHECK_HARDENING
  "Whether to check for security hardening features in the ELF targets."
  ${HARDENING_CHECK})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_check_hardening TARGET)
  set(_OPTIONS)
  foreach(_FEATURE ${HARDENING_FEATURES})
    list(APPEND _OPTIONS NO_${_FEATURE})
  endforeach()
  cmake_parse_arguments("" "${_OPTIONS}" "" "" ${ARGN})
  # --------------------------------------------------------------------------
  if(VERBOSE)
    list(APPEND _UNPARSED_ARGUMENTS --verbose)
  endif()
  # --------------------------------------------------------------------------
  list(APPEND _UNPARSED_ARGUMENTS --color)
  #
  # NOTE:
  #
  # Ninja unconditionally strips ANSI escape sequences (because of pipe).
  # --------------------------------------------------------------------------
  foreach(_FEATURE ${HARDENING_FEATURES})
    if(NOT _NO_${_FEATURE} AND NOT HARDENING_CHECK_${_FEATURE})
      set(_NO_${_FEATURE} Y)
    endif()
  endforeach()
  # --------------------------------------------------------------------------
  if(NOT _NO_NEH)
    get_target_property(_LANGUAGE ${TARGET} LINKER_LANGUAGE)
    if(NOT _LANGUAGE)
      set(_LANGUAGE CXX)
    endif()
    check_linker_noexecheap_flag(${_LANGUAGE} _SUPPORTED)
    if(NOT _SUPPORTED)
      set(_NO_NEH Y)
    endif()
  endif()
  # --------------------------------------------------------------------------
  foreach(_FEATURE ${HARDENING_FEATURES})
    if(_NO_${_FEATURE})
      string(TOLOWER "${_FEATURE}" _FEATURE_LOWER)
      list(APPEND _UNPARSED_ARGUMENTS --no-${_FEATURE_LOWER})
    endif()
  endforeach()
  # --------------------------------------------------------------------------
  # TODO:
  #
  # `HARDENING_CHECK_COMMAND' may come from 'HardeningCheckConfig.cmake'.
  #
  if(NOT HARDENING_CHECK_COMMAND)
    find_program(HARDENING_CHECK_COMMAND hardening-check)
  endif()
  # --------------------------------------------------------------------------
  add_custom_command(TARGET ${TARGET} POST_BUILD
    COMMAND ${HARDENING_CHECK_COMMAND}
    ARGS ${_UNPARSED_ARGUMENTS} $<TARGET_FILE_NAME:${TARGET}>
    VERBATIM)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
