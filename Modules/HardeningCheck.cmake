### Preamble {{{
##  ==========================================================================
##        @file HardeningCheck.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-03 Sunday 18:55:54 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_HARDENING_CHECK_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_HARDENING_CHECK_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Prerequisites {{{
##  ==========================================================================
if(WIN32)
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Modules {{{
##  ==========================================================================
include(Hardening)
include(OptionDef)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(HARDENING_CHECK
  "Whether to check for security hardening features in the ELF binaries."
  ${HARDENING})
##
# Position Independent Executable (PIE):
option_def_bool(HARDENING_CHECK_PIE
  "Check that the ELF binaries are built as PIE."
  ${HARDENING_PIE})
##
# Stack Smashing Protector (SSP):
option_def_bool(HARDENING_CHECK_SSP
  "Check that the ELF binaries are compiled with SSP."
  ${HARDENING_SSP})
##
# Fortified Functions (FFs):
option_def_bool(HARDENING_CHECK_FFS
  "Check that the ELF binaries are compiled with FFs."
  ${HARDENING_FFS})
##
# String Format Security Functions (SFSFs):
option_def_bool(HARDENING_CHECK_SFSFS
  "Check that the ELF binaries are compiled with SFSFs."
  ${HARDENING_SFSFS})
##
# Non-Executable Stack (NES):
option_def_bool(HARDENING_CHECK_NES
  "Check that the ELF binaries are linked with NES."
  ${HARDENING_NES})
##
# Non-Executable Heap (NEH):
option_def_bool(HARDENING_CHECK_NEH
  "Check that the ELF binaries are linked with NEH."
  ${HARDENING_NEH})
##
# Relocation Read-Only (RELRO):
option_def_bool(HARDENING_CHECK_RELRO
  "Check that the ELF binaries are linked with RELRO."
  ${HARDENING_RELRO})
##
# Immediate Symbol Binding (NOW):
option_def_bool(HARDENING_CHECK_NOW
  "Check that the ELF binaries are linked with NOW."
  ${HARDENING_NOW})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
