### Preamble {{{
##  ==========================================================================
##        @file GetCXX11StandardOption.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:59:03 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-05-13 Friday 00:06:40 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GET_CXX11_STANDARD_OPTION_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GET_CXX11_STANDARD_OPTION_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(GetCXX11StandardFlag)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(get_cxx11_standard_option VAR)
  get_cxx11_standard_flag(_F ${ARGN})
  if(_F)
    if(NOT CMAKE_VERSION VERSION_LESS 2.8.12)
      set(_F \$<\$<STREQUAL:\$<TARGET_PROPERTY:LINKER_LANGUAGE>,CXX>:${_F}>)
    endif()
    if(NOT CMAKE_VERSION VERSION_LESS 3.3)
      set(_F \$<\$<COMPILE_LANGUAGE:CXX>:${_F}>)
    endif()
  endif()
  set(${VAR} ${_F} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
