### Preamble {{{
##  ==========================================================================
##        @file HRule.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-01-11 Wednesday 01:13:20 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-01-11 Wednesday 00:45:49 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_HRULE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_HRULE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
include(Glue)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def (HRULE1  -)
glue(HRULE2  ${HRULE1}  ${HRULE1})
glue(HRULE4  ${HRULE2}  ${HRULE2})
glue(HRULE8  ${HRULE4}  ${HRULE4})
glue(HRULE16 ${HRULE8}  ${HRULE8})
glue(HRULE32 ${HRULE16} ${HRULE16})
glue(HRULE64 ${HRULE32} ${HRULE32})
glue(HRULE80 ${HRULE64} ${HRULE16})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
