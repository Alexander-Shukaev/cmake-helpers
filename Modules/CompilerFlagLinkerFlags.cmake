### Preamble {{{
##  ==========================================================================
##        @file CompilerFlagLinkerFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-03-03 Friday 00:35:36 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-03-03 Friday 00:00:17 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_COMPILER_FLAG_LINKER_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_COMPILER_FLAG_LINKER_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Map)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(compiler_flag_linker_flags_def PREFIX FLAG)
  map_def(${PREFIX}_COMPILER_FLAG_LINKER_FLAGS ${FLAG} ${ARGN}
    CACHE INTERNAL
    "Linker flags for the \"${FLAG}\" compiler flag.")
endmacro()
##
macro(compiler_flag_linker_flags_set PREFIX FLAG)
  map_set(${PREFIX}_COMPILER_FLAG_LINKER_FLAGS ${FLAG} ${ARGN}
    CACHE INTERNAL
    "Linker flags for the \"${FLAG}\" compiler flag.")
endmacro()
##
macro(compiler_flag_linker_flags_get PREFIX FLAG VAR)
  map_get(${PREFIX}_COMPILER_FLAG_LINKER_FLAGS ${FLAG} ${VAR} ${ARGN})
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
