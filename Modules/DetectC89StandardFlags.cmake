### Preamble {{{
##  ==========================================================================
##        @file DetectC89StandardFlags.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:00:42 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-04 Monday 23:33:48 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DETECT_C89_STANDARD_FLAGS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DETECT_C89_STANDARD_FLAGS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(FilterCCompilerFlags)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(detect_c89_standard_flags VAR)
  filter_c_compiler_flags(${VAR}
    -std=c89             # |Clang|    |   |GNU|  |IBM|Intel|Oracle|   |
    -std=gnu89           # |Clang|    |   |GNU|  |IBM|Intel|      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    /Qstd=c89            # |     |    |   |   |  |   |Intel|      |   |
    /Qstd=gnu89          # |     |    |   |   |  |   |Intel|      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    --c89                # |     |    |EDG|   |  |   |     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -c89                 # |     |    |   |   |  |   |     |      |PGI|
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -qlanglvl=stdc89     # |     |    |   |   |  |IBM|     |      |   |
    -qlanglvl=extc89     # |     |    |   |   |  |IBM|     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -h std=c89           # |     |Cray|   |   |  |   |     |      |   |
    # ------------------ # |-----|----|---|---|--|---|-----|------|---|
    -AC89                # |     |    |   |   |HP|   |     |      |   |
    +std=c89             # |     |    |   |   |HP|   |     |      |   |
    )
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
