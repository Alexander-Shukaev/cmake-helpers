### Preamble {{{
##  ==========================================================================
##        @file Add.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-05 Sunday 23:11:13 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-22 Wednesday 23:03:05 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_ADD_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_ADD_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Contains)
include(ParseSeparateArguments)
include(Target)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(add TYPE TARGET)
  set(_TYPES CUSTOM)
  contains(_TYPE_IS_CUSTOM     _TYPES ${TYPE})
  set(_TYPES EXE
             EXECUTABLE)
  contains(_TYPE_IS_EXECUTABLE _TYPES ${TYPE})
  set(_TYPES LIB
             LIBRARY)
  contains(_TYPE_IS_LIBRARY    _TYPES ${TYPE})
  # --------------------------------------------------------------------------
  if(_TYPE_IS_CUSTOM)
    add_custom_target(${TARGET} ${ARGN})
    return()
  endif()
  # --------------------------------------------------------------------------
  set(_UNPARSED_ARGUMENTS ${ARGN})
  if(_TYPE_IS_EXECUTABLE OR
     _TYPE_IS_LIBRARY)
    parse_separate_arguments("" "${TEST_PACKAGES}" ${_UNPARSED_ARGUMENTS})
  endif()
  cmake_parse_arguments("" "" "" "TARGET" ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_TYPE_IS_EXECUTABLE)
    add_executable(${TARGET} ${_UNPARSED_ARGUMENTS})
  endif()
  # --------------------------------------------------------------------------
  if(_TYPE_IS_LIBRARY)
    add_library   (${TARGET} ${_UNPARSED_ARGUMENTS})
  endif()
  # --------------------------------------------------------------------------
  if(_TYPE_IS_EXECUTABLE OR
     _TYPE_IS_LIBRARY)
    get_target_property(_TYPE ${TARGET} TYPE)
    set(_TYPES INTERFACE_LIBRARY)
    contains(_TYPE_IS_INTERFACE_LIBRARY _TYPES ${_TYPE})
    if(_TARGET)
      if(_TYPE_IS_INTERFACE_LIBRARY)
        set(TARGET_CHECK_HARDENING N)
      endif()
      target(${TARGET} ${_TARGET})
    endif()
    set(_POLICY CMP0063)
    if(POLICY ${_POLICY} AND NOT _TYPE_IS_INTERFACE_LIBRARY)
      cmake_policy(GET ${_POLICY} _BEHAVIOR)
      get_target_property(_LANGUAGE ${TARGET} LINKER_LANGUAGE)
      if(NOT _LANGUAGE)
        set(_LANGUAGE CXX)
      endif()
      get_target_property(_VISIBILITY_PRESET         ${TARGET}
        ${_LANGUAGE}_VISIBILITY_PRESET)
      get_target_property(_VISIBILITY_INLINES_HIDDEN ${TARGET}
        VISIBILITY_INLINES_HIDDEN)
      if(NOT _BEHAVIOR AND (_VISIBILITY_PRESET OR _VISIBILITY_INLINES_HIDDEN))
        message(FATAL_ERROR "Policy not set: ${_POLICY}")
      endif()
    endif()
    foreach(_PACKAGE ${TEST_PACKAGES})
      if(NOT _${_PACKAGE}_MAX LESS 0)
        foreach(_INDEX RANGE ${_${_PACKAGE}_MAX})
          set(TARGET_CHECK N)
          target(${TARGET} MAKE TEST ${_PACKAGE} ${_${_PACKAGE}_${_INDEX}})
        endforeach()
      endif()
    endforeach()
  else()
    message(FATAL_ERROR "Unknown target type: ${TYPE}")
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
