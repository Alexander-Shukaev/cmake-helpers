### Preamble {{{
##  ==========================================================================
##        @file GetCXX11StandardFlag.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:02:37 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-05-12 Thursday 21:46:14 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_GET_CXX11_STANDARD_FLAG_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_GET_CXX11_STANDARD_FLAG_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(DetectCXX11StandardFlags)
include(WarnAboutUnknownArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(get_cxx11_standard_flag VAR)
  cmake_parse_arguments("" "REQUIRED;QUIET" "" "" ${ARGN})
  warn_about_unknown_arguments(0 ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  set(_VAR)
  set(_MESSAGES)
  if(CMAKE_CXX_COMPILER_LOADED)
    detect_cxx11_standard_flags(_FLAGS)
    if(_FLAGS)
      list(GET _FLAGS 0 _VAR)
    elseif(NOT MSVC OR MSVC_VERSION LESS 1800)
      set(_MESSAGES "C++11 standard not supported: ${CMAKE_CXX_COMPILER}")
    endif()
  else()
    set(_MESSAGES "C++ compiler not loaded")
  endif()
  if(_MESSAGES)
    if(_REQUIRED)
      message(FATAL_ERROR ${_MESSAGES})
    endif()
    if(NOT _QUIET)
      message(WARNING     ${_MESSAGES})
    endif()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
