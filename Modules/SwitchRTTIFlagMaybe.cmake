### Preamble {{{
##  ==========================================================================
##        @file SwitchRTTIFlagMaybe.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-20 Saturday 23:31:43 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-08-20 Saturday 23:11:09 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_SWITCH_RTTI_FLAG_MAYBE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_SWITCH_RTTI_FLAG_MAYBE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddNoRTTIFlagMaybe)
include(AddRTTIFlagMaybe)
include(CMakeParseArguments)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(switch_rtti_flag_maybe)
  if(ARGC)
    set(_RTTI    ${ARGV0})
    set(_OPTIONS REQUIRED
                 QUIET)
    contains(_RTTI_IS_OPTION _OPTIONS ${_RTTI})
    if(_RTTI_IS_OPTION)
      if(DEFINED RTTI)
        set(_RTTI ${RTTI})
      else()
        unset(_RTTI)
      endif()
    else()
      list(REMOVE_AT ARGN 0)
    endif()
  endif()
  if(NOT DEFINED _RTTI)
    return()
  endif()
  if(_RTTI)
    add_rtti_flag_maybe(${ARGN})
  else()
    add_no_rtti_flag_maybe(${ARGN})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
