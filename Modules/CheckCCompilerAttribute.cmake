### Preamble {{{
##  ==========================================================================
##        @file CheckCCompilerAttribute.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-01 Wednesday 23:00:37 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CHECK_C_COMPILER_ATTRIBUTE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CHECK_C_COMPILER_ATTRIBUTE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCompilerAttribute)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(CHECK_C_COMPILER_ATTRIBUTE_CACHE
  "Whether to cache C compiler attribute checks."
  ${CHECK_COMPILER_ATTRIBUTE_CACHE})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(check_c_compiler_attribute ATTRIBUTE VAR)
  check_compiler_attribute(C ${ATTRIBUTE} ${VAR} ${ARGN})
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
