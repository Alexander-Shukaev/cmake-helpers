### Preamble {{{
##  ==========================================================================
##        @file DetectImportAttributes.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-09 Saturday 23:10:46 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-03 Friday 22:37:34 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_DETECT_IMPORT_ATTRIBUTES_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_DETECT_IMPORT_ATTRIBUTES_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(FilterCompilerAttributes)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(detect_import_attributes LANGUAGE VAR)
  filter_compiler_attributes(${LANGUAGE} ${VAR}
    "__attribute__((__visibility__(\"default\")))" # |Clang|GNU|Intel|  |
    "__attribute__((visibility(\"default\")))"     # |Clang|GNU|Intel|  |
    # -------------------------------              # |-----|---|-----|--|
    "__declspec(dllimport)"                        # |     |   |     |MS|
    )
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
