### Preamble {{{
##  ==========================================================================
##        @file TargetCompileDefs.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-20 Wednesday 22:47:35 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-22 Wednesday 23:58:04 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_COMPILE_DEFS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_COMPILE_DEFS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddTargetProperty)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(target_compile_defs TARGET)
  if(CMAKE_VERSION VERSION_LESS 2.8.11)
    add_target_property(${TARGET} COMPILE_DEFINITIONS ${ARGN})
  else()
    target_compile_definitions(${TARGET} ${ARGN})
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
