### Preamble {{{
##  ==========================================================================
##        @file Target.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-05 Sunday 23:12:42 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-23 Thursday 00:08:14 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(TargetCheck)
include(TargetCompile)
include(TargetGenerate)
include(TargetInclude)
include(TargetInstall)
include(TargetLink)
include(TargetMake)
include(TargetSymlink)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target TARGET)
  set(_UNPARSED_ARGUMENTS ${ARGN})
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "" "" "MAKE"     ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "CHECK"    ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "GENERATE" ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "LINK"     ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "INCLUDE"  ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "COMPILE"  ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "SYMLINK"  ${_UNPARSED_ARGUMENTS})
  cmake_parse_arguments("" "" "" "INSTALL"  ${_UNPARSED_ARGUMENTS})
  #
  # CAUTION:
  #
  # The order of `cmake_parse_arguments' calls above matters significantly!
  # For instance, this function accepts `GENERATE' as a multi-value argument
  # that (in turn) accepts `VERSION_SCRIPT' as a multi-value argument that (in
  # turn) accepts `LINK' as an option and, at the same time, this function
  # accepts `LINK' as a multi-value argument.  To prevent unexpected behavior
  # due to clash between `LINK' option and `LINK' multi-value argument, it is
  # absolutely necessary to parse arguments of `GENERATE' before `LINK'.
  # Another example is the `INSTALL' multi-value argument.
  # --------------------------------------------------------------------------
  if(_CHECK OR TARGET_CHECK)
    target_check   (${TARGET} ${_CHECK})
  endif()
  if(_GENERATE)
    target_generate(${TARGET} ${_GENERATE})
  endif()
  if(_LINK)
    target_link    (${TARGET} ${_LINK})
  endif()
  if(_INCLUDE)
    target_include (${TARGET} ${_INCLUDE})
  endif()
  if(_COMPILE)
    target_compile (${TARGET} ${_COMPILE})
  endif()
  if(_SYMLINK)
    target_symlink (${TARGET} ${_SYMLINK})
  endif()
  if(_MAKE)
    target_make    (${TARGET} ${_MAKE})
  endif()
  if(_INSTALL)
    target_install (${TARGET} ${_INSTALL})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
