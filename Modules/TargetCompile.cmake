### Preamble {{{
##  ==========================================================================
##        @file TargetCompile.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-04 Saturday 14:36:00 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-22 Wednesday 23:56:27 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_COMPILE_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_COMPILE_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(TargetCompileDefs)
include(TargetCompileOpts)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_compile TARGET)
  set(_KEYWORDS DEFS
                OPTS)
  cmake_parse_arguments("" "" "" "${_KEYWORDS}" ${ARGN})
  # --------------------------------------------------------------------------
  if(_DEFS)
    target_compile_defs(${TARGET} ${_DEFS})
  endif()
  if(_OPTS)
    target_compile_opts(${TARGET} ${_OPTS})
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
