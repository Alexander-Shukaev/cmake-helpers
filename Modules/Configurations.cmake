### Preamble {{{
##  ==========================================================================
##        @file Configurations.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-07-17 Sunday 22:06:01 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-07-06 Wednesday 23:38:08 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CONFIGURATIONS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CONFIGURATIONS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
if(CMAKE_CONFIGURATION_TYPES)
  def(CONFIGURATIONS ${CMAKE_CONFIGURATION_TYPES})
else()
  def(CONFIGURATIONS
    Debug
    Release
    RelWithDebInfo
    MinSizeRel)
endif()
set(CONFIGURATIONS_LOWER)
set(CONFIGURATIONS_UPPER)
foreach(_C ${CONFIGURATIONS})
  string(TOLOWER "${_C}" _C_LOWER)
  string(TOUPPER "${_C}" _C_UPPER)
  list(APPEND CONFIGURATIONS_LOWER ${_C_LOWER})
  list(APPEND CONFIGURATIONS_UPPER ${_C_UPPER})
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
