### Preamble {{{
##  ==========================================================================
##        @file Contains.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-08 Monday 14:29:09 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-11-05 Thursday 02:00:50 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_CONTAINS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_CONTAINS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Policies {{{
##  ==========================================================================
if(POLICY CMP0011)
  cmake_policy(SET CMP0011 NEW)
endif()
if(POLICY CMP0057)
  cmake_policy(SET CMP0057 NEW)
endif()
##  ==========================================================================
##  }}} Policies
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(contains VAR LIST_VAR STRING)
  set(${VAR} N PARENT_SCOPE)
  if(CMAKE_VERSION VERSION_LESS 3.3)
    if(";${${LIST_VAR}};" MATCHES ";${STRING};")
      set(${VAR} Y PARENT_SCOPE)
    endif()
  else()
    if(STRING IN_LIST ${LIST_VAR})
      set(${VAR} Y PARENT_SCOPE)
    endif()
  endif()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
