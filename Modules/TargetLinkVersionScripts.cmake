### Preamble {{{
##  ==========================================================================
##        @file TargetLinkVersionScripts.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-04 Saturday 14:37:29 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-18 Saturday 10:16:49 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_LINK_VERSION_SCRIPTS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_LINK_VERSION_SCRIPTS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(AddSourceFileProperty)
include(CMakeParseArguments)
include(GenerateVersionScript)
include(Path)
include(TargetLinkFlags)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_link_version_scripts TARGET)
  cmake_parse_arguments("" "" "" "LANGUAGES" ${ARGN})
  # --------------------------------------------------------------------------
  if(NOT _LANGUAGES)
    set(_LANGUAGES ${VERSION_SCRIPT_LANGUAGES})
  endif()
  set(_VERSION_SCRIPTS)
  if(NOT _UNPARSED_ARGUMENTS)
    set(_UNPARSED_ARGUMENTS ${TARGET}${VERSION_SCRIPT_EXT})
  endif()
  foreach(_FILE ${_UNPARSED_ARGUMENTS})
    set(_VERSION_SCRIPT ${_FILE})
    if(NOT IS_ABSOLUTE ${_FILE})
      get_filename_component(_VERSION_SCRIPT ${_FILE} ABSOLUTE)
      if(NOT EXISTS   "${_VERSION_SCRIPT}" OR
         IS_DIRECTORY "${_VERSION_SCRIPT}")
        path(_VERSION_SCRIPT ${CMAKE_CURRENT_BINARY_DIR} ${_FILE})
      endif()
    endif()
    list(APPEND _VERSION_SCRIPTS ${_VERSION_SCRIPT})
  endforeach()
  get_target_property(_SOURCES ${TARGET} SOURCES)
  foreach(_SOURCE ${_SOURCES})
    get_source_file_property(_LANGUAGE ${_SOURCE} LANGUAGE)
    contains(_LANGUAGE_IS_LANGUAGE _LANGUAGES ${_LANGUAGE})
    if(_LANGUAGE_IS_LANGUAGE)
      add_source_file_property(${_SOURCE} OBJECT_DEPENDS ${_VERSION_SCRIPTS})
    endif()
  endforeach()
  foreach(_VERSION_SCRIPT ${_VERSION_SCRIPTS})
    target_link_flags(${TARGET} -Wl,--version-script='${_VERSION_SCRIPT}')
  endforeach()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
