### Preamble {{{
##  ==========================================================================
##        @file Install.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-03 Friday 23:30:43 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-02-03 Friday 23:19:17 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_INSTALL_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_INSTALL_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(INSTALL_DIR_KINDS
  CMAKE
  INCLUDE
  ARCHIVE
  LIBRARY
  RUNTIME)
##
def(INSTALL_EXECUTABLE_KIND     RUNTIME)
def(INSTALL_MODULE_LIBRARY_KIND LIBRARY)
def(INSTALL_OBJECT_LIBRARY_KIND ARCHIVE)
def(INSTALL_SHARED_LIBRARY_KIND LIBRARY)
def(INSTALL_STATIC_LIBRARY_KIND ARCHIVE)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
