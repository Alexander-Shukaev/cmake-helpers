### Preamble {{{
##  ==========================================================================
##        @file FilterCompilerAttributes.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:02:30 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-02 Thursday 23:24:38 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_FILTER_COMPILER_ATTRIBUTES_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_FILTER_COMPILER_ATTRIBUTES_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CheckCompilerAttribute)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(filter_compiler_attributes LANGUAGE VAR)
  set(_VAR)
  if(CMAKE_${LANGUAGE}_COMPILER_LOADED)
    foreach(_ATTRIBUTE ${ARGN})
      check_compiler_attribute(${LANGUAGE} ${_ATTRIBUTE} _SUPPORTED)
      if(_SUPPORTED)
        list(APPEND _VAR ${_ATTRIBUTE})
      endif()
    endforeach()
  endif()
  set(${VAR} ${_VAR} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
