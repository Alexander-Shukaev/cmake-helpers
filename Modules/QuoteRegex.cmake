### Preamble {{{
##  ==========================================================================
##        @file QuoteRegex.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-11-13 Sunday 12:39:17 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-06-11 Saturday 01:36:07 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_QUOTE_REGEX_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_QUOTE_REGEX_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(quote_regex VAR STRING)
  set(_REGEX "[][)(^$.*+?|]")
  string(REGEX REPLACE "${_REGEX}" "\\\\\\0" STRING "${STRING}")
  set(${VAR} ${STRING} PARENT_SCOPE)
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
