### Preamble {{{
##  ==========================================================================
##        @file IncludeCTest.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 01:36:50 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 19:05:09 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_INCLUDE_CTEST_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_INCLUDE_CTEST_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(RequireProjectSourceDir)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(include_ctest)
  require_project_source_dir()
  # --------------------------------------------------------------------------
  if(NOT DEFINED BUILD_TESTING)
    if(CMAKE_VERSION VERSION_LESS 2.6)
      include(Dart)
    else()
      include(CTest)
    endif()
  endif()
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
