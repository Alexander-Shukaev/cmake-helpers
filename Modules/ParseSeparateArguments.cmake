### Preamble {{{
##  ==========================================================================
##        @file ParseSeparateArguments.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 00:11:23 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-07-16 Saturday 17:37:07 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_PARSE_SEPARATE_ARGUMENTS_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_PARSE_SEPARATE_ARGUMENTS_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(parse_separate_arguments PREFIX KEYWORDS)
  set(${PREFIX}_UNPARSED_ARGUMENTS PARENT_SCOPE)
  set(KEYWORDS ${KEYWORDS})
  string(      REPLACE ";"                  "|"         _R   "(${KEYWORDS})")
  string(      REPLACE "\\"                 "\\\\"      ARGN    "${ARGN}")
  string(      REPLACE "\\;"                "\\\\;"     ARGN    "${ARGN}")
  string(      REPLACE ";"                  "\\;"       ARGN    "${ARGN}")
  string(REGEX REPLACE "[\\];${_R}[\\];"    ";\\1;"     ARGN "\\;${ARGN}\\;")
  string(REGEX REPLACE "([^\\]);${_R}[\\];" "\\1;;\\2;" ARGN    "${ARGN}")
  string(REGEX REPLACE "^[\\];"             ""          ARGN    "${ARGN}")
  string(REGEX REPLACE "[\\];$"             ""          ARGN    "${ARGN}")
  string(FIND "${ARGN}" ";" _INDEX)
  set(_HAS_UNPARSED N)
  if(_INDEX GREATER 0)
    set(_HAS_UNPARSED Y)
  endif()
  set(_INDEX -1)
  foreach(_KEYWORD ${KEYWORDS})
    set(_${_KEYWORD}_INDEX -1)
  endforeach()
  set(_IS_KEYWORD N)
  foreach(_ARG IN LISTS ARGN)
    if(_IS_KEYWORD)
      set(_KEYWORD   ${_ARG})
      set(_INDEX_VAR _${_KEYWORD}_INDEX)
      if(${_INDEX_VAR} EQUAL -1)
        set(${_INDEX_VAR} 0)
      endif()
      set(_IS_KEYWORD N)
    else()
      string(REPLACE "\\;"   ";"   _ARG "${_ARG}")
      string(REPLACE "\\\\;" "\\;" _ARG "${_ARG}")
      string(REPLACE "\\\\"  "\\"  _ARG "${_ARG}")
      if(_INDEX EQUAL -1)
        if(_HAS_UNPARSED)
          set(${PREFIX}_UNPARSED_ARGUMENTS "${_ARG}" PARENT_SCOPE)
          set(_HAS_UNPARSED N)
        endif()
        set(_INDEX 0)
      else()
        set(${PREFIX}_${_KEYWORD}_${${_INDEX_VAR}} "${_ARG}" PARENT_SCOPE)
        math(EXPR ${_INDEX_VAR} "${${_INDEX_VAR}} + 1")
        math(EXPR _INDEX        "${_INDEX} + 1")
      endif()
      set(_IS_KEYWORD Y)
    endif()
  endforeach()
  if(NOT _INDEX LESS 0)
    math(EXPR _INDEX "${_INDEX} - 1")
  endif()
  foreach(_KEYWORD ${KEYWORDS})
    set(_INDEX_VAR _${_KEYWORD}_INDEX)
    if(NOT ${_INDEX_VAR} LESS 0)
      math(EXPR ${_INDEX_VAR} "${${_INDEX_VAR}} - 1")
    endif()
    set(${PREFIX}_${_KEYWORD}_MAX ${${_INDEX_VAR}} PARENT_SCOPE)
    set(${PREFIX}_${_KEYWORD}_MIN 0                PARENT_SCOPE)
  endforeach()
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
