### Preamble {{{
##  ==========================================================================
##        @file Map.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-03-02 Thursday 23:55:12 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-05-08 Sunday 17:36:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_MAP_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_MAP_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Macros {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
macro(map_var PREFIX KEY VAR)
  string(MAKE_C_IDENTIFIER ${KEY} _MAP_VAR_KEY)
  set(${VAR} ${PREFIX}_MAP_${_MAP_VAR_KEY} ${ARGN})
  unset(_MAP_VAR_KEY)
endmacro()
##
macro(map_def PREFIX KEY)
  map_var(${PREFIX} ${KEY} _MAP_SET_VAR)
  def(${_MAP_SET_VAR} ${ARGN})
  unset(_MAP_SET_VAR)
endmacro()
##
macro(map_set PREFIX KEY)
  map_var(${PREFIX} ${KEY} _MAP_SET_VAR)
  set(${_MAP_SET_VAR} ${ARGN})
  unset(_MAP_SET_VAR)
endmacro()
##
macro(map_get PREFIX KEY VAR)
  map_var(${PREFIX} ${KEY} _MAP_GET_VAR)
  set(${VAR} ${${_MAP_GET_VAR}} ${ARGN})
  unset(_MAP_GET_VAR)
endmacro()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Macros
