### Preamble {{{
##  ==========================================================================
##        @file Hardening.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:36:23 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-13 Monday 00:50:53 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_HARDENING_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_HARDENING_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(OptionDef)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(HARDENING
  "Harden against whatever is possible."
  Y)
##
# Position Independent Executable (PIE):
option_def_bool(HARDENING_PIE
  "Harden against ROP attacks by taking advantage of ASLR."
  ${HARDENING})
##
# Stack Smashing Protector (SSP):
option_def_bool(HARDENING_SSP
  "Harden against stack buffer overflows and stack smashing attacks."
  ${HARDENING})
##
# Fortified Functions (FFs):
option_def_bool(HARDENING_FFS
  "Harden against buffer overflows in string and memory handling functions."
  ${HARDENING})
##
# String Format Security Functions (SFSFs):
option_def_bool(HARDENING_SFSFS
  "Harden against security exploits in uncontrolled string format functions."
  ${HARDENING})
##
# Non-Executable Stack (NES):
option_def_bool(HARDENING_NES
  "Harden against stack memory attacks by making stack non-executable."
  ${HARDENING})
##
# Non-Executable Heap (NEH):
option_def_bool(HARDENING_NEH
  "Harden against heap memory attacks by making heap non-executable."
  ${HARDENING})
##
# Relocation Read-Only (RELRO):
option_def_bool(HARDENING_RELRO
  "Harden against GOT overwrite (hijack) attacks by making GOT read-only."
  ${HARDENING})
##
# Immediate Symbol Binding (NOW):
option_def_bool(HARDENING_NOW
  "Harden against PLT overwrite (hijack) attacks by making PLT read-only."
  ${HARDENING})
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(HARDENING_FEATURES PIE SSP FFS SFSFS NES NEH RELRO NOW)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
##
### References {{{
##  ==========================================================================
##  [ 1] http://blog.siphos.be/2011/07/high-level-explanation-on-some-binary-e
##       xecutable-security
##  [ 2] http://en.chys.info/2010/12/note-gnu-stack
##  [ 3] http://grantcurell.com/2015/09/21/what-is-the-symbol-table-and-what-i
##       s-the-global-offset-table
##  [ 4] http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka14
##       320.html
##  [ 5] http://lintian.debian.org/tags-all.html
##  [ 6] http://tk-blog.blogspot.de/2009/02/relro-not-so-well-known-memory.htm
##       l
##  [ 7] http://wiki.debian.org/Hardening
##  [ 8] http://wiki.gentoo.org/wiki/Hardened/GNU_stack_quickstart
##  [ 9] http://wiki.gentoo.org/wiki/Hardened/Toolchain
##  [10] http://wiki.osdev.org/Stack_Smashing_Protector
##  [11] http://wiki.ubuntu.com/SecurityTeam/Roadmap/ExecutableStacks
##  [12] http://wikipedia.org/wiki/Address_space_layout_randomization
##  [13] http://wikipedia.org/wiki/Buffer_overflow
##  [14] http://wikipedia.org/wiki/Buffer_overflow_protection
##  [15] http://wikipedia.org/wiki/Return-oriented_programming
##  [16] http://wikipedia.org/wiki/Stack_buffer_overflow
##  [17] http://wikipedia.org/wiki/Uncontrolled_format_string
##  [18] http://www.owasp.org/index.php/C-Based_Toolchain_Hardening
##  [19] http://www.win.tue.nl/~aeb/linux/hh/protection.html
##  ==========================================================================
##  }}} References
