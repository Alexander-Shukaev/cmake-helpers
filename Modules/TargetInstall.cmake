### Preamble {{{
##  ==========================================================================
##        @file TargetInstall.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-06 Monday 01:31:18 (+0100)
##  --------------------------------------------------------------------------
##     @created 2017-02-04 Saturday 13:59:39 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_TARGET_INSTALL_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_TARGET_INSTALL_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(CMakeParseArguments)
include(Install)
include(RequireProject)
##  ==========================================================================
##  }}} Modules
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
function(target_install TARGET)
  require_project()
  # --------------------------------------------------------------------------
  set(_SV_KEYWORDS EXPORT)
  cmake_parse_arguments("_WITH"
                        "${_SV_KEYWORDS}"
                        ""
                        ""
                        ${ARGN})
  cmake_parse_arguments(""
                        ""
                        "${_SV_KEYWORDS}"
                        ""
                        ${ARGN})
  # --------------------------------------------------------------------------
  cmake_parse_arguments("" "REQUIRED;QUIET" "" "" ${_UNPARSED_ARGUMENTS})
  # --------------------------------------------------------------------------
  if(_UNPARSED_ARGUMENTS)
    set(_UNPARSED_ARGUMENTS ${ARGN})
  else()
    if(_WITH_EXPORT)
      if(NOT _EXPORT)
        set(_EXPORT ${PROJECT}Targets)
      endif()
      list(APPEND _UNPARSED_ARGUMENTS EXPORT ${_EXPORT})
    endif()
    set(_VAR INSTALL_INCLUDE_DIR)
    if(${_VAR})
      list(APPEND _UNPARSED_ARGUMENTS INCLUDES DESTINATION ${${_VAR}})
    endif()
    get_target_property(_TYPE ${TARGET} TYPE)
    if(NOT _TYPE MATCHES "^INTERFACE_LIBRARY$")
      set(_KIND ${INSTALL_${_TYPE}_KIND})
      if(_KIND)
        set(_VAR INSTALL_${_KIND}_DIR)
        if    (NOT DEFINED ${_VAR})
          set(_MESSAGES "Variable not set: ${_VAR}")
        elseif(NOT         ${_VAR})
          set(_MESSAGES "Variable empty: ${_VAR}")
        else()
          set(_MESSAGES)
        endif()
      else()
        set(_MESSAGES "No install kind for target type: ${_TYPE}")
      endif()
      if(_MESSAGES)
        if(_REQUIRED AND PROJECT_STANDALONE)
          message(FATAL_ERROR ${_MESSAGES})
        endif()
        if(NOT _QUIET)
          message(WARNING     ${_MESSAGES})
        endif()
        return()
      endif()
      list(APPEND _UNPARSED_ARGUMENTS ${_KIND} DESTINATION ${${_VAR}})
    endif()
  endif()
  install(TARGETS ${TARGET} ${_UNPARSED_ARGUMENTS})
endfunction()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Functions
