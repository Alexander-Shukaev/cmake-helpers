### Preamble {{{
##  ==========================================================================
##        @file Visibility.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2019-07-05 Friday 18:37:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-16 Thursday 23:42:12 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2019,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_VISIBILITY_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_VISIBILITY_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Prerequisites {{{
##  ==========================================================================
if(WIN32)
  return()
endif()
##  ==========================================================================
##  }}} Prerequisites
##
### Modules {{{
##  ==========================================================================
include(OptionDef)
##  ==========================================================================
##  }}} Modules
##
### Options {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
option_def_bool(VISIBILITY_HIDDEN
  "Whether to hide ELF symbols by default."
  Y)
##
option_def_bool(VISIBILITY_INLINES_HIDDEN
  "Whether to hide ELF symbols of inline functions by default."
  Y)
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Options
##
### References {{{
##  ==========================================================================
##  [1] http://gcc.gnu.org/wiki/Visibility
##  ==========================================================================
##  }}} References
