### Preamble {{{
##  ==========================================================================
##        @file Languages.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2017-02-26 Sunday 23:10:25 (+0100)
##  --------------------------------------------------------------------------
##     @created 2016-09-18 Sunday 20:48:44 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2017,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Guard {{{
##  ==========================================================================
if(_CMAKE_HELPERS_LANGUAGES_CMAKE_INCLUDED)
  return()
endif()
set(_CMAKE_HELPERS_LANGUAGES_CMAKE_INCLUDED Y)
##  ==========================================================================
##  }}} Guard
##
### Modules {{{
##  ==========================================================================
include(Def)
##  ==========================================================================
##  }}} Modules
##
### Variables {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def(LANGUAGES
  C
  CXX)
set(LANGUAGES_LOWER)
set(LANGUAGES_UPPER)
foreach(_L ${LANGUAGES})
  string(TOLOWER "${_L}" _L_LOWER)
  string(TOUPPER "${_L}" _L_UPPER)
  list(APPEND LANGUAGES_LOWER ${_L_LOWER})
  list(APPEND LANGUAGES_UPPER ${_L_UPPER})
endforeach()
##  ==========================================================================
##  }}} Public
##  ==========================================================================
##  }}} Variables
