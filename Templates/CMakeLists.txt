### Preamble {{{
##  ==========================================================================
##        @file CMakeLists.txt
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2016-08-17 Wednesday 23:40:53 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-06-03 Friday 23:56:49 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2016,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
if(INSTALL_CMAKE_DIR)
  get_filename_component(INSTALL_TEMPLATES_DIR
    ${CMAKE_CURRENT_SOURCE_DIR} NAME)
  path(INSTALL_TEMPLATES_DIR ${INSTALL_CMAKE_DIR} ${INSTALL_TEMPLATES_DIR})
  install(DIRECTORY ./
    DESTINATION ${INSTALL_TEMPLATES_DIR}
    FILES_MATCHING PATTERN *.cmake)
endif()
