### Preamble {{{
##  ==========================================================================
##        @file ExecuteProcess.cmake
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2020-11-01 Sunday 14:46:30 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-10-31 Saturday 20:48:51 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2020,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
set(ARGS)
set(VARS COMMAND
         WORKING_DIRECTORY
         TIMEOUT
          INPUT_FILE
         OUTPUT_FILE
          ERROR_FILE)
foreach(VAR ${VARS})
  if(${VAR})
    list(APPEND ARGS ${VAR} ${${VAR}})
  endif()
endforeach()
set(VARS OUTPUT_QUIET
          ERROR_QUIET
         OUTPUT_STRIP_TRAILING_WHITESPACE
          ERROR_STRIP_TRAILING_WHITESPACE)
foreach(VAR ${VARS})
  if(${VAR})
    list(APPEND ARGS ${VAR})
  endif()
endforeach()
foreach(VAR OUTPUT ERROR)
  if(${VAR}_FILE)
    get_filename_component(${VAR}_DIR ${${VAR}_FILE} PATH)
    file(MAKE_DIRECTORY ${${VAR}_DIR})
  endif()
endforeach()
execute_process(${ARGS} RESULT_VARIABLE RESULT)
foreach(VAR OUTPUT ERROR)
  if(${VAR}_FILE)
    file(READ ${${VAR}_FILE} ${VAR})
    if(NOT ${VAR})
      file(REMOVE ${${VAR}_FILE})
    endif()
  else()
    set(${VAR})
  endif()
endforeach()
if(RESULT)
  if(ERROR)
    message("${ERROR}")
  endif()
  string(REPLACE "'" "'\\''" COMMAND_STRING "${COMMAND}")
  string(REPLACE ";" "' '"   COMMAND_STRING "${COMMAND_STRING}")
  set(COMMAND_STRING "'${COMMAND_STRING}'")
  message(FATAL_ERROR "Failed command: "
                      "Failure exit status (${RESULT}): "
                      "${COMMAND_STRING}")
endif()
